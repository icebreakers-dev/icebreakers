﻿using UnityEngine;

namespace Icebreakers.Editor
{
    public class UWintab_Tablet 
    {

        public void Init()
        {
            UWintab_Library.Initialize();
            Debug.Log("tablet initilized");
        }

        public void Deinit()
        {
            UWintab_Library.Finalize();
            Debug.Log("tablet deinitialized");
        }

        public bool isAvailable
        {
            get { return UWintab_Library.IsAvailable(); }
        }

        public string deviceName
        {
            get { return UWintab_Library.GetDeviceName(); }
        }

        public string version
        {
            get { return UWintab_Library.GetVersion(); }
        }

        public bool isPressureSupported
        {
            get { return UWintab_Library.IsPressureSupported(); }
        }

        public bool isWheelSupported
        {
            get { return UWintab_Library.IsWheelSupported(); }
        }

        public bool isOrientationSupported
        {
            get { return UWintab_Library.IsOrientationSupported(); }
        }

        public bool isExpKeysSupported
        {
            get { return UWintab_Library.IsExpKeysSupported(); }
        }

        public int deviceNum
        {
            get { return UWintab_Library.GetDeviceNum(); }
        }

        public int expKeyNum
        {
            get { return UWintab_Library.GetExpKeyNum(0); }
        }

        public float x
        {
            get { return UWintab_Library.GetX(); }
        }

        public float y
        {
            get { return UWintab_Library.GetY(); }
        }

        public float pressure
        {
            get { return UWintab_Library.GetPressure(); }
        }

        public float wheel
        {
            get { return UWintab_Library.GetWheel(); }
        }

        public float azimuth
        {
            get { return UWintab_Library.GetAzimuth(); }
        }

        public float altitude
        {
            get { return UWintab_Library.GetAltitude(); }
        }

        public float twist
        {
            get { return UWintab_Library.GetTwist(); }
        }

        public int penId
        {
            get { return UWintab_Library.GetPenId(); }
        }

        public CursorType cursor
        {
            get { return UWintab_Library.GetCursor(); }
        }

        public int time
        {
            get { return UWintab_Library.GetTime(); }
        }

        public bool proximity
        {
            get { return UWintab_Library.GetProximity(); }
        }

        public int GetExpKeyNum(int tabletId)
        {
            return UWintab_Library.GetExpKeyNum(tabletId);
        }

        public bool GetButton(int id)
        {
            return UWintab_Library.GetButton(id);
        }

        public bool GetButtonDown(int id)
        {
            return UWintab_Library.GetButtonDown(id);
        }

        public bool GetButtonUp(int id)
        {
            return UWintab_Library.GetButtonUp(id);
        }

        public bool GetExpKey(int tabletId, int id)
        {
            return UWintab_Library.GetExpKey(tabletId, id);
        }

        public bool GetExpKey(int id)
        {
            return UWintab_Library.GetExpKey(0, id);
        }

        public bool GetExpKeyDown(int tabletId, int id)
        {
            return UWintab_Library.GetExpKeyDown(tabletId, id);
        }

        public bool GetExpKeyDown(int id)
        {
            return UWintab_Library.GetExpKeyDown(0, id);
        }

        public bool GetExpKeyUp(int tabletId, int id)
        {
            return UWintab_Library.GetExpKeyUp(tabletId, id);
        }

        public bool GetExpKeyUp(int id)
        {
            return UWintab_Library.GetExpKeyUp(0, id);
        }
    }

}