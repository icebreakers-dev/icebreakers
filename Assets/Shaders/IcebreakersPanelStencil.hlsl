#ifndef ICEBREAKERS_PANEL_STENCIL_INCLUDED
#define ICEBREAKERS_PANEL_STENCIL_INCLUDED

#include "IcebreakersCommon.hlsl"

struct Attributes
{
    float3 position : POSITION;
    float2 texcoord : TEXCOORD0;
    UNITY_VERTEX_INPUT_INSTANCE_ID
};

struct Varyings
{
    float4 positionCS : SV_POSITION;
    float2 uv : TEXCOORD0;

    UNITY_VERTEX_INPUT_INSTANCE_ID
    UNITY_VERTEX_OUTPUT_STEREO
};

Texture2D _PanelMask;
SamplerState sampler_PanelMask;

Texture2D _DitherTex;
SamplerState sampler_DitherTex;

float _Fade;

Varyings vert(Attributes input)
{
    Varyings output = (Varyings)0;

    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

    output.positionCS = TransformWorldToHClip(TransformObjectToWorld(input.position));
    output.uv = input.texcoord;
    return output;
}

half4 frag(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

    if (_PanelMask.Sample(sampler_PanelMask, input.uv).a < 0.5)
        discard;

    float dither = _DitherTex.Sample(sampler_DitherTex, input.uv * 10);
    if (_Fade < dither)
        discard;

    return half4(0, 0, 0, 0);
}

Varyings vert_clear(Attributes input)
{
    Varyings output = (Varyings)0;

    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_TRANSFER_INSTANCE_ID(input, output);
    UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

    float depth = 1;
    #if UNITY_REVERSED_Z
    depth = 0;
    #endif
    output.positionCS = float4(input.position.xy * 2, depth, 1);

    return output;
}

half4 frag_clear(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

    return half4(0, 0, 0, 0);
}

half4 frag_fade(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

    return half4(0, 0, 0, _Fade);
}

half4 frag_debug(Varyings input) : SV_Target
{
    UNITY_SETUP_INSTANCE_ID(input);
    UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

    if (!(_PanelMask.Sample(sampler_PanelMask, input.uv).a < 0.5))
        discard;

    return half4(0, 0, 0, 0);
}

#endif