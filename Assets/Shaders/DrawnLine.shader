Shader "Hidden/Icebreakers/DrawnLine"
{
	Properties
	{
		_PanelNumber("Panel Number", float) = 0
	}

    SubShader
	{
		Tags {"RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="4.5"}
        LOD 100

        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        struct Attributes
        {
            float4 vertex : POSITION;
            float4 side : TEXCOORD0;
            UNITY_VERTEX_INPUT_INSTANCE_ID
        };

        struct Varyings
        {
            float4 positionCS : SV_POSITION;
            float3 positionWS : TEXCOORD0;

            UNITY_VERTEX_INPUT_INSTANCE_ID
            UNITY_VERTEX_OUTPUT_STEREO
        };

        half4 _BaseColor;

        Varyings vert(Attributes input)
        {
            Varyings output = (Varyings)0;

            UNITY_SETUP_INSTANCE_ID(input);
            UNITY_TRANSFER_INSTANCE_ID(input, output);
            UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

            float3 vertex = fakeFOV(TransformObjectToWorld(input.vertex.xyz));
            float3 side = TransformObjectToWorldNormal(input.side.xyz);
            float width = input.side.w;

            vertex += side * width;

            output.positionCS = TransformWorldToHClip(vertex);
            output.positionWS = vertex;
            return output;
        }

        half4 frag(Varyings input) : SV_Target
        {
            UNITY_SETUP_INSTANCE_ID(input);
            UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

            #ifdef _PANEL_CLIP
                if (CullInFrontOfPanel(input.positionWS)) discard;
            #endif

            #ifdef _PANEL_CUTOUT
                if (IsInsidePanel(input.positionWS)) discard;
            #endif

            half3 color = _BaseColor.rgb;
            return half4(color.rgb, 1);
        }

        ENDHLSL

        // 0 CPU
        Pass
        {
            Name "SpeechBubble"

            Cull Off
            Offset -5, -5

            Stencil {
                Ref [_PanelNumber]
                Comp Equal
            }

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile_instancing

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }

        // 1 CPU No Stencil
        Pass
        {
            Name "SpeechBubble"

            Cull Off
            Offset -5, -5

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile_instancing

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            ENDHLSL
        }
    }
}
