Shader "Icebreakers/Standard"
{
    Properties
    {
        [MainTexture] _BaseMap("Texture", 2D) = "white" {}
        [MainColor]   _BaseColor("Color", Color) = (1, 1, 1, 1)

        [MaterialToggle] _UseShadowMap("Use Shadow Map", float) = 0
        [NoScaleOffset] _ShadowMap("Shadow Albedo", 2D) = "white" {}
        _ShadowColor("Shadow Color", Color) = (1,1,1,1)

        [NoScaleOffset] _HatchingMap("Hatching Map", 2D) = "white" {}
        _HatchingLineCount("Hatching Line Count", float) = 200
        _HatchingRatio("Hatching Ratio", float) = 1

        _ShadowOutlineWidth("Shadow Outline Width", float) = 1

        [HideInInspector] _OverrideLightDirection("Override Light Direction", vector) = (0, 0, 0, 0)
		[MaterialToggle] _FakeFOV("use fake fov", float) = 0
		[MaterialToggle] _DropShadow("hide Drop Shadow", float) = 0
    }
    SubShader
    {
        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        TEXTURE2D(_BaseMap);      SAMPLER(sampler_BaseMap);
        TEXTURE2D(_ShadowMap);    SAMPLER(sampler_ShadowMap);
        TEXTURE2D(_HatchingMap);  SAMPLER(sampler_HatchingMap);
        TEXTURE2D_SHADOW(_DirectionalShadowMap); SAMPLER_CMP(sampler_DirectionalShadowMap);

        CBUFFER_START(UnityPerMaterial)
            float4 _BaseMap_ST;
            half4 _BaseColor;
            half _Cutoff;
            half _Surface;
            half4 _ShadowColor;
            half _UseShadowMap;
            half _HatchingLineCount;
            half _HatchingRatio;
            half _ShadowOutlineWidth;
            half4 _OverrideLightDirection;
            float _FakeFOV;
		    float _DropShadow;
        CBUFFER_END

        float3 _ViewOrigin;
        float3 _ViewOriginForward;
        float3 _ViewOriginUp;
        float3 _ViewOriginRight;

        float4 _SceneLightDir;
        float4 _SceneLightColor;

        float4 _BakeRect;
        float4x4 _WorldToShadow;

		/*float3 _PanelCameraOrigin;
		float3 _PanelCameraDirection;
		float _PanelCameraFOV;
		float _PanelCameraImageDist;*/

        struct Varyings
        {
            float2 uv        : TEXCOORD0;
            float2 uv2        : TEXCOORD3;
            float3 positionWS : TEXCOORD1;
            float3 normalWS : TEXCOORD2;
            float4 vertex : SV_POSITION;

            UNITY_VERTEX_INPUT_INSTANCE_ID
            UNITY_VERTEX_OUTPUT_STEREO
        };

        struct Attributes
        {
            float3 positionOS       : POSITION;
            float3 normalOS         : NORMAL;
            float2 uv               : TEXCOORD0;
            float2 uv2               : TEXCOORD1;
            UNITY_VERTEX_INPUT_INSTANCE_ID
        };

        Varyings vert(Attributes input)
        {
            Varyings output = (Varyings)0;

            UNITY_SETUP_INSTANCE_ID(input);
            UNITY_TRANSFER_INSTANCE_ID(input, output);
            UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

#if _NO_FAKE_FOV
            output.positionWS = TransformObjectToWorld(input.positionOS);
#else
            output.positionWS = lerp(fakeFOV(TransformObjectToWorld(input.positionOS)), TransformObjectToWorld(input.positionOS), _FakeFOV);
#endif

            output.vertex = TransformWorldToHClip(output.positionWS);
            output.normalWS = TransformObjectToWorldNormal(input.normalOS);
            output.uv = TRANSFORM_TEX(input.uv, _BaseMap);
            output.uv2 = input.uv2;

            return output;
        }

        half4 frag(Varyings input) : SV_Target
        {
            UNITY_SETUP_INSTANCE_ID(input);
            UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

            #ifdef _PANEL_CLIP
                if (CullInFrontOfPanel(input.positionWS)) discard; //currently disabled
            #endif

            #ifdef _PANEL_CUTOUT
                if (IsInsidePanel(input.positionWS)) discard;
            #endif

            float3 staticLinesDirection = input.positionWS - _ViewOrigin;
            float distanceToViewOrigin = length(staticLinesDirection);

            float upComponent = dot(staticLinesDirection, _ViewOriginUp);
            float fwdComponent = dot(staticLinesDirection, _ViewOriginForward);

            half angle = atan2(upComponent, -fwdComponent);
            angle *= round(_HatchingLineCount);

            //float3 hatching = _HatchingMap.SampleLevel(sampler_HatchingMap, float2(0, angle), 0).rgb;
            //float hatching = ProceduralHatching(angle, _HatchingRatio);


            half3 lightDir = -lerp(_SceneLightDir.xyz, _OverrideLightDirection.xyz, _OverrideLightDirection.w);
            half3 lightColor = _SceneLightColor.rgb;

            // TODO: shadows
            half shadow = 1;
            #if _SHADOWS
            float4 shadowCoord = mul(_WorldToShadow, float4(input.positionWS, 1));
			//shadowCoord.xy = float
            shadow = SAMPLE_TEXTURE2D_SHADOW(_DirectionalShadowMap, sampler_DirectionalShadowMap, shadowCoord.xyz);
			shadow = lerp(shadow, 1, _DropShadow);
            #elif _RAYTRACED_SHADOWS
            shadow = RaytraceShadow(input.positionWS, lightDir);
            shadow = lerp(shadow, 1, _DropShadow);
            #endif


            half lambert = dot(lightDir, input.normalWS);

            //float shadingOutlineWidth = distanceToViewOrigin / 400 * _ShadowOutlineWidth;
            //half shadingOutline = lambert > -shadingOutlineWidth && lambert < shadingOutlineWidth ? 0 : 1;

            int rshadow = saturate(lambert) * shadow > 0.001 ? 1 : 0;
			rshadow = saturate(lerp(rshadow, 1.0, distance(input.positionWS, _WorldSpaceCameraPos.xyz) - 500));

            half4 color = SAMPLE_TEXTURE2D(_BaseMap, sampler_BaseMap, input.uv);
            half4 shadowmap = SAMPLE_TEXTURE2D(_ShadowMap, sampler_ShadowMap, input.uv);

            if (_UseShadowMap)
            {
                color = lerp(color * _BaseColor, shadowmap, rshadow);
            }
            else
            {
                color *= lerp(_ShadowColor, _BaseColor, rshadow);
            }

            #if _BAKING_PREVIEW
            float size = 300;
            float2 pixel = round(input.uv2 * (size / 4096) * 4096);
            float value = hash(pixel.x + pixel.y * 4096);
            color.rgb *= value / (float)(1u << 31);
            #endif

            //color.rgb *= lerp(hatching, float3(1, 1, 1), rshadow);
            //color.rgb *= shadingOutline;
			//color.rgb = shadow;
            return color;
        }

        ENDHLSL

        Tags {"RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="4.5"}
        LOD 100

        Pass
        {
            Name "Standard"

            HLSLPROGRAM
            #pragma target 4.5

            #pragma vertex vert
            #pragma fragment frag

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile_instancing

            #pragma multi_compile _ _SHADOWS
            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP
            #pragma multi_compile _ _BAKING_PREVIEW
            #pragma multi_compile _ _NO_FAKE_FOV

            ENDHLSL
        }

        Pass
        {
            Name "DepthOnly"
            Tags{"LightMode" = "DepthOnly"}

            ZWrite On
            ColorMask 0

            HLSLPROGRAM
            #pragma target 4.5

            #pragma vertex DepthOnlyVertex
            #pragma fragment DepthOnlyFragment

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            struct AttributesDepthOnly
            {
                float4 position     : POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct VaryingsDepthOnly
            {
                float2 uv           : TEXCOORD0;
                float4 positionCS   : SV_POSITION;
                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            VaryingsDepthOnly DepthOnlyVertex(AttributesDepthOnly input)
            {
                VaryingsDepthOnly output = (VaryingsDepthOnly)0;
                UNITY_SETUP_INSTANCE_ID(input);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

                output.positionCS = TransformObjectToHClip(input.position.xyz);
                return output;
            }

            half4 DepthOnlyFragment(VaryingsDepthOnly input) : SV_TARGET
            {
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);
                return 0;
            }

            ENDHLSL
        }

        Pass
        {
            Name "ShadowCaster"
            Tags{"LightMode" = "ShadowCaster"}

            ZWrite On
            ZTest LEqual
            ColorMask 0

            HLSLPROGRAM
            #pragma target 4.5

            //--------------------------------------
            // GPU Instancing
            #pragma multi_compile_instancing

            #pragma vertex ShadowPassVertex
            #pragma fragment ShadowPassFragment

            #include "Packages/com.unity.render-pipelines.core/ShaderLibrary/CommonMaterial.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"
            #include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Shadows.hlsl"

            float3 _LightDirection;

            struct AttributesShadowCaster
            {
                float4 positionOS   : POSITION;
                float3 normalOS     : NORMAL;
                float2 texcoord     : TEXCOORD0;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct VaryingsShadowCaster
            {
                float2 uv           : TEXCOORD0;
                float4 positionCS   : SV_POSITION;
            };

            float4 GetShadowPositionHClip(AttributesShadowCaster input)
            {
				float3 positionWS = fakeFOV(TransformObjectToWorld(input.positionOS.xyz));

                float3 normalWS = TransformObjectToWorldNormal(input.normalOS);

                float4 positionCS = TransformWorldToHClip(ApplyShadowBias(positionWS, normalWS, _LightDirection));

            #if UNITY_REVERSED_Z
                positionCS.z = min(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
            #else
                positionCS.z = max(positionCS.z, positionCS.w * UNITY_NEAR_CLIP_VALUE);
            #endif

                return positionCS;
            }

            VaryingsShadowCaster ShadowPassVertex(AttributesShadowCaster input)
            {
                VaryingsShadowCaster output;
                UNITY_SETUP_INSTANCE_ID(input);

                output.uv = TRANSFORM_TEX(input.texcoord, _BaseMap);
                output.positionCS = GetShadowPositionHClip(input);
                return output;
            }

            half4 ShadowPassFragment(VaryingsShadowCaster input) : SV_TARGET
            {
                return 0;
            }
            ENDHLSL
        }

        Pass
        {
            Name "Bake"
            Tags { "LightMode"="Bake" }

            Cull Off
            ZTest Always
            Blend One One

            HLSLPROGRAM

            #pragma target 5.0
            #pragma vertex vert_bake
            #pragma fragment frag
            #pragma multi_compile _ _RAYTRACED_SHADOWS

            struct AttributesBake
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };

            float4 _SampleOffset;

            Varyings vert_bake(AttributesBake input)
            {
                Varyings output = (Varyings)0;

                float2 bakeUV = input.texcoord1 * _BakeRect.zw + _BakeRect.xy;
                output.vertex = float4(bakeUV * 2 - 1, 0, 1);
                #if UNITY_UV_STARTS_AT_TOP
                output.vertex.y *= -1;
                #endif

                output.vertex.xy += _SampleOffset.xy;

                output.positionWS = TransformObjectToWorld(input.position);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord;
                return output;
            }

            ENDHLSL
        }
    }

    FallBack "Hidden/Universal Render Pipeline/FallbackError"
}
