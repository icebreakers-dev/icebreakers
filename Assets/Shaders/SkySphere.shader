Shader "Icebreakers/SkySphere"
{
    Properties
    {
        [MainTexture] _Skybox("Skybox", 2D) = "white" {}
		_BaseColor("Color", color) = (0,0,0,0)
    }

    SubShader
    {
        HLSLINCLUDE

        #include "IcebreakersCommon.hlsl"

        struct Varyings
        {
            float2 uv         : TEXCOORD0;
            float3 positionWS : TEXCOORD1;
            float3 normalWS   : TEXCOORD2;
            float4 positionCS : SV_POSITION;

            UNITY_VERTEX_INPUT_INSTANCE_ID
            UNITY_VERTEX_OUTPUT_STEREO
        };

        Texture2D _Skybox;
        SamplerState sampler_Skybox;

		CBUFFER_START(UnityPerMaterial)
			half4 _BaseColor;
        CBUFFER_END

        half4 frag(Varyings input) : SV_Target
        {
            UNITY_SETUP_INSTANCE_ID(input);
            UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

            #ifdef _PANEL_CLIP
                if (CullInFrontOfPanel(input.positionWS)) discard; //currently disabled
            #endif

            #ifdef _PANEL_CUTOUT
                if (IsInsidePanel(input.positionWS)) discard;
            #endif

            half4 color = SAMPLE_TEXTURE2D(_Skybox, sampler_Skybox, input.uv) * _BaseColor;
            return color;
        }

        ENDHLSL

        Tags {"RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="4.5" "Queue"="Geometry-200"}
		LOD 100

			Pass
		{
			Name "SkySphere"

			Cull Off
			Zwrite Off

            HLSLPROGRAM

            #pragma target 4.5

            #pragma vertex vert
            #pragma fragment frag

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile_instancing

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP

            struct Attributes
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;

                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

			

            Varyings vert(Attributes input)
            {
                Varyings output = (Varyings)0;

                UNITY_SETUP_INSTANCE_ID(input);
                UNITY_TRANSFER_INSTANCE_ID(input, output);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);

                output.positionWS = _WorldSpaceCameraPos + input.position;
                output.positionCS = TransformWorldToHClip(output.positionWS);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord;
                return output;
            }

            ENDHLSL
        }

        Pass // 1 Bake
        {
            Name "Bake"
            Tags { "LightMode"="Bake" }

            Cull Off
            ZTest Always

            HLSLPROGRAM

            #pragma vertex vert
            #pragma fragment frag

            struct Attributes
            {
                float3 position : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
            };

            float4 _BakeRect;

            Varyings vert(Attributes input)
            {
                Varyings output = (Varyings)0;

                float2 bakeUV = input.texcoord * _BakeRect.zw + _BakeRect.xy;
                output.positionCS = float4(bakeUV * 2 - 1, 0, 1);
                #if UNITY_UV_STARTS_AT_TOP
                output.positionCS.y *= -1;
                #endif

                output.positionWS = TransformObjectToWorld(input.position);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord;
                return output;
            }

            ENDHLSL
        }
    }

    FallBack "Hidden/Universal Render Pipeline/FallbackError"
}
