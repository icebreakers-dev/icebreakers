#ifndef ICEBREAKERS_COMMON_INCLUDED
#define ICEBREAKERS_COMMON_INCLUDED

#include "Packages/com.unity.render-pipelines.universal/ShaderLibrary/Core.hlsl"

float3 _PanelCameraOrigin;
float3 _PanelCameraDirection;
float _PanelCameraFOV;
float _PanelCameraImageDist;

// packs given two floats into one float
float pack_float(half x, half y) {
    int x1 = (int)(x * 255);
    int y1 = (int)(y * 255);
    float f = (y1 * 256) + x1;
    return f;
}

// unpacks given float to two floats
half2 unpack_float(float f) {
    float dy = floor(f / 256);
    float dx = f - (dy * 256);
    half y = (half)(dy / 255);
    half x = (half)(dx / 255);
    return half2(x, y);
}

float3 fakeFOV(float3 ws){
	float3 op = ws - _PanelCameraOrigin;
	float l = dot(op, _PanelCameraDirection);
	float3 opDot = _PanelCameraDirection * l;
	float3 scaleVect = op - opDot;
	float dist = (l - _PanelCameraImageDist);
	float scale = dist / pow(abs(dist), _PanelCameraFOV);
	scale *= sign(dist);
	ws = _PanelCameraOrigin + opDot + scaleVect * scale;
	return ws;
}

#if _PANEL_CUTOUT || _PANEL_CLIP
Texture2D _PanelMask;
SamplerState sampler_PanelMask;
Texture2D _DitherTex;
SamplerState sampler_DitherTex;
float _Fade;
float3 _PanelForward;
float3 _PanelUp;
float3 _PanelRight;
float3 _PanelPosition;
int _PanelInvert;

bool IsInsidePanel(float3 posWS)
{
    float3 rayOrigin = _WorldSpaceCameraPos;
    float3 rayDir = posWS - rayOrigin;
    float dist = length(rayDir);
    rayDir /= dist;

    float3 planeNormal = _PanelForward;
    float3 planeOrigin = _PanelPosition;

    float distance = -dot(planeNormal, planeOrigin);

    float vdot = dot(rayDir, planeNormal);
    if (vdot > 0)
        return false;

    float ndot = -dot(rayOrigin, planeNormal) - distance;

    if (abs(vdot) < 0.0001f)
    {
        return false;
    }

    float t = ndot / vdot;

    if (t < 0 || t > dist)
    {
        return false;
    }

    float3 pointOnPlane = (rayOrigin + rayDir * t) - planeOrigin;
    float x = dot(pointOnPlane, _PanelRight);
    float y = dot(pointOnPlane, _PanelUp);

    float2 uv = float2(x, y) + 0.5;

    if (any(saturate(uv) != uv))
    {
        return false;
    }

    if (_PanelMask.Sample(sampler_PanelMask, uv).a < 0.5)
        return false;

    float dither = _DitherTex.Sample(sampler_DitherTex, uv * 10);
    if (_Fade < dither)
        return false;

    return true;
}

bool ClipToPanel(float3 positionWS)
{
    return _PanelInvert ^ IsInsidePanel(positionWS);
}

bool CullInFrontOfPanel(float3 positionWS)
{
	return false;// dot(positionWS - _PanelPosition, _PanelForward) > 0;
}
#endif

float ProceduralHatching(float x, float spacing)
{
    // make x periodic
    x = (frac(x) * 2 - 1);

    // then use a light falloff function so that it reaches 0
    // at spacing and scale it back to [-1, 1]
    float x2 = x*x;
    float x4 = x*x*x*x;
    float xr = x*spacing*2;
    float f = ((1 - x4) * (1 - x4)) / ((xr*xr) + 1);

    // threshold at 0.5 to get a repeating pattern
    // the dark lines will smaller the steeper the falloff function
    return f < 0.5;
}

uint hash( uint a)
{
   a = (a+0x7ed55d16) + (a<<12);
   a = (a^0xc761c23c) ^ (a>>19);
   a = (a+0x165667b1) + (a<<5);
   a = (a+0xd3a2646c) ^ (a<<9);
   a = (a+0xfd7046c5) + (a<<3);
   a = (a^0xb55a4f09) ^ (a>>16);
   return a;
}

#if _RAYTRACED_SHADOWS

struct Bounds
{
    float3 _min;
    float3 _max;
};

struct BVHNode
{
    Bounds bbox;
    int start;
    int primitiveCount;
    int rightOffset;
};

struct Intersection
{
    int primitiveIndex;
    float t;
};

struct NativeRay
{
    float3 o;
    float3 d;
    float3 invD;
    float tMin;
    float tMax;
};

struct BVHPrimitive
{
    float3 v1;
    float3 v2;
    float3 v3;
};

StructuredBuffer<BVHPrimitive> _Primitives;
StructuredBuffer<BVHNode> _Nodes;

bool IsLeaf(BVHNode node)
{
    return node.rightOffset == 0;
}

bool IntersectRayBBox(Bounds bbox, NativeRay ray, out float tnear, out float tfar)
{
    float3 _min = bbox._min;
    float3 _max = bbox._max;

    float tmin = (_min.x - ray.o.x) * ray.invD.x;
    float tmax = (_max.x - ray.o.x) * ray.invD.x;

    if (tmin > tmax)
    {
        Swap(tmin, tmax);
    }

    float tymin = (_min.y - ray.o.y) * ray.invD.y;
    float tymax = (_max.y - ray.o.y) * ray.invD.y;

    if (tymin > tymax)
    {
        Swap(tymin, tymax);
    }

    if ((tmin > tymax) || (tymin > tmax))
    {
        tnear = 0;
        tfar = 0;
        return false;
    }

    tmin = max(tymin, tmin);
    tmax = min(tymax, tmax);

    float tzmin = (_min.z - ray.o.z) * ray.invD.z;
    float tzmax = (_max.z - ray.o.z) * ray.invD.z;

    if (tzmin > tzmax)
    {
        Swap(tzmin, tzmax);
    }

    if ((tmin > tzmax) || (tzmin > tmax))
    {
        tnear = 0;
        tfar = 0;
        return false;
    }

    tmin = max(tzmin, tmin);
    tmax = min(tzmax, tmax);

    tnear = tmin;
    tfar = tmax;

    return true;
}

bool IntersectRayTriangle2(float3 p0, float3 p1, float3 p2, NativeRay ray,
    out float t, out float b0, out float b1, out float b2)
{
    t = 0;
    b0 = 0;
    b1 = 0;
    b2 = 0;

    float3 e1 = p1 - p0;
    float3 e2 = p2 - p0;
    float3 h = cross(ray.d, e2);
    float divisor = dot(h, e1);

    if (divisor > -0.0000001 && divisor < 0.0000001)
    {
        return false;
    }

    float inv_divisor = 1.0 / divisor;

    float3 s = ray.o - p0;
    float u = dot(h, s) * inv_divisor;
    if (u < 0.0 || u > 1.0)
    {
        return false;
    }

    float3 q = cross(s, e1);
    float v = dot(ray.d, q) * inv_divisor;
    if (v < 0.0 || u + v > 1.0)
    {
        return false;
    }

    float tt = dot(e2, q) * inv_divisor;
    if (tt < ray.tMin || tt > ray.tMax)
    {
        return false;
    }

    b0 = u;
    b1 = v;
    b2 = 1 - u - v;
    t = tt;

    return true;
}

Intersection IntersectRayBVH(NativeRay ray, bool onlyTestOcclusion)
{
    Intersection intersection;
    intersection.primitiveIndex = -1;
    intersection.t = 10000000;

    uint numStructs, stride;
    _Nodes.GetDimensions(numStructs, stride);
    if (numStructs == 0)
        return intersection;

    int todo[64];
    float todoT[64];
    int stackptr = 0;

    // "Push" on the root node to the working set
    todo[stackptr] = 0;
    todoT[stackptr] = -10000000;

    while (stackptr >= 0)
    {
        // Pop off the next node to work on.
        int ni = todo[stackptr];
        float near = todoT[stackptr];
        stackptr--;

        BVHNode node = _Nodes[ni];

        // If this node is further than the closest found intersection, continue
        if (near > intersection.t) continue;

        // Is leaf -> Intersect
        if (IsLeaf(node))
        {
            for (int i = 0; i < node.primitiveCount; i++)
            {
                BVHPrimitive obj = _Primitives[node.start + i];

                // frontface culling culling
                float3 n = cross(obj.v2 - obj.v1, obj.v3 - obj.v1);
                if (dot(n, ray.d) < 0)
                {
                    continue;
                }

                float current;
                float b0, b1, b2;
                if (!IntersectRayTriangle2(
                    obj.v1, obj.v2, obj.v3, ray,
                    current, b0, b1, b2))
                {
                    continue;
                }

                if (current < intersection.t)
                {
                    intersection.t = current;
                    intersection.primitiveIndex = node.start + i;

                    if (onlyTestOcclusion)
                    {
                        return intersection;
                    }
                }
            }

        }
        else
        {  // Not a leaf

            float hit1, hit2, hit3, hit4;
            bool hitc0 = IntersectRayBBox(_Nodes[ni + 1].bbox, ray, hit1, hit2);
            bool hitc1 = IntersectRayBBox(_Nodes[ni + node.rightOffset].bbox, ray, hit3, hit4);

            if (hitc0 && hitc1)
            {
                // We assume that the left child is a closer hit...
                int closer = ni + 1;
                int other = ni + node.rightOffset;

                // ... If the right child was actually closer, swap the relavent values.
                if (hit3 < hit1)
                {
                    Swap(hit1, hit3);
                    Swap(hit2, hit4);
                    Swap(closer, other);
                }

                // It's possible that the nearest object is still in the other side, but
                // we'll check the further-awar node later...

                // Push the farther first
                stackptr++;
                todo[stackptr] = other;
                todoT[stackptr] = hit3;

                // And now the closer (with overlap test)
                stackptr++;
                todo[stackptr] = closer;
                todoT[stackptr] = hit1;
            }
            else if (hitc0)
            {
                stackptr++;
                todo[stackptr] = ni + 1;
                todoT[stackptr] = hit1;
            }
            else if (hitc1)
            {
                stackptr++;
                todo[stackptr] = ni + node.rightOffset;
                todoT[stackptr] = hit3;
            }
        }
    }

    return intersection;
}

half RaytraceShadow(float3 positionWS, float3 lightDir)
{
    NativeRay ray;
    ray.o = positionWS;
    ray.d = lightDir;
    ray.invD = 1.0/lightDir;
    ray.tMin = 0.00001f;
    ray.tMax = 1000000;

    Intersection isect = IntersectRayBVH(ray, true);
    if (isect.primitiveIndex == -1)
    {
        return 1;
    }

    return 0;
}
#endif

#endif