Shader "Icebreakers/Baked"
{
    Properties
    {
        [NoScaleOffset] _BakedTexture("Baked Texture", 2D) = "white" {}
        _BakedRect("Baked Rect", Vector) = (0,0,1,1)

        [MaterialToggle] _FakeFOV("use fake fov", float) = 0
    }

    SubShader
    {
        Tags {"RenderType" = "Opaque" "IgnoreProjector" = "True" "RenderPipeline" = "UniversalPipeline" "ShaderModel"="4.5"}
        LOD 100

        Pass
        {
            Name "Baked"

            HLSLPROGRAM
            #pragma target 4.5
            #pragma vertex vert
            #pragma fragment frag

            // -------------------------------------
            // Unity defined keywords
            #pragma multi_compile_instancing

            #pragma multi_compile _ _PANEL_CUTOUT _PANEL_CLIP
            #pragma multi_compile _ _NO_FAKE_FOV

            #include "IcebreakersCommon.hlsl"

            struct Attributes
            {
                float3 positionOS : POSITION;
                float3 normal   : NORMAL;
                float2 texcoord : TEXCOORD0;
                float2 texcoord1 : TEXCOORD1;
                UNITY_VERTEX_INPUT_INSTANCE_ID
            };

            struct Varyings
            {
                float2 uv         : TEXCOORD0;
                float3 positionWS : TEXCOORD1;
                float3 normalWS   : TEXCOORD2;
                float4 positionCS : SV_POSITION;

                UNITY_VERTEX_INPUT_INSTANCE_ID
                UNITY_VERTEX_OUTPUT_STEREO
            };

            Texture2D _BakedTexture;
            SamplerState sampler_BakedTexture;

            CBUFFER_START(UnityPerMaterial)
                float4 _BakedRect;
                float _FakeFOV;
            CBUFFER_END

            Varyings vert(Attributes input)
            {
                Varyings output = (Varyings)0;

                UNITY_SETUP_INSTANCE_ID(input);
                UNITY_TRANSFER_INSTANCE_ID(input, output);
                UNITY_INITIALIZE_VERTEX_OUTPUT_STEREO(output);
#if _NO_FAKE_FOV
                output.positionWS = TransformObjectToWorld(input.positionOS);
#else
                output.positionWS = lerp(fakeFOV(TransformObjectToWorld(input.positionOS)), TransformObjectToWorld(input.positionOS), _FakeFOV);
#endif
                output.positionCS = TransformWorldToHClip(output.positionWS);

                output.normalWS = TransformObjectToWorldNormal(input.normal);

                output.uv = input.texcoord1 * _BakedRect.zw + _BakedRect.xy;
                return output;
            }

            half4 frag(Varyings input) : SV_Target
            {
                UNITY_SETUP_INSTANCE_ID(input);
                UNITY_SETUP_STEREO_EYE_INDEX_POST_VERTEX(input);

                #ifdef _PANEL_CLIP
                    if (CullInFrontOfPanel(input.positionWS)) discard;
                #endif

                #ifdef _PANEL_CUTOUT
                    if (IsInsidePanel(input.positionWS)) discard;
                #endif

                half4 color = _BakedTexture.Sample(sampler_BakedTexture, input.uv);
                return color;
            }

            ENDHLSL
        }
    }
}
