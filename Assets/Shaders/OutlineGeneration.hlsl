#ifndef OUTLINE_GENERATION_INCLUDED
#define OUTLINE_GENERATION_INCLUDED

struct Line
{
    float3 p1;
    float3 p2;
};

struct IndirectArguments
{
    uint vertexCountPerInstance;
    uint instanceCount;
    uint startVertexLocation;
    uint startInstanceLocation;
};

struct Face
{
    int v1;
    int v2;
    int v3;
};

struct HalfEdge
{
    int v1;
    int v2;
    int face;
    int edge;
};

struct Edge
{
    int e1;
    int e2;
    int sharp;
};

#endif