using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;

namespace Icebreakers
{
    [CreateAssetMenu(menuName = "Render Pipeline/Render Pipeline", order = 1)]
    public class RenderPipelineAsset : UnityEngine.Rendering.RenderPipelineAsset
    {
        [SerializeField]
        RenderPipelineResources resources;

        [SerializeField]
        bool useComputeLineGeneration = false;

        public bool UseComputeLineGeneration => useComputeLineGeneration && SystemInfo.supportsComputeShaders;

        [SerializeField]
        [Range(0, 8)]
        int msaaSamples = 4;
        public int MSAASamples => msaaSamples;

        [SerializeField]
        [Range(1, 10)]
        float outlineThickness = 2.3f;
        public float OutlineThickness => outlineThickness;

        protected override UnityEngine.Rendering.RenderPipeline CreatePipeline()
        {
            return new RenderPipeline(this, resources);
        }

        Material _defaultMaterial;
        public override Material defaultMaterial
        {
            get => new Material(defaultShader);
        }

        Shader _defaultShader;
        public override Shader defaultShader
        {
            get
            {
                if (_defaultShader == null)
                {
                    _defaultShader = Shader.Find("Icebreakers/Standard");
                }

                return _defaultShader;
            }
        }

        static string[] s_RenderingLayerNames;
        public override string[] renderingLayerMaskNames
        {
            get
            {
                if (s_RenderingLayerNames == null)
                {
                    s_RenderingLayerNames = new string[32];
                    s_RenderingLayerNames[0] = "Default";
                    for (int i = 1; i < 32; i++)
                    {
                        s_RenderingLayerNames[i] = $"Panel {i}";
                    }
                }

                return s_RenderingLayerNames;
            }
        }
    }
}
