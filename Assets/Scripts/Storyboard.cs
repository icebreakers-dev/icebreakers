using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.IO;
#if UNITY_EDITOR
using UnityEditor;
using UnityEditor.SceneManagement;
#endif

namespace Icebreakers
{
    [System.Serializable]
    public class StoryboardPage : ISerializationCallbackReceiver
    {
#if UNITY_EDITOR
        public SceneAsset pageScene;
#endif

        public string title;
        public Texture2D previewIcon;
        public int runtimeSceneIndex;

        [SerializeField]
        TimelineEntry[] timelineEntries = Array.Empty<TimelineEntry>();
        public Dictionary<string, float> timeline = new Dictionary<string, float>();

        public void OnAfterDeserialize()
        {
            timeline.Clear();
            foreach (var entry in timelineEntries)
            {
                timeline[entry.id] = entry.time;
            }
        }

        public void OnBeforeSerialize()
        {
            timelineEntries = new TimelineEntry[timeline.Count];
            int i = 0;
            foreach (var pair in timeline)
            {
                timelineEntries[i++] = new TimelineEntry { id = pair.Key, time = pair.Value };
            }
        }

        public float GetTimelineTime(string id)
        {
            if (String.IsNullOrEmpty(id))
                return 0;

            if (timeline.TryGetValue(id, out float time))
            {
                return time;
            }

            return 0;
        }
    }

    [System.Serializable]
    public struct TimelineEntry
    {
        public string id;
        public float time;
    }

    [CreateAssetMenu(menuName="Icebreakers/Storyboard")]
    public class Storyboard : ScriptableObject
    {
        [SerializeField]
        List<StoryboardPage> pages = new List<StoryboardPage>();
        public IReadOnlyList<StoryboardPage> Pages => pages;


#if UNITY_EDITOR
        [SerializeField]
        public SceneAsset mainScene;

        public IList PagesList => pages;

        public StoryboardPage CreatePage(SceneAsset sceneAsset, string title)
        {
            if (sceneAsset == null)
                throw new ArgumentNullException(nameof(sceneAsset));

            if (sceneAsset == mainScene || pages.FindIndex(x => x.pageScene == sceneAsset) != -1)
                throw new ArgumentException("Cannot create page with scene that is already in the storyboard");

            var page = new StoryboardPage();
            page.pageScene = sceneAsset;
            page.title = title;
            pages.Add(page);

            UpdateBuildSettings();
            EditorUtility.SetDirty(this);
            return page;
        }

        public void RemovePage(StoryboardPage page)
        {
            if (page == null)
                throw new ArgumentNullException(nameof(page));

            int index = pages.FindIndex(x => x == page);
            if (index == -1)
                throw new ArgumentException("Page is not part of the storyboard");

            pages.RemoveAt(index);

            UpdateBuildSettings();
            EditorUtility.SetDirty(this);
        }

        public void UpdateBuildSettings()
        {
            List<EditorBuildSettingsScene> scenes = new List<EditorBuildSettingsScene>();
            if (mainScene == null)
            {
                Debug.LogError("[Storyboard] Missing main scene", this);
            }
            else
            {
                scenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(mainScene), true));
            }

            for (int i = 0; i < pages.Count; i++)
            {
                StoryboardPage page = pages[i];
                if (page.pageScene == null)
                {
                    Debug.LogError($"[Storyboard] Missing scene for page {i + 1}");
                    continue;
                }

                string scenePath = AssetDatabase.GetAssetPath(page.pageScene);
                string bakingFolder = Path.Combine(Path.GetDirectoryName(scenePath), Path.GetFileNameWithoutExtension(scenePath));
                string bakedScenePath = Path.Combine(bakingFolder, Path.GetFileNameWithoutExtension(scenePath) + "_baked.unity");

                if (AssetDatabase.LoadAssetAtPath<SceneAsset>(bakedScenePath) != null)
                {
                    scenes.Add(new EditorBuildSettingsScene(bakedScenePath, true));
                }
                else
                {
                    scenes.Add(new EditorBuildSettingsScene(AssetDatabase.GetAssetPath(page.pageScene), true));
                }

                page.runtimeSceneIndex = i + 1;
            }

            EditorBuildSettings.scenes = scenes.ToArray();
            EditorUtility.SetDirty(this);
        }
#endif
    }
}