using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    [ExecuteInEditMode]
    public class PanelView : MonoBehaviour
    {
        [SerializeField]
        Vector2 size = new Vector2(1.0f, 0.7f);
        public Vector2 Size
        {
            get => size;
            set => size = value;
        }

        [SerializeField]
        bool isPolygon = false;

        [SerializeField]
        Vector2[] polygonPoints = Array.Empty<Vector2>();

        [SerializeField]
        Vector2[] sizePoints = new Vector2[4]{
            new Vector2(0, 0.35f),
            new Vector2(0, -0.35f),
            new Vector2(0.5f, 0f),
            new Vector2(-0.5f, 0f)
        };

        [SerializeField]
        Texture2D mask;
        public Texture2D Mask => mask;

        public bool IsPolygon
        {
            get => isPolygon;
            set
            {
                isPolygon = value;
                if (isPolygon && polygonPoints.Length == 0)
                {
                    polygonPoints = new Vector2[4]
                    {
                        new Vector2(-size.x, -size.y) * 0.5f,
                        new Vector2(-size.x, size.y) * 0.5f,
                        new Vector2(size.x, size.y) * 0.5f,
                        new Vector2(size.x, -size.y) * 0.5f,
                    };
                }
            }
        }

        public Vector2[] PolygonPoints
        {
            get
            {
                if (!isPolygon)
                    throw new Exception("Not a polygon");

                return polygonPoints;
            }
            set
            {
                if (!isPolygon)
                    throw new Exception("Not a polygon");

                if (value == null)
                    throw new ArgumentNullException();

                if (value.Length < 3)
                    throw new ArgumentException("Needs to have at least three points");

                polygonPoints = (Vector2[])value.Clone();
            }
        }


        Mesh stencilMesh;

        public Mesh StencilMesh => stencilMesh;

        void OnEnable()
        {
            stencilMesh = new Mesh() { name = "PanelViewMesh" };
            stencilMesh.hideFlags = HideFlags.HideAndDontSave;
            RebuildMesh();
        }

        void OnDisable()
        {
            GameObject.DestroyImmediate(stencilMesh);
        }

        public void RebuildMesh()
        {
            stencilMesh.Clear();

            float depth = 0.02f;

            Vector2 v1 = new Vector2(0, 0);
            Vector2 v2 = new Vector2(0, 0);
            Vector2 v3 = new Vector2(0, 0);
            Vector2 v4 = new Vector2(0, 0);

            if (isPolygon)
            {
                if (polygonPoints.Length != 4 && polygonPoints.Length != 3)
                {
                    Debug.LogError("Panel view only supports polygons with three or four points");
                }

                if (polygonPoints.Length > 0)
                    v1 = polygonPoints[0];

                if (polygonPoints.Length > 1)
                    v2 = polygonPoints[1];

                if (polygonPoints.Length > 2)
                    v3 = polygonPoints[2];

                if (polygonPoints.Length > 3)
                    v4 = polygonPoints[3];
            }
            else
            {
                v1 = new Vector2(-size.x, -size.y) * 0.5f;
                v2 = new Vector2(-size.x, size.y) * 0.5f;
                v3 = new Vector2(size.x, size.y) * 0.5f;
                v4 = new Vector2(size.x, -size.y) * 0.5f;
            }

            Vector3[] vertices = new Vector3[]
            {
                new Vector3(v1.x, v1.y, 0),
                new Vector3(v2.x, v2.y, 0),
                new Vector3(v4.x, v4.y, 0),
                new Vector3(v3.x, v3.y, 0),

                new Vector3(v1.x, v1.y, -depth),
                new Vector3(v2.x, v2.y, -depth),
                new Vector3(v4.x, v4.y, -depth),
                new Vector3(v3.x, v3.y, -depth),
            };

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),

                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),
            };

            int[] indices = new int[6]
            {
                0, 3, 1,
                0, 2, 3,
            };

            int[] indicesCube = new int[]
            {
                4, 7, 5,
                4, 6, 7,
                4, 5, 1,
                4, 1, 0,
                6, 3, 7,
                6, 2, 3,
                5, 3, 1,
                5, 7, 3,
                4, 2, 6,
                4, 0, 2,
            };

            stencilMesh.subMeshCount = 2;
            stencilMesh.vertices = vertices;
            stencilMesh.uv = uvs;
            stencilMesh.SetIndices(indices, MeshTopology.Triangles, 0);
            stencilMesh.SetIndices(indicesCube, MeshTopology.Triangles, 1);
            stencilMesh.UploadMeshData(false);
        }


#if UNITY_EDITOR
        [HideInInspector]
        public Vector3 originalPosition = Vector3.zero;

        [HideInInspector]
        public Quaternion originalRotation = Quaternion.identity;

        void OnDrawGizmos()
        {
            Gizmos.matrix = transform.localToWorldMatrix;
            Gizmos.color = Color.red;
            Panel panel = GetComponentInParent<Panel>();
            if (panel != null && panel.isInEditMode)
            {
                Gizmos.color = Color.green;
            }

            if (isPolygon)
            {
                for (int i = 0; i < polygonPoints.Length; i++)
                {
                    Gizmos.DrawLine(polygonPoints[i], polygonPoints[(i + 1) % polygonPoints.Length]);
                }
            }
            else
            {
                Gizmos.DrawWireCube(new Vector3(0, 0, 0), new Vector3(size.x, size.y, 0));
            }
        }

        void Update()
        {
            if (!Application.isPlaying)
            {
                RebuildMesh();
            }
        }
#endif

    }
}