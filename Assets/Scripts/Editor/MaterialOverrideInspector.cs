using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Icebreakers.Editor
{
    [CustomEditor(typeof(MaterialOverride))]
    public class MaterialOverrideInspector : UnityEditor.Editor
    {
        SerializedProperty overrideLightDirection;
        SerializedProperty lightDirection;
        SerializedProperty lightDirectionQuaternion;

        SerializedProperty overrideAlbedo;
        SerializedProperty albedo;

        SerializedProperty overrideColor;
        SerializedProperty color;

        SerializedProperty overrideUseShadowmap;
        SerializedProperty useShadowmap;

        SerializedProperty overrideShadowAlbedo;
        SerializedProperty shadowAlbedo;

        SerializedProperty overrideShadowColor;
        SerializedProperty shadowColor;

        SerializedProperty overrideHatchingLineCount;
        SerializedProperty hatchingLineCount;

        SerializedProperty overrideHatchingRatio;
        SerializedProperty hatchingRatio;

        SerializedProperty overrideShadowOutlineWidth;
        SerializedProperty shadowOutlineWidth;

        void OnEnable()
        {
            overrideLightDirection = serializedObject.FindProperty("overrideLightDirection");
            lightDirection = serializedObject.FindProperty("lightDirection");
            lightDirectionQuaternion = serializedObject.FindProperty("lightDirectionQuaternion");
            overrideAlbedo = serializedObject.FindProperty("overrideAlbedo");
            albedo = serializedObject.FindProperty("albedo");
            overrideColor = serializedObject.FindProperty("overrideColor");
            color = serializedObject.FindProperty("color");
            overrideUseShadowmap = serializedObject.FindProperty("overrideUseShadowmap");
            useShadowmap = serializedObject.FindProperty("useShadowmap");
            overrideShadowAlbedo = serializedObject.FindProperty("overrideShadowAlbedo");
            shadowAlbedo = serializedObject.FindProperty("shadowAlbedo");
            overrideShadowColor = serializedObject.FindProperty("overrideShadowColor");
            shadowColor = serializedObject.FindProperty("shadowColor");
            overrideHatchingLineCount = serializedObject.FindProperty("overrideHatchingLineCount");
            hatchingLineCount = serializedObject.FindProperty("hatchingLineCount");
            overrideHatchingRatio = serializedObject.FindProperty("overrideHatchingRatio");
            hatchingRatio = serializedObject.FindProperty("hatchingRatio");
            overrideShadowOutlineWidth = serializedObject.FindProperty("overrideShadowOutlineWidth");
            shadowOutlineWidth = serializedObject.FindProperty("shadowOutlineWidth");
        }

        public override void OnInspectorGUI()
        {
            serializedObject.UpdateIfRequiredOrScript();

            //OveridePropertyField(overrideLightDirection, lightDirection);
            OveridePropertyField(overrideLightDirection, lightDirectionQuaternion);
            OveridePropertyField(overrideAlbedo, albedo);
            OveridePropertyField(overrideColor, color);
            OveridePropertyField(overrideUseShadowmap, useShadowmap);
            OveridePropertyField(overrideShadowAlbedo, shadowAlbedo);
            OveridePropertyField(overrideShadowColor, shadowColor);
            OveridePropertyField(overrideHatchingLineCount, hatchingLineCount);
            OveridePropertyField(overrideHatchingRatio, hatchingRatio);
            OveridePropertyField(overrideShadowOutlineWidth, shadowOutlineWidth);

            serializedObject.ApplyModifiedProperties();
        }

        void OveridePropertyField(SerializedProperty overrideProperty, SerializedProperty property)
        {
            Rect rect = EditorGUILayout.GetControlRect(true, 20);
            Rect toggleRect = rect.FromLeft(20);
            toggleRect.height = 18;
            EditorGUI.PropertyField(toggleRect, overrideProperty, GUIContent.none);
            GUI.enabled = overrideProperty.boolValue;
            EditorGUI.PropertyField(rect, property);
            GUI.enabled = true;
        }

        void OnSceneGUI()
        {
            serializedObject.UpdateIfRequiredOrScript();

            if (overrideLightDirection.boolValue)
            {
                MaterialOverride target = (MaterialOverride)this.target;
                Vector3 position = target.transform.position + new Vector3(0, 0.5f, 0);
                if (target.TryGetComponent<Renderer>(out Renderer renderer))
                {
                    Bounds bounds = renderer.bounds;
                    position = bounds.center;
                }
                lightDirectionQuaternion.quaternionValue = Handles.DoRotationHandle(lightDirectionQuaternion.quaternionValue, position);
                lightDirection.vector3Value = Quaternion.Inverse(target.transform.rotation) * lightDirectionQuaternion.quaternionValue * target.transform.forward;

            }

            serializedObject.ApplyModifiedProperties();
        }
    }
}