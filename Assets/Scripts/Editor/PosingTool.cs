using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using UnityEditor;
using UnityEngine;
using UnityEngine.Rendering;

namespace Icebreakers.Editor
{
    public class PosingTool
    {
        bool isInPoseMode = false;
        public PosableCharacter posableCharacter;
        string activeBoneName;
        List<string> selectedBoneNames = new List<string>();
        List<PoseBone> children = new List<PoseBone>();

        /*[Serializable]
        public struct BoneLayer
        {
            public string name;
            public bool visible;
        }

        public List<BoneLayer> layers = new List<BoneLayer>();*/

        public bool IsInPoseMode => isInPoseMode;

        public static bool ToolsHidden
        {
            get
            {
                Type type = typeof(Tools);
                FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
                return ((bool)field.GetValue(null));
            }
            set
            {
                Type type = typeof(Tools);
                FieldInfo field = type.GetField("s_Hidden", BindingFlags.NonPublic | BindingFlags.Static);
                field.SetValue(null, value);
            }
        }

        public void EnterPoseMode(PosableCharacter posableCharacter)
        {
            if (posableCharacter == null)
                throw new ArgumentNullException(nameof(posableCharacter));

            if (isInPoseMode)
                throw new Exception("Already in pose mode");

            SceneView.duringSceneGui += OnPoseModeSceneGUI;
            isInPoseMode = true;
            this.posableCharacter = posableCharacter;
            ToolsHidden = true;
            SceneView.RepaintAll();
        }

        public void LeavePoseMode()
        {
            if (!isInPoseMode)
                throw new Exception("Not in pose mode");

            SceneView.duringSceneGui -= OnPoseModeSceneGUI;
            isInPoseMode = false;
            activeBoneName = null;
            posableCharacter = null;
            posableCharacter = null;
            ToolsHidden = false;
            SceneView.RepaintAll();
        }

        public static void RestoreRestPose(PosableCharacter posableCharacter)
        {
            Undo.RecordObjects(posableCharacter.PoseBones.Select(x => x.transform).ToArray(), "Restore Rest Pose");
            foreach (var poseBone in posableCharacter.PoseBones)
            {
                poseBone.transform.localPosition = poseBone.restPosition;
                poseBone.transform.localRotation = poseBone.restRotation;
                poseBone.transform.localScale = poseBone.restScale;
            }
        }

        Quaternion lastBoneRotation = Quaternion.identity;

        public void OnPoseModeSceneGUI(SceneView sceneView)
        {
            if (posableCharacter == null)
            {
                LeavePoseMode();
                return;
            }

            PoseBone activeBone = posableCharacter.FindBone(activeBoneName);
            List<PoseBone> selectedBones = new List<PoseBone>();
            foreach (var boneName in selectedBoneNames)
            {
                PoseBone poseBone = posableCharacter.FindBone(boneName);
                if (poseBone != null)
                {
                    selectedBones.Add(poseBone);
                }
            }

            int id = GUIUtility.GetControlID("PoseMode".GetHashCode(), FocusType.Passive);
            Event evt = Event.current;

            if (evt.GetTypeForControl(id) == EventType.Repaint)
            {
                DrawBones(posableCharacter, sceneView.camera, selectedBones, activeBone);
            }

            if (evt.type == EventType.MouseUp)
            {
                lastBoneRotation = Quaternion.identity;
            }

            Handles.BeginGUI();

            bool showBoneSettings = EditorPrefs.GetBool("Icebreakers/ShowBoneSettings", true);
            float height = 330;
            if (activeBone == null)
            {
                height = 110;
            }
            else if (!showBoneSettings)
            {
                height = 130;
            }
            using (new GUILayout.AreaScope(new Rect(10, 10, 300, height), "Bone Settings", GUI.skin.window))
            {
                EditorGUILayout.LabelField("Selected Bone", activeBone != null ? activeBone.name : "None");

                GUI.enabled = activeBone != null;
                if (GUILayout.Button("Reset Position"))
                {
                    ResetSelectedBonesPosition(selectedBones);
                }

                if (GUILayout.Button("Reset Rotation"))
                {
                    ResetSelectedBonesRotation(selectedBones);
                }

                if (GUILayout.Button("Reset Scale"))
                {
                    ResetSelectedBonesScale(selectedBones);
                }
                GUI.enabled = true;

                if (activeBone != null)
                {
                    EditorGUI.BeginChangeCheck();
                    showBoneSettings = EditorGUILayout.Foldout(showBoneSettings, "Advanced");
                    if (EditorGUI.EndChangeCheck())
                    {
                        EditorPrefs.SetBool("Icebreakers/ShowBoneSettings", showBoneSettings);
                    }

                    if (showBoneSettings)
                    {
                        Undo.RecordObject(posableCharacter, "Change Bone Settings");
                        posableCharacter.FindChildrenOfBone(activeBone, children);

                        ////
                        string[] options = new string[posableCharacter.layers.Count];
                        for (int i = 0; i < options.Length; i++)
                        {
                            options[i] = posableCharacter.layers[i].name;
                        }
                        int index = EditorGUILayout.Popup("Bone Layer", activeBone.boneLayer, options);
                        if (index != -1)
                        {
                            activeBone.boneLayer = index;
                        }
                        ////

                        if (children.Count == 1)
                        {
                            if (GUILayout.Button("Set Length To Child"))
                            {
                                PoseBone child = children[0];
                                activeBone.length = Vector3.Dot(child.transform.localPosition, PosableCharacter.BoneAxisToVector(posableCharacter.PrimaryBoneAxis));
                            }
                        }
                        else if (children.Count > 1)
                        {
                            options = new string[children.Count];
                            for (int i = 0; i < options.Length; i++)
                            {
                                options[i] = children[i].name;
                            }
                            index = EditorGUILayout.Popup("Set Length To Child", -1, options);
                            if (index != -1)
                            {
                                PoseBone child = children[index];
                                activeBone.length = Vector3.Dot(child.transform.localPosition, PosableCharacter.BoneAxisToVector(posableCharacter.PrimaryBoneAxis));
                            }
                        }
                        activeBone.length = EditorGUILayout.FloatField("Bone Length", activeBone.length);
                        activeBone.color = EditorGUILayout.ColorField("Bone Color", activeBone.color);
                        activeBone.enablePosition = EditorGUILayout.Toggle("Enable Position", activeBone.enablePosition);
                        activeBone.enableRotation = EditorGUILayout.Toggle("Enable Rotation", activeBone.enableRotation);
                        activeBone.enableScale = EditorGUILayout.Toggle("Enable Scale", activeBone.enableScale);

                        EditorGUI.BeginChangeCheck();
                        activeBone.enableIK = EditorGUILayout.Toggle("Enable IK", activeBone.enableIK);
                        GUI.enabled = activeBone.enableIK;
                        activeBone.ikChainLength = EditorGUILayout.IntSlider("IK Chain Length", activeBone.ikChainLength, 0, 3);

                        PoseBone ikTargetBone = posableCharacter.FindBone(activeBone.ikTargetName);
                        EditorGUI.BeginChangeCheck();
                        Transform ikTarget = EditorGUILayout.ObjectField("IK Target", ikTargetBone?.transform, typeof(Transform), true) as Transform;
                        if (EditorGUI.EndChangeCheck())
                        {
                            if (ikTarget == null)
                            {
                                activeBone.ikTargetName = null;
                            }
                            else
                            {
                                ikTargetBone = posableCharacter.FindBone(ikTarget.name);
                                if (ikTargetBone != null)
                                {
                                    activeBone.ikTargetName = ikTarget.name;
                                }
                            }
                        }

                        PoseBone ikPoleTargetBone = posableCharacter.FindBone(activeBone.ikPoleTargetName);
                        EditorGUI.BeginChangeCheck();
                        Transform ikPoleTarget = EditorGUILayout.ObjectField("IK PoleTarget", ikPoleTargetBone?.transform, typeof(Transform), true) as Transform;
                        if (EditorGUI.EndChangeCheck())
                        {
                            if (ikPoleTarget == null)
                            {
                                activeBone.ikPoleTargetName = null;
                            }
                            else
                            {
                                ikPoleTargetBone = posableCharacter.FindBone(ikPoleTarget.name);
                                if (ikPoleTargetBone != null)
                                {
                                    activeBone.ikPoleTargetName = ikPoleTarget.name;
                                }
                            }
                        }

                        GUI.enabled = true;
                        if (EditorGUI.EndChangeCheck())
                        {
                            posableCharacter.ResetConstraints();
                        }
                    }
                }
            }

            Handles.EndGUI();

            if (activeBone != null)
            {
                if (!posableCharacter.BoneLayerHidden(activeBone.boneLayer))
                {
                    Handles.matrix = Matrix4x4.identity;

                    EditorGUI.BeginChangeCheck();

                    bool enablePosition = false;
                    bool enableRotation = false;
                    bool enableScale = false;

                    foreach (var poseBone in selectedBones)
                    {
                        enablePosition = activeBone.enablePosition;
                        enableRotation = activeBone.enableRotation;
                        enableScale = activeBone.enableScale;
                    }

                    PoseBone targetBone = activeBone;
                    bool needsRetarget = false;
                    foreach (var poseBone in selectedBones)
                    {
                        if (IsParentOf(poseBone, targetBone))
                        {
                            needsRetarget = true;
                            break;
                        }
                    }

                    if (needsRetarget)
                    {
                        foreach (var poseBone in selectedBones)
                        {
                            if (poseBone == activeBone)
                                continue;

                            bool hasParentInSelection = false;
                            foreach (var potentialParent in selectedBones)
                            {
                                if (IsParentOf(potentialParent, poseBone))
                                {
                                    hasParentInSelection = true;
                                    break;
                                }
                            }

                            if (!hasParentInSelection)
                            {
                                targetBone = poseBone;
                                break;
                            }
                        }

                        Debug.Assert(targetBone != activeBone);
                    }

                    Vector3 currentPosition = targetBone.transform.position;
                    Quaternion currentRotation = targetBone.transform.rotation;
                    Vector3 currentScale = targetBone.transform.localScale;

                    if (Tools.current == Tool.Move)
                    {
                        if (enablePosition)
                        {
                            if (Tools.pivotRotation == PivotRotation.Global)
                            {
                                currentPosition = Handles.DoPositionHandle(currentPosition, Quaternion.identity);
                            }
                            else
                            {
                                currentPosition = Handles.DoPositionHandle(currentPosition, currentRotation);
                            }
                        }
                    }
                    else if (Tools.current == Tool.Rotate)
                    {
                        if (enableRotation)
                        {
                            if (Tools.pivotRotation == PivotRotation.Global)
                            {
                                EditorGUI.BeginChangeCheck();
                                Quaternion newRotation = Handles.DoRotationHandle(Quaternion.identity, currentPosition);
                                if (EditorGUI.EndChangeCheck())
                                {
                                    currentRotation = newRotation * Quaternion.Inverse(lastBoneRotation) * currentRotation;
                                    lastBoneRotation = newRotation;
                                }
                                
                            }
                            else
                            {
                                currentRotation = Handles.DoRotationHandle(currentRotation, currentPosition);
                            }
                        }
                    }
                    else if (Tools.current == Tool.Scale)
                    {
                        if (enableScale)
                        {
                            currentScale = Handles.DoScaleHandle(currentScale, currentPosition, currentRotation, HandleUtility.GetHandleSize(currentPosition));
                        }
                    }
                    else if (Tools.current == Tool.Transform)
                    {
                        if (enablePosition && enableRotation && enableScale)
                        {
                            Handles.TransformHandle(ref currentPosition, ref currentRotation, ref currentScale);
                        }
                        else if (enablePosition && enableRotation)
                        {
                            Handles.TransformHandle(ref currentPosition, ref currentRotation);
                        }
                        else if (enablePosition)
                        {
                            currentPosition = Handles.DoPositionHandle(currentPosition, currentRotation);
                        }
                        else if (enableRotation)
                        {
                            currentRotation = Handles.DoRotationHandle(currentRotation, currentPosition);
                        }
                        else if (enableScale)
                        {
                            currentScale = Handles.DoScaleHandle(currentScale, currentPosition, currentRotation, HandleUtility.GetHandleSize(currentPosition));
                        }
                        else
                        {
                            Handles.TransformHandle(ref currentPosition, ref currentRotation, ref currentScale);
                        }
                    }

                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObjects(posableCharacter.PoseBones.Select(x => x.transform).ToArray(), "Transform Bone");

                        Vector3 prevLocalPosition = targetBone.transform.localPosition;
                        Quaternion prevLocalRotation = targetBone.transform.localRotation;
                        Vector3 prevLocalScale = targetBone.transform.localScale;

                        targetBone.transform.position = currentPosition;
                        targetBone.transform.rotation = currentRotation;
                        targetBone.transform.localScale = currentScale;

                        Vector3 deltaPosition = targetBone.transform.localPosition - prevLocalPosition;
                        Quaternion deltaRotation = Quaternion.Inverse(prevLocalRotation) * targetBone.transform.localRotation;
                        Vector3 deltaScale = new Vector3(
                            targetBone.transform.localScale.x / prevLocalScale.x,
                            targetBone.transform.localScale.y / prevLocalScale.y,
                            targetBone.transform.localScale.z / prevLocalScale.z
                        );

                        targetBone.transform.localPosition = prevLocalPosition;
                        targetBone.transform.localRotation = prevLocalRotation;
                        targetBone.transform.localScale = prevLocalScale;

                        foreach (PoseBone poseBone in selectedBones)
                        {
                            if (poseBone.enablePosition)
                            {
                                poseBone.transform.localPosition += deltaPosition;
                            }

                            if (poseBone.enableRotation)
                            {
                                poseBone.transform.localRotation *= deltaRotation;
                            }

                            if (poseBone.enableScale)
                            {
                                poseBone.transform.localScale = new Vector3(
                                    poseBone.transform.localScale.x * deltaScale.x,
                                    poseBone.transform.localScale.y * deltaScale.y,
                                    poseBone.transform.localScale.z * deltaScale.z
                                );
                            }
                        }

                        posableCharacter.UpdateConstraints();
                    }

                    if (evt.GetTypeForControl(id) == EventType.KeyDown)
                    {
                        if (evt.keyCode == KeyCode.W && evt.control)
                        {
                            ResetSelectedBonesPosition(selectedBones);
                            evt.Use();
                        }

                        if (evt.keyCode == KeyCode.E && evt.control)
                        {
                            ResetSelectedBonesRotation(selectedBones);
                            evt.Use();
                        }

                        if (evt.keyCode == KeyCode.R && evt.control)
                        {
                            ResetSelectedBonesScale(selectedBones);
                            evt.Use();
                        }
                    }

                    if (EditorGUI.EndChangeCheck())
                    {
                        posableCharacter.UpdateConstraints();
                    }
                }
            }

            switch (evt.GetTypeForControl(id))
            {
                case EventType.MouseDown:
                    if (evt.button == 0 && !Tools.viewToolActive)
                    {
                        PoseBone pickedBone = PickBone(posableCharacter, sceneView.camera);
                        if (pickedBone == null)
                        {
                            selectedBones.Clear();
                            selectedBoneNames.Clear();
                            activeBone = null;
                            activeBoneName = null;
                        }
                        else
                        {
                            if (posableCharacter.BoneLayerHidden(pickedBone.boneLayer))
                            {
                                selectedBones.Clear();
                                selectedBoneNames.Clear();
                                activeBone = null;
                                activeBoneName = null;
                            }
                            else
                            {
                                if (!evt.control)
                                {
                                    selectedBones.Clear();
                                    selectedBoneNames.Clear();
                                }

                                if (!selectedBones.Contains(pickedBone))
                                {
                                    selectedBones.Add(pickedBone);
                                    selectedBoneNames.Add(pickedBone.name);
                                }

                                activeBone = pickedBone;
                                activeBoneName = pickedBone.name;
                            }
                        }

                        sceneView.Repaint();
                    }
                    break;

                case EventType.MouseUp:
                    break;

                case EventType.KeyDown:
                    if (evt.keyCode == KeyCode.F)
                    {
                        if (activeBone != null)
                        {
                            Bounds bounds = new Bounds(activeBone.transform.position, Vector3.one * activeBone.length * 2);
                            sceneView.Frame(bounds, instant: false);
                        }
                        evt.Use();
                    }
                    break;
            }

            if (evt.type == EventType.Layout)
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));
        }

        static bool IsParentOf(PoseBone potentialParent, PoseBone child)
        {
            if (potentialParent == null)
                throw new ArgumentNullException(nameof(potentialParent));

            if (potentialParent == child)
                return false;

            PoseBone parent = child.parent;
            while (parent != null)
            {
                if (parent == potentialParent)
                {
                    return true;
                }

                parent = parent.parent;
            }

            return false;
        }

        void ResetSelectedBonesPosition(List<PoseBone> selectedBones)
        {
            Undo.RecordObjects(posableCharacter.PoseBones.Select(x => x.transform).ToArray(), "Reset Bone Position");
            foreach (PoseBone poseBone in selectedBones)
            {
                poseBone.transform.localPosition = poseBone.restPosition;
            }
            posableCharacter.UpdateConstraints();
        }

        void ResetSelectedBonesRotation(List<PoseBone> selectedBones)
        {
            Undo.RecordObjects(posableCharacter.PoseBones.Select(x => x.transform).ToArray(), "Reset Bone Rotation");
            foreach (PoseBone poseBone in selectedBones)
            {
                poseBone.transform.localRotation = poseBone.restRotation;
            }
            posableCharacter.UpdateConstraints();
        }

        void ResetSelectedBonesScale(List<PoseBone> selectedBones)
        {
            Undo.RecordObjects(posableCharacter.PoseBones.Select(x => x.transform).ToArray(), "Reset Bone Scale");
            foreach (PoseBone poseBone in selectedBones)
            {
                poseBone.transform.localScale = poseBone.restScale;
            }
            posableCharacter.UpdateConstraints();
        }



        List<PosingToolHelper.Triangle> triangles = new List<PosingToolHelper.Triangle>();

        void DrawBones(PosableCharacter posableCharacter, Camera camera, List<PoseBone> selectedBones, PoseBone activeBone)
        {
            Vector3 primaryAxis = PosableCharacter.BoneAxisToVector(posableCharacter.PrimaryBoneAxis);
            Vector3 secondaryAxis = PosableCharacter.BoneAxisToVector(posableCharacter.SecondaryBoneAxis);

            triangles.Clear();

            foreach (PoseBone poseBone in posableCharacter.PoseBones)
            {
                if (!posableCharacter.BoneLayerHidden(poseBone.boneLayer))
                {
                    bool selected = selectedBones.Contains(poseBone);
                    bool active = poseBone == activeBone;
                    float length = poseBone.length;
                    Color baseColor = poseBone.color;
                    Matrix4x4 localToWorldMatrix = poseBone.transform.localToWorldMatrix;

                    Vector3 thirdAxis = Vector3.Cross(primaryAxis, secondaryAxis);

                    float boneSize = length * 0.3f;
                    float boneRadius = length * 0.2f;

                    Vector3 vBase = localToWorldMatrix.MultiplyPoint(new Vector3(0, 0, 0));
                    Vector3 vTip = localToWorldMatrix.MultiplyPoint(primaryAxis * length);

                    Vector3 vBone1 = localToWorldMatrix.MultiplyPoint(primaryAxis * boneSize + secondaryAxis * boneRadius);
                    Vector3 vBone2 = localToWorldMatrix.MultiplyPoint(primaryAxis * boneSize + secondaryAxis * -boneRadius);
                    Vector3 vBone3 = localToWorldMatrix.MultiplyPoint(primaryAxis * boneSize + thirdAxis * boneRadius);
                    Vector3 vBone4 = localToWorldMatrix.MultiplyPoint(primaryAxis * boneSize + thirdAxis * -boneRadius);

                    float alpha = 0.8f;
                    Color faceColor = baseColor * 0.3f;
                    Color outlineColor = baseColor * 0.45f;

                    if (selected)
                    {
                        faceColor = baseColor * 0.6f;
                        outlineColor = baseColor * 0.3f;
                    }

                    if (active)
                    {
                        faceColor = baseColor * 0.9f;
                        outlineColor = baseColor * 0.2f;
                    }

                    faceColor.a = alpha;
                    outlineColor.a = alpha;

                    triangles.Add(new PosingToolHelper.Triangle { v1 = vTip, v2 = vBone1, v3 = vBone3, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vTip, v2 = vBone3, v3 = vBone2, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vTip, v2 = vBone2, v3 = vBone4, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vTip, v2 = vBone4, v3 = vBone1, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vBase, v2 = vBone1, v3 = vBone3, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vBase, v2 = vBone3, v3 = vBone2, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vBase, v2 = vBone2, v3 = vBone4, faceColor = faceColor, outlineColor = outlineColor });
                    triangles.Add(new PosingToolHelper.Triangle { v1 = vBase, v2 = vBone4, v3 = vBone1, faceColor = faceColor, outlineColor = outlineColor });
                }
            }

            Vector3 cameraPosition = camera.transform.position;
            Vector3 forward = camera.transform.forward;
            for (int i = 0; i < triangles.Count; i++)
            {
                PosingToolHelper.Triangle t = triangles[i];
                Vector3 centroid = (t.v1 + t.v2 + t.v3) / 3.0f;
                t.distanceToCamera = Vector3.Dot(centroid - cameraPosition, forward);
                triangles[i] = t;
            }

            triangles.Sort((x, y) => y.distanceToCamera.CompareTo(x.distanceToCamera));

            PosingToolHelper.DrawTrianglesWithOutline(triangles);
        }

        PoseBone PickBone(PosableCharacter posableCharacter, Camera camera)
        {
            Vector3 primaryAxis = PosableCharacter.BoneAxisToVector(posableCharacter.PrimaryBoneAxis);
            Vector3 secondaryAxis = PosableCharacter.BoneAxisToVector(posableCharacter.SecondaryBoneAxis);

            float closestBoneDistance = float.PositiveInfinity;
            PoseBone closestBone = null;

            Vector3 cameraPosition = camera.transform.position;
            Vector3 forward = camera.transform.forward;
            foreach (PoseBone poseBone in GetSortedBones(camera, frontToBack: true))
            {
                float distanceToCamera = Vector3.Dot(poseBone.transform.position - cameraPosition, forward);
                if (distanceToCamera < 0.05f)
                    continue;

                Handles.matrix = poseBone.transform.localToWorldMatrix;
                float distance = PosingToolHelper.DistanceToBone(primaryAxis, secondaryAxis, poseBone.length);
                if (distance < 10 && distance < closestBoneDistance)
                {
                    closestBoneDistance = distance;
                    closestBone = poseBone;
                }
            }

            return closestBone;
        }

        class BoneDistanceComparer : IComparer<float>
        {
            bool frontToBack;
            public BoneDistanceComparer(bool frontToBack)
            {
                this.frontToBack = frontToBack;
            }

            public int Compare(float x, float y)
            {
                if (frontToBack)
                    return x.CompareTo(y);
                else
                    return y.CompareTo(x);
            }
        }


        PoseBone[] GetSortedBones(Camera camera, bool frontToBack)
        {
            PoseBone[] poseBones = posableCharacter.PoseBones.ToArray();
            float[] distances = new float[poseBones.Length];
            Vector3 cameraPosition = camera.transform.position;
            Vector3 forward = camera.transform.forward;
            for (int i = 0; i < poseBones.Length; i++)
            {
                PoseBone poseBone = poseBones[i];
                float distanceToCamera = Vector3.Dot(poseBone.transform.position - cameraPosition, forward);
                distances[i] = distanceToCamera;
            }

            Array.Sort(distances, poseBones, new BoneDistanceComparer(frontToBack));
            return poseBones;
        }
    }

    static class PosingToolHelper
    {
        static Func<Vector3[], float> pDistanceToPointCloudConvexHull;
        public static float DistanceToPointCloudConvexHull(Vector3[] points)
        {
            if (pDistanceToPointCloudConvexHull == null)
            {
                pDistanceToPointCloudConvexHull = (Func<Vector3[], float>)typeof(HandleUtility).GetMethod("DistanceToPointCloudConvexHull",
                    System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic)
                    .CreateDelegate(typeof(Func<Vector3[], float>));
            }

            return pDistanceToPointCloudConvexHull(points);
        }

        static Action<CompareFunction> pApplyWireMaterial;
        public static void ApplyWireMaterial(CompareFunction zTest)
        {
            if (pApplyWireMaterial == null)
            {
                pApplyWireMaterial = (Action<CompareFunction>)typeof(HandleUtility).GetMethod("ApplyWireMaterial",
                    System.Reflection.BindingFlags.Static | System.Reflection.BindingFlags.Public | System.Reflection.BindingFlags.NonPublic,
                    null, new Type[] { typeof(CompareFunction) }, null)
                    .CreateDelegate(typeof(Action<CompareFunction>));
            }

            pApplyWireMaterial(zTest);
        }

        static Vector3[] bonePointCloud = new Vector3[6];
        public static float DistanceToBone(Vector3 primaryAxis, Vector3 secondaryAxis, float length)
        {
            Vector3 thirdAxis = Vector3.Cross(primaryAxis, secondaryAxis);

            float boneSize = length * 0.3f;
            float boneRadius = length * 0.2f;

            bonePointCloud[0] = new Vector3(0, 0, 0);
            bonePointCloud[1] = primaryAxis * length;

            bonePointCloud[2] = primaryAxis * boneSize + secondaryAxis * boneRadius;
            bonePointCloud[3] = primaryAxis * boneSize + secondaryAxis * -boneRadius;
            bonePointCloud[4] = primaryAxis * boneSize + thirdAxis * boneRadius;
            bonePointCloud[5] = primaryAxis * boneSize + thirdAxis * -boneRadius;

            return DistanceToPointCloudConvexHull(bonePointCloud);
        }

        public static void DrawBone(Vector3 primaryAxis, Vector3 secondaryAxis, float length, Color baseColor, bool selected, bool active)
        {
            Vector3 thirdAxis = Vector3.Cross(primaryAxis, secondaryAxis);

            float boneSize = length * 0.3f;
            float boneRadius = length * 0.2f;

            Vector3 vBase = new Vector3(0, 0, 0);
            Vector3 vTip = primaryAxis * length;

            Vector3 vBone1 = primaryAxis * boneSize + secondaryAxis * boneRadius;
            Vector3 vBone2 = primaryAxis * boneSize + secondaryAxis * -boneRadius;
            Vector3 vBone3 = primaryAxis * boneSize + thirdAxis * boneRadius;
            Vector3 vBone4 = primaryAxis * boneSize + thirdAxis * -boneRadius;

            float alpha = 0.8f;
            Color faceColor = baseColor * 0.3f;
            Color outlineColor = baseColor * 0.45f;

            if (selected)
            {
                faceColor = baseColor * 0.6f;
                outlineColor = baseColor * 0.3f;
            }

            if (active)
            {
                faceColor = baseColor * 0.9f;
                outlineColor = baseColor * 0.2f;
            }

            faceColor.a = alpha;
            outlineColor.a = alpha;

            DrawTriangle(vTip, vBone1, vBone3, faceColor, outlineColor);
            DrawTriangle(vTip, vBone3, vBone2, faceColor, outlineColor);
            DrawTriangle(vTip, vBone2, vBone4, faceColor, outlineColor);
            DrawTriangle(vTip, vBone4, vBone1, faceColor, outlineColor);
            DrawTriangle(vBase, vBone1, vBone3, faceColor, outlineColor);
            DrawTriangle(vBase, vBone3, vBone2, faceColor, outlineColor);
            DrawTriangle(vBase, vBone2, vBone4, faceColor, outlineColor);
            DrawTriangle(vBase, vBone4, vBone1, faceColor, outlineColor);
        }

        static Vector3[] rectangleVertices = new Vector3[4];
        public static void DrawTriangle(Vector3 v1, Vector3 v2, Vector3 v3, Color faceColor, Color outlineColor)
        {
            rectangleVertices[0] = v1;
            rectangleVertices[1] = v2;
            rectangleVertices[2] = v3;
            rectangleVertices[3] = v1;
            Handles.DrawSolidRectangleWithOutline(rectangleVertices, faceColor, outlineColor);
        }

        public struct Triangle
        {
            public Vector3 v1;
            public Vector3 v2;
            public Vector3 v3;
            public Color faceColor;
            public Color outlineColor;
            public float distanceToCamera;
        }

        static Mesh triangleMesh;
        static List<Vector3> vertices = new List<Vector3>();
        static List<Color> colors = new List<Color>();
        static List<int> indices = new List<int>();
        public static void DrawTrianglesWithOutline(List<Triangle> triangles)
        {
            if (Event.current.type != EventType.Repaint)
                return;

#if false
            if (triangleMesh == null)
            {
                triangleMesh = new Mesh() { name = "Posing Tool Triangle Mesh" };
                triangleMesh.MarkDynamic();
            }

            ApplyWireMaterial(Handles.zTest);

            triangleMesh.Clear();

            vertices.Clear();
            for (int i = 0; i < triangles.Count; i++)
            {
                Triangle t = triangles[i];
                vertices.Add(t.v1);
                vertices.Add(t.v2);
                vertices.Add(t.v3);
            }
            triangleMesh.SetVertices(vertices);

            colors.Clear();
            for (int i = 0; i < triangles.Count; i++)
            {
                Triangle t = triangles[i];
                colors.Add(t.faceColor);
                colors.Add(t.faceColor);
                colors.Add(t.faceColor);
            }
            triangleMesh.SetColors(colors);

            indices.Clear();
            for (int i = 0; i < triangles.Count; i++)
            {
                indices.Add(i * 3 + 0);
                indices.Add(i * 3 + 1);
                indices.Add(i * 3 + 2);
                indices.Add(i * 3 + 0);
                indices.Add(i * 3 + 2);
                indices.Add(i * 3 + 1);
            }

            triangleMesh.SetIndices(indices, MeshTopology.Triangles, 0);

            triangleMesh.UploadMeshData(false);

            Graphics.DrawMeshNow(triangleMesh, Handles.matrix);

            colors.Clear();
            for (int i = 0; i < triangles.Count; i++)
            {
                Triangle t = triangles[i];
                colors.Add(t.outlineColor);
                colors.Add(t.outlineColor);
                colors.Add(t.outlineColor);
            }
            triangleMesh.SetColors(colors);

            indices.Clear();
            for (int i = 0; i < triangles.Count; i++)
            {
                indices.Add(i * 3 + 0);
                indices.Add(i * 3 + 1);
                indices.Add(i * 3 + 1);
                indices.Add(i * 3 + 2);
                indices.Add(i * 3 + 2);
                indices.Add(i * 3 + 0);
            }

            triangleMesh.SetIndices(indices, MeshTopology.Lines, 0);
            triangleMesh.UploadMeshData(false);

            Graphics.DrawMeshNow(triangleMesh, Handles.matrix);
#else

            GL.PushMatrix();
            GL.MultMatrix(Handles.matrix);

            ApplyWireMaterial(Handles.zTest);
            Color color = Handles.color;

            for (int i = 0; i < triangles.Count; i++)
            {
                Triangle t = triangles[i];

                // Triangles (Draw it twice to ensure render of both front and back faces)
                GL.Begin(GL.TRIANGLES);
                GL.Color(t.faceColor * color);
                GL.Vertex(t.v1);
                GL.Vertex(t.v2);
                GL.Vertex(t.v3);
                GL.Vertex(t.v1);
                GL.Vertex(t.v3);
                GL.Vertex(t.v2);
                GL.End();

                // Outline
                GL.Begin(GL.LINES);
                GL.Color(t.outlineColor * color);
                GL.Vertex(t.v1);
                GL.Vertex(t.v2);
                GL.Vertex(t.v2);
                GL.Vertex(t.v3);
                GL.Vertex(t.v3);
                GL.Vertex(t.v1);
                GL.End();
            }

            GL.PopMatrix();

#endif
        }
    }
}