using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEditor;
using Unity.Collections;
using Unity.Profiling;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using System;
using UnityEngine.InputSystem;

namespace Icebreakers.Editor
{
    public class LineDrawingTool : EditorWindow
    {
        UWintab_Tablet tablet;

        [MenuItem("Window/Icebreakers/Line Drawing")]
        static void Initialize()
        {
            var window = EditorWindow.GetWindow<LineDrawingTool>();
            window.titleContent = new GUIContent("Line Drawing");
        }

        bool isInDrawingMode = false;

        NativeBVH<BVHPrimitive> drawingScene;
        Dictionary<int, RendererEntry> renderers = new Dictionary<int, RendererEntry>();

        float brushSize = 4;

        Vector3? lastBrushPosition;
        Vector2? lastMousePosition;
        int lastBrushPrimIndex;
        DrawnLineRenderer activeLineRenderer;

        AnimationCurve pressureCurve;
        float lazyMouseDist;

        Matrix4x4 cameraMatrixOriginal;
        Matrix4x4 cameraMatrixEditor;

        readonly Dictionary<SceneView, Matrix4x4> SceneViewCameraMatrices = new Dictionary<SceneView, Matrix4x4>();
        readonly Dictionary<SceneView, Matrix4x4> SceneViewCameraProjectionMatrices = new Dictionary<SceneView, Matrix4x4>();
        readonly Dictionary<SceneView, Matrix4x4> SceneViewDrawModeMatrices = new Dictionary<SceneView, Matrix4x4>();
        readonly Dictionary<SceneView, cameraTransform2D> SceneView2DCameraTransforms = new Dictionary<SceneView, cameraTransform2D>();
        readonly Dictionary<SceneView, Vector2> SceneViewLazyMousePositions = new Dictionary<SceneView, Vector2>();

        Vector2 cameraOffsetTest;

        class cameraTransform2D
        {
            public Vector2 position;
            public float angle;
            public float scale;
        }

        private void OnEnable()
        {
            if (EditorPrefs.HasKey("Icebreakers_LineTool_LazyMouseDist"))
            {
                lazyMouseDist = EditorPrefs.GetFloat("Icebreakers_LineTool_LazyMouseDist");
            }
        }

        void OnDisable()
        {
            if (isInDrawingMode)
            {
                LeaveDrawingMode();
            }
        }

        void OnGUI()
        {
            string text = isInDrawingMode ? "Leave Drawing Mode" : "Enter Drawing Mode";
            bool newIsInDrawingMode = GUILayout.Toggle(isInDrawingMode, text, GUI.skin.button);
            if (newIsInDrawingMode != isInDrawingMode)
            {
                if (newIsInDrawingMode)
                {
                    EnterDrawingMode();
                }
                else
                {
                    LeaveDrawingMode();
                }
            }
            brushSize = EditorGUILayout.Slider("brush Size", brushSize, 1, 20);

            if (pressureCurve == null)
            {
                if (EditorPrefs.HasKey("Icebreakers_LineTool_PressureCurve"))
                {
                    string jsonCurve = EditorPrefs.GetString("Icebreakers_LineTool_PressureCurve");
                    pressureCurve = JsonUtility.FromJson<AnimationCurve>(jsonCurve);
                }
                else
                {
                    pressureCurve = new AnimationCurve();
                    pressureCurve.AddKey(new Keyframe()
                    {
                        inTangent = 0,
                        outTangent = 0,
                        time = 0,
                        value = 0,
                        weightedMode = WeightedMode.Both,
                        outWeight = 0.5f
                    });

                    pressureCurve.AddKey(new Keyframe()
                    {
                        inTangent = 5,
                        outTangent = 0,
                        time = 1,
                        value = 1,
                        weightedMode = WeightedMode.Both,
                        inWeight = 0.2f
                    });
                }
            }

            EditorGUI.BeginChangeCheck();
            pressureCurve = EditorGUILayout.CurveField("Pressure Curve", pressureCurve);
            if (EditorGUI.EndChangeCheck())
            {
                Keyframe first = pressureCurve.keys[0];
                Keyframe last = pressureCurve.keys[pressureCurve.keys.Length - 1];
                first.time = 0;
                first.value = Mathf.Clamp01(first.value);
                first.weightedMode = WeightedMode.Both;
                last.time = 1;
                last.value = Mathf.Clamp01(last.value);
                last.weightedMode = WeightedMode.Both;
                pressureCurve.MoveKey(0, first);
                pressureCurve.MoveKey(pressureCurve.keys.Length - 1, last);
                string jsonCurve = JsonUtility.ToJson(pressureCurve);
                EditorPrefs.SetString("Icebreakers_LineTool_PressureCurve", jsonCurve);
            }

            EditorGUI.BeginChangeCheck();
            lazyMouseDist = EditorGUILayout.FloatField("LazyMouseDistance", lazyMouseDist);
            if (EditorGUI.EndChangeCheck())
            {
                EditorPrefs.SetFloat("Icebreakers_LineTool_LazyMouseDist", lazyMouseDist);
            }


            cameraOffsetTest = EditorGUILayout.Vector2Field("camera offset Test", cameraOffsetTest);
            //Debug.Log(pressureCurve.Evaluate(GetPenPressure()));
        }

        void EnterDrawingMode()
        {
            isInDrawingMode = true;
            SceneView.duringSceneGui += OnSceneGUI;
            if(tablet == null) tablet = new UWintab_Tablet();
            //tablet.Init();
            SceneView.RepaintAll();
        }

        void LeaveDrawingMode()
        {
            isInDrawingMode = false;
            SceneView.duringSceneGui -= OnSceneGUI;
            SceneView.duringSceneGui += CleanSceneViewMatrices;
            //tablet.Deinit();
            activeLineRenderer = null;
            ReleaseDrawingStructure();
            SceneViewDrawModeMatrices.Clear();
            SceneViewCameraProjectionMatrices.Clear();
            SceneView2DCameraTransforms.Clear();
            SceneViewLazyMousePositions.Clear();
            SceneView.RepaintAll();
        }

        void CleanSceneViewMatrices(SceneView sceneView)
        {
            Matrix4x4 originalCamMatrix;
            if (SceneViewCameraMatrices.TryGetValue(sceneView, out originalCamMatrix))
            {
                sceneView.camera.ResetWorldToCameraMatrix();
                sceneView.camera.ResetProjectionMatrix();
                SceneViewCameraMatrices.Remove(sceneView);
            }
            if (SceneViewCameraMatrices.Count == 0)
            {
                SceneView.duringSceneGui -= CleanSceneViewMatrices;
            }
        }

        bool makingStroke = false;
        bool isEraser = false;
        float lastBrushPressure = 0;
        int currentLineStart = 0;
        Vector2 currentDragOrigin, currentMouseOrigin;
        float currentZoomOrigin, currentRotateOrigin;
        bool penDown, penUsedForSelection;

        //List<Vector3> testPositions = new List<Vector3>();

        private void Update()
        {
            SceneView.RepaintAll();
        }

        //List<Vector3> testPositions = new List<Vector3>();

        /*List<Stroke> rebuildStrokes(List<DrawnLine> lines)
        {
            List<Stroke> newStrokeList = new List<Stroke>();
            List<DrawnLine> currentStrokeList = new List<DrawnLine>();
            int currentIndex = 0;
            for (int i = 0; i < lines.Count; i++)
            {
                DrawnLine dl = lines[i];
                currentStrokeList.Add(dl);
                if (dl.end)
                {
                    Stroke currentStroke = new Stroke()
                    {
                        bounds = StrokeBounds(currentStrokeList),
                        index = currentIndex,
                        length = currentStrokeList.Count
                    };
                    newStrokeList.Add(currentStroke);
                    currentIndex = i + 1;
                    currentStrokeList.Clear();
                }
            }
            return newStrokeList;
        }*/

        void printMatrix(Matrix4x4 mat)
        {
            Debug.LogFormat(
                $"{mat.m00:f4},  {mat.m01:f4},  {mat.m02:f4},  {mat.m03:f4}\n" +
                $"{mat.m10:f4},  {mat.m11:f4},  {mat.m12:f4},  {mat.m13:f4}\n" +
                $"{mat.m20:f4},  {mat.m21:f4},  {mat.m22:f4},  {mat.m23:f4}\n" +
                $"{mat.m30:f4},  {mat.m31:f4},  {mat.m32:f4},  {mat.m33:f4}");
        }

        float GetPenPressure()
        {
            if (Pen.current != null)
            {
                return Pen.current.pressure.ReadValue();
            }

            return 0;
        }

        float GetEraser()
        {
            if (Pen.current != null)
            {
                return Pen.current.eraser.ReadValue();
            }

            return 0;
        }

        void OnSceneGUI(SceneView sceneView)
        {
            Event evt = Event.current;

            if (!SceneViewCameraMatrices.ContainsKey(sceneView))
            {
                SceneViewCameraProjectionMatrices.Add(sceneView, sceneView.camera.projectionMatrix);
                SceneViewCameraMatrices.Add(sceneView, sceneView.camera.worldToCameraMatrix);
                SceneViewDrawModeMatrices.Add(sceneView, sceneView.camera.worldToCameraMatrix);
                SceneViewLazyMousePositions.Add(sceneView, evt.mousePosition);
                SceneView2DCameraTransforms.Add(sceneView, new cameraTransform2D()
                {
                    angle = 0,
                    scale = 0,
                    position = new Vector2(0,0)
                });
            }

            Matrix4x4 drawModeMatrix;
            if (SceneViewDrawModeMatrices.TryGetValue(sceneView, out drawModeMatrix))
            {
                sceneView.camera.worldToCameraMatrix = drawModeMatrix;
            }

            Vector2 lazyMousePos = evt.mousePosition;
            SceneViewLazyMousePositions.TryGetValue(sceneView, out lazyMousePos);

            cameraTransform2D currentCameraTransform2D;
            if (SceneView2DCameraTransforms.TryGetValue(sceneView, out currentCameraTransform2D))
            {
                Matrix4x4 drawModeProjectionMatrix;
                if (SceneViewCameraProjectionMatrices.TryGetValue(sceneView, out drawModeProjectionMatrix))
                {
                    Vector2 position = currentCameraTransform2D.position;
                    Matrix4x4 translateMatrix2D = Matrix4x4.Translate(new Vector3(position.x, position.y, 0));

                    float scale = math.pow(2, currentCameraTransform2D.scale);
                    Matrix4x4 scaleMatrix2D = new Matrix4x4(
                        new Vector4(scale, 0, 0, 0),
                        new Vector4(0, scale, 0, 0),
                        new Vector4(0, 0, 1, 0),
                        new Vector4(0, 0, 0, 1)
                        );

                    /*float angle = currentCameraTransform2D.angle;
                    Matrix4x4 rotateMatrix2D = new Matrix4x4(
                        new Vector4(math.cos(angle), -math.sin(angle), 0, 0),
                        new Vector4(math.sin(angle), math.cos(angle), 0, 0),
                        new Vector4(0, 0, 1, 0),
                        new Vector4(0, 0, 0, 1)
                        );*/


                    sceneView.camera.projectionMatrix = translateMatrix2D * scaleMatrix2D /** rotateMatrix2D*/ * drawModeProjectionMatrix;

                    //?
                    //printMatrix(sceneView.camera.projectionMatrix);
                    //printMatrix(sceneView.camera.projectionMatrix);
                }
            }

            float brushPressure = pressureCurve.Evaluate(GetPenPressure());
            Vector2 lazyVec = (lazyMousePos - evt.mousePosition);
            lazyMousePos = evt.mousePosition + (lazyVec.normalized * Mathf.Clamp(lazyVec.magnitude, 0, lazyMouseDist));

            SceneViewLazyMousePositions.Remove(sceneView);
            SceneViewLazyMousePositions.Add(sceneView, lazyMousePos);

            Vector3 cameraOrigin = Camera.current.transform.position;

            Vector2 mousePos01 = evt.mousePosition;
            mousePos01 /= new Vector2(sceneView.position.width, sceneView.position.height - EditorStyles.toolbar.fixedHeight);
            mousePos01 = (mousePos01 * 2) - new Vector2(1, 1);

            if (Keyboard.current.spaceKey.isPressed) {
                if (evt.control || evt.command) EditorGUIUtility.AddCursorRect(new Rect(0, 0, 10000, 10000), MouseCursor.Zoom);
                else EditorGUIUtility.AddCursorRect(new Rect(0,0,10000,10000), MouseCursor.Pan);
                if (evt.type == EventType.MouseDrag && evt.button == 0)
                {
                    if(evt.control || evt.command)
                    {
                        currentCameraTransform2D.scale = currentZoomOrigin + (mousePos01.x - currentMouseOrigin.x) * 10;
                        float zoomScale = math.pow(2, currentCameraTransform2D.scale) / math.pow(2, currentZoomOrigin);
                        Vector2 flippedMouseOrigin = currentMouseOrigin * new Vector2(1, -1);
                        Vector2 offsetFromZoomOrigin = currentDragOrigin - flippedMouseOrigin;
                        currentCameraTransform2D.position = flippedMouseOrigin + offsetFromZoomOrigin * zoomScale;
                    }/*else if (evt.shift)
                    {
                        currentCameraTransform2D.angle = currentRotateOrigin + (mousePos01.x - currentMouseOrigin.x) * 3;
                        //float angleOffset = currentCameraTransform2D.angle - currentRotateOrigin;
                        //Vector2 flippedMouseOrigin = currentMouseOrigin * new Vector2(1, -1);
                        //Vector2 offsetFromRotateOrigin = currentDragOrigin - flippedMouseOrigin;
                        //float sin = math.sin(angleOffset), cos = math.cos(angleOffset);
                        //offsetFromRotateOrigin =
                        //currentCameraTransform2D.position = flippedMouseOrigin + offsetFromRotateOrigin;
                    }*/
                    else currentCameraTransform2D.position = currentDragOrigin + ((mousePos01 - currentMouseOrigin) * new Vector2(1,-1));
                }

                if ((evt.type == EventType.MouseDown && evt.button == 0) || (GetPenPressure() > 0 && !penDown))
                {
                    penDown = true;
                    currentDragOrigin = currentCameraTransform2D.position;
                    currentMouseOrigin = mousePos01;
                    currentZoomOrigin = currentCameraTransform2D.scale;
                    currentRotateOrigin = currentCameraTransform2D.angle;
                }
                if (GetPenPressure() == 0 && penDown)
                {
                    penDown = false;
                }
            }
            else
            {
                if ((GetPenPressure() > 0  || (evt.type == EventType.MouseDown && evt.button == 0)) && evt.control)
                {
                    if (!(evt.type == EventType.Repaint || evt.type == EventType.Layout))
                    {
                        GameObject picked = HandleUtility.PickGameObject(evt.mousePosition, false);

                        if (picked.TryGetComponent<Renderer>(out Renderer renderer))
                        {
                            if (!picked.TryGetComponent<DrawnLineRenderer>(out DrawnLineRenderer lineRenderer))
                            {
                                lineRenderer = (DrawnLineRenderer)Undo.AddComponent(picked, typeof(DrawnLineRenderer));
                            }

                            activeLineRenderer = lineRenderer;

                            BuildDrawingStructure(renderer);
                        }
                        if(evt.type == EventType.MouseDown) evt.Use();
                        penUsedForSelection = true;
                    }
                }

                if (activeLineRenderer != null)
                {
                    //Debug.Log(Pen.current.eraser.ReadValue());
                    if ((GetPenPressure() > 0 || (evt.type == EventType.MouseDrag && evt.button == 0)) && !penUsedForSelection)
                    {
                        bool isFirstLine = false;
                        if (makingStroke == false)
                        {
                            //testPositions.Clear();
                            activeLineRenderer.currentLine = new List<DrawnLine>();
                            lazyMousePos = evt.mousePosition;
                            SceneViewLazyMousePositions.Remove(sceneView);
                            SceneViewLazyMousePositions.Add(sceneView, lazyMousePos);
                            currentLineStart = activeLineRenderer.currentLine.Count;
                            makingStroke = true;
                            isFirstLine = true;
                        }

                        if (GetEraser() != 0 || Keyboard.current.eKey.isPressed)
                        {
                            isEraser = true;
                        }

                        if (brushPressure == 0) brushPressure = 1; //drawn by mouse

                        int steps;

                        Vector2 from;
                        Vector2 to;
                        if (lastMousePosition.HasValue)
                        {
                            from = lastMousePosition.Value;
                            to = lazyMousePos;
                            steps = Mathf.RoundToInt((to - from).magnitude);
                        }
                        else
                        {
                            steps = 1;
                            from = lazyMousePos;
                            to = lazyMousePos;
                        }

                        if (lastMousePosition == lazyMousePos) return;

                        Vector3 prevNormal = Vector3.zero;

                        for (int i = 0; i < steps; i++)
                        {
                            Vector2 mousePosition = from + (to - from) * (i / (float)steps);
                            Ray ray = HandleUtility.GUIPointToWorldRay(mousePosition);

                            NativeRay nativeRay = NativeRay.FromRay(ray, 0, float.PositiveInfinity);

                            Vector3? newBrushPosition = null;
                            int newBrushPrimIndex = -1;

                            Intersection isect = drawingScene.Intersect(new Intersector(), nativeRay, false);
                            if (isect.t != float.PositiveInfinity)
                            {
                                newBrushPosition = nativeRay.o + nativeRay.d * isect.t;
                                newBrushPrimIndex = isect.primitiveIndex;
                            }
                            else
                            {
                                lastBrushPosition = null;
                                if (activeLineRenderer.currentLine.Count > 0)
                                {
                                    DrawnLine dl = activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1]; // issue here on drawing outside;
                                    dl.end = true;
                                    activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1] = dl;
                                }
                            }

                            /*foreach (Vector3 tp in testPositions)
                            {
                                Debug.DrawLine(tp, tp + Vector3.up * 0.01f);
                            }*/


                            if (lastBrushPosition.HasValue && newBrushPosition.HasValue
                                && lastBrushPosition != newBrushPosition)
                            {
                                BVHPrimitive prim = drawingScene.Primitives[newBrushPrimIndex];

                                float3 normal = math.normalize(math.cross(prim.v2 - prim.v1, prim.v3 - prim.v1)); //something was broken here with Vector3?
                                prevNormal = normal;
                                Camera camera = sceneView.camera;
                                float relativeSize = brushSize * 2 / camera.pixelHeight;
                                float worldSize = Mathf.Tan(Mathf.Deg2Rad * camera.fieldOfView * 0.5f);
                                Matrix4x4 worldToLocalMatrix = activeLineRenderer.transform.worldToLocalMatrix;
                                Matrix4x4 localToWorldMatrix = activeLineRenderer.transform.localToWorldMatrix;
                                Vector3 p1 = lastBrushPosition.Value;
                                Vector3 p2 = newBrushPosition.Value;
                                Vector3 line = p2 - p1;
                                Vector3 lineSS = math.dot(line, camera.transform.right) * camera.transform.right + math.dot(line, camera.transform.up) * camera.transform.up;
                                bool tooSteep = false;
                                if (line.magnitude / lineSS.magnitude > 3)
                                {
                                    if (activeLineRenderer.currentLine.Count > 0)
                                    {
                                        DrawnLine prev = activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1];
                                        prev.end = true;
                                        activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1] = prev;
                                    }
                                    tooSteep = true;
                                }
                                Vector3 lineNormal = line.normalized;
                                Vector3 localLine = worldToLocalMatrix.MultiplyVector(line);
                                Vector3 tangent = Vector3.Cross(line, normal).normalized;

                                if (!isEraser)
                                {
                                    if (!tooSteep)
                                    {
                                        Vector3 p1CamFwd = p1 - camera.transform.position;
                                        Vector3 p1CamRight = new Vector3(p1CamFwd.y, -p1CamFwd.x, 0).normalized;
                                        Vector3 p1CamUp = Vector3.Cross(p1CamFwd, p1CamRight).normalized;
                                        Vector3 p1LineSS = math.dot(line, p1CamRight) * p1CamRight + math.dot(line, p1CamUp) * p1CamUp;
                                        Vector3 p1TangentSS = math.normalize(math.cross(p1CamFwd, p1LineSS));
                                        float width1 = Vector3.Dot(camera.transform.forward, lastBrushPosition.Value - camera.transform.position) * worldSize * relativeSize * lastBrushPressure;
                                        Vector3 p1dot1 = p1 + p1TangentSS * width1;
                                        Vector3 p1dot2 = p1 - p1TangentSS * width1;
                                        Plane p1Surf = new Plane(normal, p1);
                                        Ray p1dot1Ray = new Ray(camera.transform.position, p1dot1 - camera.transform.position);
                                        Ray p1dot2Ray = new Ray(camera.transform.position, p1dot2 - camera.transform.position);
                                        float p1dot1dist = 0;
                                        bool hitP1Surf = p1Surf.Raycast(p1dot1Ray, out p1dot1dist);
                                        float p1dot2dist = 0;
                                        hitP1Surf = hitP1Surf && p1Surf.Raycast(p1dot2Ray, out p1dot2dist);
                                        Vector3 p1dot1surf = p1dot1Ray.GetPoint(p1dot1dist);
                                        Vector3 p1dot2surf = p1dot2Ray.GetPoint(p1dot2dist);
                                        Vector3 p1Line = (p1dot1surf - p1dot2surf);
                                        Vector3 localP1Tangent = worldToLocalMatrix.MultiplyVector(p1Line.normalized);
                                        Vector3 localP1 = worldToLocalMatrix.MultiplyPoint(p1dot2surf + p1Line * 0.5f);
                                        float widthProj1 = (p1Line.magnitude / 2);
                                        Vector3 localP1Normal = -Vector3.Cross(localLine, localP1Tangent).normalized;

                                        Vector3 p2CamFwd = p2 - camera.transform.position;
                                        Vector3 p2CamRight = new Vector3(p2CamFwd.y, -p2CamFwd.x, 0).normalized;
                                        Vector3 p2CamUp = Vector3.Cross(p2CamFwd, p2CamRight).normalized;
                                        Vector3 p2LineSS = math.dot(line, p2CamRight) * p2CamRight + math.dot(line, p2CamUp) * p2CamUp;
                                        Vector3 p2TangentSS = math.normalize(math.cross(p2CamFwd, p2LineSS));
                                        float width2 = Vector3.Dot(camera.transform.forward, newBrushPosition.Value - camera.transform.position) * worldSize * relativeSize * brushPressure;
                                        Vector3 p2dot1 = p2 + p2TangentSS * width2;
                                        Vector3 p2dot2 = p2 - p2TangentSS * width2;
                                        Plane p2Surf = new Plane(normal, p2);
                                        Ray p2dot1Ray = new Ray(camera.transform.position, p2dot1 - camera.transform.position);
                                        Ray p2dot2Ray = new Ray(camera.transform.position, p2dot2 - camera.transform.position);
                                        float p2dot1dist = 0;
                                        bool hitP2Surf = p2Surf.Raycast(p2dot1Ray, out p2dot1dist);
                                        float p2dot2dist = 0;
                                        hitP2Surf = hitP2Surf && p2Surf.Raycast(p2dot2Ray, out p2dot2dist);
                                        Vector3 p2dot1surf = p2dot1Ray.GetPoint(p2dot1dist);
                                        Vector3 p2dot2surf = p2dot2Ray.GetPoint(p2dot2dist);
                                        Vector3 p2Line = (p2dot1surf - p2dot2surf);
                                        Vector3 localP2Tangent = worldToLocalMatrix.MultiplyVector(p2Line.normalized);
                                        Vector3 localP2 = worldToLocalMatrix.MultiplyPoint(p2dot2surf + p2Line * 0.5f);
                                        float widthProj2 = (p2Line.magnitude / 2);
                                        Vector3 localP2Normal = -Vector3.Cross(localLine, localP2Tangent).normalized;

                                        Vector3 localTangent = math.lerp(localP1Tangent, localP2Tangent, 0.5f);

                                        DrawnLine current = new DrawnLine
                                        {
                                            p1 = localP1,
                                            p2 = localP2,
                                            p1Normal = localP1Normal,
                                            p2Normal = localP2Normal,
                                            tangent = localTangent,
                                            p1Tangent = localP1Tangent,
                                            p2Tangent = localP2Tangent,
                                            width1 = widthProj1,
                                            width2 = widthProj2,
                                            end = false
                                        };

                                        /*// unprojected
                                        Vector3 localNormal = worldToLocalMatrix.MultiplyVector(normal);
                                        localTangent = Vector3.Cross(localLine, localNormal).normalized;
                                        current = new DrawnLine
                                        {
                                            p1 = worldToLocalMatrix.MultiplyPoint(p1),
                                            p2 = worldToLocalMatrix.MultiplyPoint(p2),
                                            p1Normal = localNormal,
                                            p2Normal = localNormal,
                                            tangent = localTangent,
                                            p1Tangent = localTangent,
                                            p2Tangent = localTangent,
                                            width1 = width1,
                                            width2 = width2,
                                            end = false
                                        };*/

                                        if (activeLineRenderer.currentLine.Count > 0)
                                        {
                                            DrawnLine prev = activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1];
                                            if (prev.end == false)
                                            {
                                                Vector3 pointTan = math.normalize(math.lerp(current.p1Tangent, prev.p2Tangent, 0.5f));
                                                Vector3 pointNorm = math.normalize(math.lerp(current.p1Normal, prev.p2Normal, 0.5f));
                                                float width = math.lerp(current.width1, prev.width2, 0.5f);
                                                Vector3 point = math.lerp(current.p1, prev.p2, 0.5f);
                                                prev.p2 = point;
                                                current.p1 = point;
                                                prev.width2 = width;
                                                current.width1 = width;
                                                prev.p2Normal = pointNorm;
                                                current.p1Normal = pointNorm;
                                                prev.p2Tangent = pointTan;
                                                current.p1Tangent = pointTan;
                                            }
                                            activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1] = prev;
                                        }

                                        activeLineRenderer.currentLine.Add(current);
                                        activeLineRenderer.RefreshMesh();
                                    }
                                }
                                else
                                {
                                    for(int j = 0; j < activeLineRenderer.strokes.Count; j++)
                                    {
                                        Stroke s = activeLineRenderer.strokes[j];
                                        foreach (DrawnLine dl in s.lines)
                                        {
                                            float eraserRadius = Vector3.Dot(camera.transform.forward, newBrushPosition.Value - camera.transform.position) * worldSize * relativeSize;
                                            Vector3 p1world = localToWorldMatrix.MultiplyPoint(dl.p1);
                                            Vector3 p2world = localToWorldMatrix.MultiplyPoint(dl.p2);
                                            Vector3 pa = newBrushPosition.Value - p1world, ba = p2world - p1world;
                                            float h = Mathf.Clamp01(Vector3.Dot(pa, ba) / Vector3.Dot(ba, ba));
                                            float dist = (pa - ba * h).magnitude;
                                            if (dist < eraserRadius)
                                            {
                                                s.hide = true;
                                                // removing the item takes multiple seconds after a certain amount of strokes.
                                                // for some reason, undo doesn't like removing an item from a list.
                                                // currently for acceptable performance, I just hide the stroke instead of actually removing it :/
                                                Undo.RecordObject(activeLineRenderer, "Erase Line");
                                                activeLineRenderer.strokes[j] = s;
                                                activeLineRenderer.RefreshMesh();
                                                sceneView.Repaint();
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            lastBrushPosition = newBrushPosition;
                            lastBrushPrimIndex = newBrushPrimIndex;
                            lastMousePosition = lazyMousePos;
                        }
                        sceneView.Repaint();
                        lastBrushPressure = brushPressure;
                        if ((evt.type == EventType.MouseDrag && evt.button == 0))
                        {
                            evt.Use();
                        }
                    }


                    if ((GetPenPressure() == 0 && evt.pointerType == PointerType.Pen) || (evt.type == EventType.MouseUp && evt.button == 0))
                    {
                        penUsedForSelection = false;
                        if (makingStroke)
                        {
                            makingStroke = false;

                            if (activeLineRenderer.currentLine != null)
                            {
                                if (!isEraser && activeLineRenderer.currentLine.Count > 0)
                                {
                                    DrawnLine dl = activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1]; // error here
                                    dl.end = true;
                                    activeLineRenderer.currentLine[activeLineRenderer.currentLine.Count - 1] = dl;
                                    Simplify(activeLineRenderer.currentLine, currentLineStart, activeLineRenderer.currentLine.Count);
                                    List<DrawnLine> undoTempList = new List<DrawnLine>(activeLineRenderer.currentLine);
                                    activeLineRenderer.currentLine = null;

                                    Stroke currentStroke = new Stroke()
                                    {
                                        bounds = StrokeBounds(undoTempList),
                                        lines = undoTempList
                                    };

                                    Undo.RecordObject(activeLineRenderer, "Draw Line");
                                    //activeLineRenderer.lines.AddRange(undoTempList);
                                    activeLineRenderer.strokes.Add(currentStroke);
                                    activeLineRenderer.RefreshMesh();

                                }
                                else
                                {
                                    isEraser = false;
                                }

                                lastBrushPosition = null;
                                lastMousePosition = null;
                                sceneView.Repaint();
                            }
                        }

                        if ((evt.type == EventType.MouseUp && evt.button == 0))
                        {
                            evt.Use();
                        }
                    }

                    /*if (evt.type == EventType.Repaint)
                    {
                        if (lastBrushPosition != null)
                        {
                            //Handles.color = Color.red;
                            //Handles.DrawSolidDisc(lastBrushPosition.Value, sceneView.camera.transform.forward, 0.05f);
                        }
                    }*/
                }
            }

            Handles.BeginGUI();
            Handles.color = Color.black;
            Handles.DrawLine(lazyMousePos, evt.mousePosition);
            Handles.color = Color.white;
            Handles.DrawWireDisc(lazyMousePos, new Vector3(0, 0, 1), brushSize - 0.5f);
            Handles.color = Color.black;
            Handles.DrawWireDisc(lazyMousePos, new Vector3(0, 0, 1), brushSize + 0.5f);
            Handles.EndGUI();

            if (evt.type == EventType.Layout)
                HandleUtility.AddDefaultControl(GUIUtility.GetControlID(FocusType.Passive));

            //lastCanvasDragPosition = mousePos01;

            if (evt.type == EventType.MouseDown || evt.type == EventType.ScrollWheel)
            {
                evt.Use();
            }
        }

        Bounds StrokeBounds(List<DrawnLine> lines)
        {
            if (lines.Count > 0)
            {
                Bounds b = new Bounds(activeLineRenderer.transform.localToWorldMatrix.MultiplyPoint(lines[0].p1), Vector3.zero);
                Matrix4x4 localToWorldMatrix = activeLineRenderer.transform.localToWorldMatrix;
                foreach (DrawnLine dl in lines)
                {
                    b.Encapsulate(activeLineRenderer.transform.localToWorldMatrix.MultiplyPoint(dl.p2));
                }
                b.Encapsulate(activeLineRenderer.transform.localToWorldMatrix.MultiplyPoint(lines[0].p1));
                return b;
            }
            else return new Bounds();
        }

        void Simplify(List<DrawnLine> lines, int from, int to)
        {
            Matrix4x4 localToWorldMatrix = activeLineRenderer.transform.localToWorldMatrix;

            List<DrawnLine> temp = new List<DrawnLine>();
            for (int i = from; i < to; i++)
            {
                temp.Add(lines[i]);
            }

            for (int i = 1; i < temp.Count; i++)
            {
                DrawnLine l1 = temp[i - 1];
                DrawnLine l2 = temp[i];
                if (l1.end) continue;

                Vector2
                   p0SS = HandleUtility.WorldToGUIPoint(localToWorldMatrix.MultiplyPoint(l1.p1)),
                   p1SS = HandleUtility.WorldToGUIPoint(localToWorldMatrix.MultiplyPoint(l1.p2)),
                   p2SS = HandleUtility.WorldToGUIPoint(localToWorldMatrix.MultiplyPoint(l2.p2));

                float smoothScale = 100;
                float featureScreenSize = (p0SS - p1SS).magnitude + (p1SS - p2SS).magnitude;
                float epsilon = 1 - (0.001f * Mathf.Pow((1 - (Mathf.Clamp(featureScreenSize, 0, smoothScale) / smoothScale)), 4));

                if (Vector3.Dot(l1.p2Normal.normalized, l2.p1Normal.normalized) > 0.98f && Vector3.Dot(l2.p2Normal.normalized, l2.p1Normal.normalized) > 0.98f)
                {
                    if (Vector3.Dot(l1.tangent.normalized, l2.tangent.normalized) > epsilon)
                    {
                        DrawnLine l = new DrawnLine();
                        l.p1 = l1.p1;
                        l.p2 = l2.p2;
                        l.width1 = l1.width1;
                        l.width2 = l2.width2;
                        l.p1Normal = l1.p1Normal;
                        l.p2Normal = l2.p2Normal;
                        l.tangent = l2.p1Tangent;

                        Vector3 nP1Tangent = l1.p1Tangent;
                        if (i > 1) nP1Tangent = temp[i - 2].p1Tangent;
                        Vector3 nP2Tangent = l2.p2Tangent;
                        if (i < temp.Count - 1) nP1Tangent = temp[i + 1].p2Tangent;

                        l.p1Tangent = math.lerp(l.tangent, nP1Tangent, 0.5f);
                        l.p2Tangent = math.lerp(l.tangent, nP2Tangent, 0.5f);

                        l.end = l2.end;
                        temp.RemoveAt(i);
                        temp[i - 1] = l;
                        i--;
                    }
                }
            }

            for (int i = 1; i < temp.Count; i++)
            {
                DrawnLine l1 = temp[i - 1];
                DrawnLine l2 = temp[i];
                if (l1.end) continue;

                Vector2
                    p0SS = HandleUtility.WorldToGUIPoint(localToWorldMatrix.MultiplyPoint(l1.p1)),
                    p1SS = HandleUtility.WorldToGUIPoint(localToWorldMatrix.MultiplyPoint(l1.p2)),
                    p2SS = HandleUtility.WorldToGUIPoint(localToWorldMatrix.MultiplyPoint(l2.p2));

                float smoothScale = 10;
                float featureScreenSize = (p0SS - p1SS).magnitude + (p1SS - p2SS).magnitude;
                float epsilon = 1 - (2 * Mathf.Pow((1 - (Mathf.Clamp(featureScreenSize, 0, smoothScale) / smoothScale)), 8));

                if (Vector3.Dot(l1.p2Normal, l2.p1Normal) > 0.98f && Vector3.Dot(l2.p2Normal, l2.p1Normal) > 0.98f)
                {
                    if (Vector3.Dot(l1.tangent, l2.tangent) > epsilon)
                    {
                        DrawnLine l = new DrawnLine();
                        l.p1 = l1.p1;
                        l.p2 = l2.p2;
                        l.width1 = l1.width1;
                        l.width2 = l2.width2;
                        l.p1Normal = l1.p1Normal;
                        l.p2Normal = l2.p2Normal;
                        l.tangent = l2.p1Tangent;

                        Vector3 nP1Tangent = l1.p1Tangent;
                        if (i > 1) nP1Tangent = temp[i - 2].p1Tangent;
                        Vector3 nP2Tangent = l2.p2Tangent;
                        if (i < temp.Count-1) nP1Tangent = temp[i+1].p2Tangent;

                        l.p1Tangent = math.lerp(l.tangent, nP1Tangent, 0.5f);
                        l.p2Tangent = math.lerp(l.tangent, nP2Tangent, 0.5f);

                        l.end = l2.end;
                        temp.RemoveAt(i);
                        temp[i - 1] = l;
                        i--;
                    }
                }
            }

            for (int i = 1; i < temp.Count; i++)
            {
                DrawnLine dl = temp[i - 1];
                DrawnLine dl2 = temp[i];
                if (dl.end) continue;

                Vector3 e0 = dl.p2 - dl.p1;
                Vector3 e1 = dl2.p1 - dl2.p2;
                Vector3 pointTan = math.normalize(math.lerp(dl2.tangent, dl.tangent, 0.5f));
                float angle = Vector3.Angle(e0, e1) * Mathf.Deg2Rad; //calc miter length for even extrusion
                float miterLength = 1 / Mathf.Sin(angle / 2);

                if (miterLength > 1.1f && dl.p1 != dl.p2)
                {
                    DrawnLine miter = new DrawnLine()
                    {
                        p1 = dl.p2,
                        p2 = dl2.p1,
                        tangent = pointTan,
                        p1Normal = dl.p2Normal,
                        p2Normal = dl2.p1Normal,
                        p1Tangent = dl.p2Tangent,
                        p2Tangent = dl2.p1Tangent,
                        width1 = dl.width2,
                        width2 = dl2.width1
                    };
                    temp.Insert(i - 1, miter);
                    i++;
                }
                else
                {
                    Vector3 pointNorm = math.normalize(math.lerp(dl2.p1Normal, dl.p2Normal, 0.5f));
                    float pointWidth = dl.width2;
                    dl.p2Normal = pointNorm;
                    dl2.p1Normal = pointNorm;
                    dl.p2Tangent = pointTan;
                    dl2.p1Tangent = pointTan;
                    dl.width2 *= miterLength;
                    dl2.width1 *= miterLength;
                    temp[i - 1] = dl;
                    temp[i] = dl2;
                }
            }

            //add endcaps
            for (int i = 1; i < temp.Count; i++)
            {
                DrawnLine l0 = temp[i - 1];
                DrawnLine l1 = temp[i];

                if (i == 1 || l0.end)
                {
                    DrawnLine fl = l0;
                    if (l0.end) fl = l1;
                    float flCapSegmentWidth = fl.width1 / 2.41421356237f; //1 + sqrt(2) -> segment width based on octagon height
                    float flCapLength = (fl.width1 - flCapSegmentWidth);
                    Vector3 flCapTip = ((fl.p1 - fl.p2));
                    flCapTip = (activeLineRenderer.transform.localToWorldMatrix * flCapTip).normalized * flCapLength;
                    flCapTip = (activeLineRenderer.transform.worldToLocalMatrix * flCapTip);
                    flCapTip += fl.p1;

                    DrawnLine flCap = new DrawnLine()
                    {
                        p2 = fl.p1,
                        p1 = flCapTip,
                        p1Normal = fl.p1Normal,
                        p2Normal = fl.p1Normal,
                        p1Tangent = fl.p1Tangent,
                        p2Tangent = fl.p1Tangent,
                        tangent = fl.tangent,
                        width1 = flCapSegmentWidth,
                        width2 = fl.width1
                    };

                    if (l0.end)
                    {
                        temp.Insert(i - 1, flCap);
                    }
                    else
                    {
                        temp.Insert(0, l0);
                        temp[0] = flCap;
                    }
                    i++;
                }

                if (l1.end)
                {
                    DrawnLine ll = l1;
                    float llCapSegmentWidth = ll.width2 / 2.41421356237f; //1 + sqrt(2) -> segment width based on octagon height
                    float llCapLength = (ll.width2 - llCapSegmentWidth);
                    Vector3 llCapTip = ((ll.p2 - ll.p1));
                    llCapTip = (activeLineRenderer.transform.localToWorldMatrix * llCapTip).normalized * llCapLength;
                    llCapTip = (activeLineRenderer.transform.worldToLocalMatrix * llCapTip);
                    llCapTip += ll.p2;

                    DrawnLine llCap = new DrawnLine()
                    {
                        p1 = ll.p2,
                        p2 = llCapTip,
                        p1Normal = ll.p2Normal,
                        p2Normal = ll.p2Normal,
                        p1Tangent = ll.p2Tangent,
                        p2Tangent = ll.p2Tangent,
                        tangent = ll.tangent,
                        width2 = llCapSegmentWidth,
                        width1 = ll.width2
                    };
                    temp.Insert(i, llCap);
                    i++;
                }
            }

            DrawnLine lastLine = temp[temp.Count - 1];
            lastLine.end = true;
            temp[temp.Count - 1] = lastLine;

            lines.RemoveRange(from, to - from);
            lines.InsertRange(from, temp);
        }

        [BurstCompile]
        struct RenderDrawingStructureJob : IJobParallelFor
        {
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<Ray> rays;
            [WriteOnly, NativeMatchesParallelForLength]
            public NativeArray<Color32> pixels;

            [ReadOnly]
            public NativeBVH<BVHPrimitive> drawingScene;

            public void Execute(int index)
            {
                Color color = new Color32(0, 0, 0, 255);
                Ray ray = rays[index];

                NativeRay nativeRay = NativeRay.FromRay(ray, 0, float.PositiveInfinity);

                Intersection isect = drawingScene.Intersect(new Intersector(), nativeRay, false);
                if (isect.t != float.PositiveInfinity)
                {
                    color = new Color32(255, 255, 255, 255);
                }

                pixels[index] = color;
            }
        }

        static readonly ProfilerMarker BuildDrawingStructurePerfMarker = new ProfilerMarker("LineDrawingTool.BuildDrawingStructure");
        void BuildDrawingStructure(params Renderer[] toBuild)
        {
            using var _ = BuildDrawingStructurePerfMarker.Auto();

            ReleaseDrawingStructure();

            foreach (var renderer in toBuild)
            {
                if (renderer is MeshRenderer meshRenderer)
                {
                    MeshFilter meshFilter = meshRenderer.GetComponent<MeshFilter>();
                    if (meshFilter != null && meshFilter.sharedMesh != null)
                    {
                        RendererEntry entry = new RendererEntry();
                        entry.renderer = renderer;
                        entry.mesh = meshFilter.sharedMesh;
                        entry.isAllocated = false;
                        entry.rendererIndex = renderers.Count;

                        renderers.Add(entry.rendererIndex, entry);
                    }
                }
                else if (renderer is SkinnedMeshRenderer skinnedMeshRenderer)
                {
                    if (skinnedMeshRenderer.sharedMesh != null)
                    {
                        Mesh bakedMesh = new Mesh() { hideFlags = HideFlags.HideAndDontSave };
                        skinnedMeshRenderer.BakeMesh(bakedMesh, true);

                        RendererEntry entry = new RendererEntry();
                        entry.renderer = renderer;
                        entry.mesh = bakedMesh;
                        entry.isAllocated = true;
                        entry.rendererIndex = renderers.Count;

                        renderers.Add(entry.rendererIndex, entry);
                    }
                }
            }

            NativeList<BVHPrimitive> primitives = new NativeList<BVHPrimitive>(200_000, Allocator.TempJob);
            foreach (RendererEntry entry in renderers.Values)
            {
                var meshDataArray = MeshUtility.AcquireReadOnlyMeshData(entry.mesh);
                var meshData = meshDataArray[0];

                NativeArray<Vector3> vertices = new NativeArray<Vector3>(meshData.vertexCount, Allocator.TempJob);
                meshData.GetVertices(vertices);

                int numUsedSubMeshes = Mathf.Min(meshData.subMeshCount, entry.renderer.sharedMaterials.Length);
                for (int i = 0; i < numUsedSubMeshes; i++)
                {
                    SubMeshDescriptor descriptor = meshData.GetSubMesh(i);
                    if (descriptor.topology != MeshTopology.Triangles)
                        continue;

                    NativeArray<int> indices = new NativeArray<int>(descriptor.indexCount, Allocator.TempJob);
                    meshData.GetIndices(indices, i);

                    int primitiveCount = descriptor.indexCount / 3;
                    int offset = primitives.Length;
                    primitives.ResizeUninitialized(primitives.Length + primitiveCount);

                    new AppendPrimitivesJob
                    {
                        primitives = primitives,
                        primOffset = offset,
                        vertices = vertices,
                        indices = indices,
                        rendererIndex = entry.rendererIndex,
                        localToWorldMatrix = entry.renderer.localToWorldMatrix,
                    }.Schedule(primitiveCount, 256).Complete();

                    indices.Dispose();
                }

                vertices.Dispose();

                meshDataArray.Dispose();
            }

            drawingScene = new NativeBVH<BVHPrimitive>(primitives.AsArray(), Allocator.Persistent);
            primitives.Dispose();

            drawingScene.BuildJob(4).Complete();
        }

        [BurstCompile(CompileSynchronously = true)]
        struct AppendPrimitivesJob : IJobParallelFor
        {
            [NativeDisableParallelForRestriction]
            public NativeList<BVHPrimitive> primitives;
            public int primOffset;

            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> vertices;
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<int> indices;

            public int rendererIndex;

            public float4x4 localToWorldMatrix;

            public void Execute(int index)
            {
                int index1 = indices[index * 3 + 0];
                int index2 = indices[index * 3 + 1];
                int index3 = indices[index * 3 + 2];

                float3 vertex1 = math.transform(localToWorldMatrix, vertices[index1]);
                float3 vertex2 = math.transform(localToWorldMatrix, vertices[index2]);
                float3 vertex3 = math.transform(localToWorldMatrix, vertices[index3]);

                primitives[primOffset + index] = new BVHPrimitive
                {
                    v1 = vertex1,
                    v2 = vertex2,
                    v3 = vertex3,
                    rendererIndex = rendererIndex,
                };
            }
        }

        void ReleaseDrawingStructure()
        {
            foreach (var entry in renderers.Values)
            {
                if (entry.isAllocated)
                {
                    GameObject.DestroyImmediate(entry.mesh);
                }
            }

            renderers.Clear();

            if (drawingScene.IsCreated)
            {
                drawingScene.Dispose();
                drawingScene = default;
            }
        }

        class RendererEntry
        {
            public Renderer renderer;
            public Mesh mesh;
            public bool isAllocated;
            public int rendererIndex;
        }

        struct BVHPrimitive : IPrimitive
        {
            public float3 v1;
            public float3 v2;
            public float3 v3;
            public int rendererIndex;

            public Bounds GetBounds()
            {
                float3 min = Vector3.Min(Vector3.Min(v1, v2), v3);
                float3 max = Vector3.Max(Vector3.Max(v1, v2), v3);
                return new Bounds((min + max) * 0.5f, max - min);
            }

            public Vector3 GetCentroid()
            {
                return (v1 + v2 + v3) * (1 / 3.0f);
            }
        }

        struct Intersector : IIntersectionTest<BVHPrimitive>
        {
            //IntersectionPrimitives.RayTrianglePrecomputed precomputed;

            public float IntersectWith(in BVHPrimitive primitive, in NativeRay ray)
            {
                bool intersect = IntersectionPrimitives.IntersectRayTriangle2(
                    primitive.v1, primitive.v2, primitive.v3, ray,
                    out float t, out float b0, out float b1, out float b2);

                if (!intersect)
                {
                    return float.PositiveInfinity;
                }

                return t;
            }

            public void Precompute(in NativeRay ray)
            {
                //precomputed = IntersectionPrimitives.PrecomputeRayTriangle(ray);
            }
        }
    }
}