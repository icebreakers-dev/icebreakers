using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEditor;
using UnityEditor.SceneManagement;
using System.IO;

using Object = UnityEngine.Object;
using UnityEngine.Experimental.Rendering;
using UnityEngine.Rendering;
using Unity.Burst;
using Unity.Jobs;
using Unity.Collections;
using Unity.Mathematics;

using static Icebreakers.Editor.RectPack;
using System.Runtime.CompilerServices;
using Unity.Profiling;
using System.Linq;
using Unity.Collections.LowLevel.Unsafe;

namespace Icebreakers.Editor
{
    public class BakingTool : IDisposable
    {
        [MenuItem("Tools/Icebreakers/Bake Open Scene")]
        public static void BakeOpenScene()
        {
            SceneAsset targetScene = null;
            for (int i = 0; i < EditorSceneManager.sceneCount; i++)
            {
                Scene scene = EditorSceneManager.GetSceneAt(i);
                foreach (var root in scene.GetRootGameObjects())
                {
                    if (root.TryGetComponent<Page>(out _))
                    {
                        SceneAsset sceneAsset = AssetDatabase.LoadAssetAtPath<SceneAsset>(scene.path);
                        if (sceneAsset != null)
                        {
                            targetScene = sceneAsset;
                            break;
                        }
                    }
                }
            }

            if (targetScene == null)
            {
                EditorUtility.DisplayDialog("Error", "Cannot bake scene. No valid scene open\nThere needs to be a scene that contains a page", "Ok");
                return;
            }

            if (!EditorSceneManager.SaveCurrentModifiedScenesIfUserWantsTo())
            {
                return;
            }

            var oldSetup = EditorSceneManager.GetSceneManagerSetup();
            try
            {
                using (BakingTool bakingTool = new BakingTool())
                {
                    bakingTool.BakeScene(targetScene);
                }
            }
            finally
            {
                EditorSceneManager.RestoreSceneManagerSetup(oldSetup);
            }
        }

        const int supersamplingFactor = 32;
        const float sampleDistance = 0.8f;
        const int atlasSize = 4096;
        const int atlasPadding = 4;
        const int pixelsPerMeter = 400;
        const GraphicsFormat atlasFormat = GraphicsFormat.R8G8B8A8_SRGB;

        NativeBVH<BVHPrimitive> intersectionStructure;

        List<(string, Object)> errors = new List<(string, Object)>();

        ComputeShader dilateShader;
        Shader standardShader;
        Shader skySphereShader;
        int standardBakePassIndex;
        int skySphereBakePassIndex;

        Material downsampleMaterial;

        public BakingTool()
        {
            dilateShader = AssetDatabase.LoadAssetAtPath<ComputeShader>("Assets/Shaders/DilateTexture.compute");
            if (dilateShader == null)
                throw new Exception("Cannot find dilate shader");

            standardShader = Shader.Find("Icebreakers/Standard");
            if (standardShader == null)
                throw new Exception("Cannot find standard shader");

            skySphereShader = Shader.Find("Icebreakers/SkySphere");
            if (skySphereShader == null)
                throw new Exception("Cannot find sky sphere shader");

            standardBakePassIndex = -1;
            var lightModeId = new ShaderTagId("LightMode");
            var bakePassId = new ShaderTagId("Bake");
            for (int i = 0; i < standardShader.passCount; i++)
            {
                if (standardShader.FindPassTagValue(i, new ShaderTagId("LightMode")) == bakePassId)
                {
                    standardBakePassIndex = i;
                    break;
                }
            }

            if (standardBakePassIndex == -1)
                throw new Exception("Cannot find standard bake pass");

            skySphereBakePassIndex = -1;
            for (int i = 0; i < skySphereShader.passCount; i++)
            {
                if (skySphereShader.FindPassTagValue(i, new ShaderTagId("LightMode")) == bakePassId)
                {
                    skySphereBakePassIndex = i;
                    break;
                }
            }

            if (skySphereBakePassIndex == -1)
                throw new Exception("Cannot find sky sphere bake pass");

            Shader downsampleShader = Shader.Find("Hidden/Icebreakers/BakingDownsample");
            if (downsampleShader == null)
                throw new Exception("Cannot find downsample shader");

            downsampleMaterial = new Material(downsampleShader) { hideFlags = HideFlags.HideAndDontSave };
        }

        public void Dispose()
        {
            ReleaseIntersectionStructure();
            Object.DestroyImmediate(downsampleMaterial);
        }

        public static string GetBakedScenePath(SceneAsset sceneAsset)
        {
            string scenePath = AssetDatabase.GetAssetPath(sceneAsset);
            string bakingFolder = Path.Combine(Path.GetDirectoryName(scenePath), Path.GetFileNameWithoutExtension(scenePath));
            string bakedScenePath = Path.Combine(bakingFolder, Path.GetFileNameWithoutExtension(scenePath) + "_baked.unity");
            return bakedScenePath;
        }

        public void BakeScene(SceneAsset sceneAsset)
        {
            try
            {
                if (EditorUtility.DisplayCancelableProgressBar("Baking Scene", "Starting", 0))
                    throw new Exception("User Canceled");

                errors.Clear();

                string scenePath = AssetDatabase.GetAssetPath(sceneAsset);
                string bakingFolder = Path.Combine(Path.GetDirectoryName(scenePath), Path.GetFileNameWithoutExtension(scenePath));
                string bakedScenePath = Path.Combine(bakingFolder, Path.GetFileNameWithoutExtension(scenePath) + "_baked.unity");
                try
                {
                    Directory.Delete(bakingFolder, true);
                }
                catch (DirectoryNotFoundException)
                {
                }

                Directory.CreateDirectory(bakingFolder);

                SceneSetup[] sceneSetup = new SceneSetup[1]
                {
                    new SceneSetup
                    {
                        path = AssetDatabase.GetAssetPath(sceneAsset),
                        isLoaded = true,
                        isActive = true,
                    }
                };
                EditorSceneManager.RestoreSceneManagerSetup(sceneSetup);

                Scene scene = EditorSceneManager.GetSceneAt(0);



                UnpackPrefabs(scene.GetRootGameObjects());

                Page page = null;
                foreach (GameObject root in scene.GetRootGameObjects())
                {
                    if (root.TryGetComponent<Page>(out Page p))
                    {
                        page = p;
                    }
                }

                if (page == null)
                {
                    ReportError("No page found");
                    return;
                }
                else
                {
                    CheckPageTransforms(page);
                }

                page.autoUpdateLayers = false;

                List<PosableCharacter> posableCharacters = new List<PosableCharacter>();
                posableCharacters.AddRange(Object.FindObjectsOfType<PosableCharacter>(includeInactive: true));

                List<Mesh> bakedMeshes = new List<Mesh>();

                BakePosableCharacters(posableCharacters, bakedMeshes, bakingFolder);

                List<MeshRenderer> renderers = new List<MeshRenderer>();
                renderers.AddRange(Object.FindObjectsOfType<MeshRenderer>(includeInactive: true));

                FilterBakeableRenderers(renderers);

                FixSecondaryUVChannels(renderers, bakedMeshes, bakingFolder);

                List<TextureAtlas> textureAtlases = CreateTextureAtlases(renderers, page.ViewOrigin);

                BakeTextures(textureAtlases, page);

                SaveTextureAtlases(textureAtlases, bakingFolder);
                ApplyTextureAtlases(textureAtlases, bakingFolder);

                RemoveMaterialOverrides(renderers);

                DisableShadows();

                MakeAllObjectsVisibleAndSelectable(page);

                page.RefreshRenderingLayers();

                BakePokeyObjects(bakedMeshes, bakingFolder);

                List<OutlineRenderer> outlineRenderers = new List<OutlineRenderer>();
                outlineRenderers.AddRange(Object.FindObjectsOfType<OutlineRenderer>(includeInactive: true));
                List<DrawnLineRenderer> drawnLineRenderers = new List<DrawnLineRenderer>();
                drawnLineRenderers.AddRange(Object.FindObjectsOfType<DrawnLineRenderer>(includeInactive: true));

                BakeLineData(outlineRenderers, drawnLineRenderers, bakingFolder);

                EditorSceneManager.SaveScene(scene, bakedScenePath);

                AssetDatabase.Refresh();
            }
            finally
            {
                EditorUtility.ClearProgressBar();
            }
        }

        void UnpackPrefabs(GameObject[] roots)
        {
            void UnpackRecursively(GameObject gameObject)
            {
                if (PrefabUtility.IsAnyPrefabInstanceRoot(gameObject))
                {
                    PrefabUtility.UnpackPrefabInstance(gameObject, PrefabUnpackMode.Completely, InteractionMode.AutomatedAction);
                }

                foreach (Transform child in gameObject.transform)
                {
                    UnpackRecursively(child.gameObject);
                }
            }

            foreach (var gameObject in roots)
            {
                UnpackRecursively(gameObject);
            }
        }

        void MakeAllObjectsVisibleAndSelectable(Page page)
        {
            SceneVisibilityManager.instance.EnablePicking(page.gameObject, true);

            foreach (var tr in page.GetComponentsInChildren<Transform>())
            {
                tr.gameObject.hideFlags &= ~(HideFlags.HideInHierarchy & HideFlags.HideInInspector);
            }
        }

        void BakePokeyObjects(List<Mesh> bakedMeshes, string bakingFolder)
        {
            var objects = GameObject.FindObjectsOfType<PokeyObject>(true);
            for (int i = 0; i < objects.Length; i++)
            {
                if (EditorUtility.DisplayCancelableProgressBar("Baking Scene", "Baking Pokey", i / (float)objects.Length))
                    throw new Exception("User Canceled");

                PokeyObject obj = objects[i];
                if (!obj.enabled)
                    continue;

                if (!obj.TryGetComponent<MeshRenderer>(out var renderer))
                    continue;

                if (!obj.TryGetComponent<MeshFilter>(out var meshFilter))
                    continue;

                if (meshFilter.sharedMesh == null)
                    continue;

                Panel panel = obj.GetComponentInParent<Panel>();
                if (panel == null)
                    continue;

                GameObject go = GameObject.Instantiate(obj.gameObject, obj.transform.parent);
                GameObject.DestroyImmediate(go.GetComponent<PokeyObject>());

                renderer = go.GetComponent<MeshRenderer>();
                meshFilter = go.GetComponent<MeshFilter>();

                Mesh bakedMesh = GameObject.Instantiate(meshFilter.sharedMesh);
                bakedMesh.name = $"BakedMesh_{bakedMeshes.Count + 1} ({meshFilter.sharedMesh.name})";

                Plane plane = new Plane(panel.PanelView.transform.forward, panel.PanelView.transform.position);
                Vector3[] vertices = bakedMesh.vertices;
                Matrix4x4 localToWorld = go.transform.localToWorldMatrix;
                for (int j = 0; j < bakedMesh.subMeshCount; j++)
                {
                    if (bakedMesh.GetTopology(j) != MeshTopology.Triangles)
                    {
                        continue;
                    }

                    List<int> newIndices = new List<int>();
                    int[] indices = bakedMesh.GetIndices(j);
                    for (int k = 0; k < indices.Length; k += 3)
                    {
                        int idx1 = indices[k + 0];
                        int idx2 = indices[k + 1];
                        int idx3 = indices[k + 2];

                        Vector3 v1 = localToWorld.MultiplyPoint(vertices[idx1]);
                        Vector3 v2 = localToWorld.MultiplyPoint(vertices[idx2]);
                        Vector3 v3 = localToWorld.MultiplyPoint(vertices[idx3]);

                        if (plane.GetSide(v1) || plane.GetSide(v2) || plane.GetSide(v3))
                        {
                            newIndices.Add(idx1);
                            newIndices.Add(idx2);
                            newIndices.Add(idx3);
                        }
                    }

                    bakedMesh.SetIndices(newIndices, bakedMesh.GetTopology(j), j);
                }

                AssetDatabase.CreateAsset(bakedMesh, Path.Combine(bakingFolder, bakedMesh.name + ".asset"));

                meshFilter.sharedMesh = bakedMesh;
                renderer.renderingLayerMask = 1;
            }

            foreach (var obj in objects)
            {
                GameObject.DestroyImmediate(obj);
            }
        }

        class TextureAtlas
        {
            public Texture2D texture;
            public List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
        }

        class TextureAtlasEntry
        {
            public TextureAtlas atlas;
            public MeshRenderer renderer;
            public Rect atlasRect;
        }

        void DisableShadows()
        {
            foreach (var light in GameObject.FindObjectsOfType<Light>(true))
            {
                light.shadows = LightShadows.None;
            }
        }

        void RemoveMaterialOverrides(List<MeshRenderer> renderers)
        {
            foreach (var renderer in renderers)
            {
                if (renderer.TryGetComponent<MaterialOverride>(out var materialOverride))
                {
                    Object.DestroyImmediate(materialOverride);
                }
            }
        }

        void SaveTextureAtlases(List<TextureAtlas> textureAtlases, string bakingFolder)
        {
            for (int i = 0; i < textureAtlases.Count; i++)
            {
                TextureAtlas entry = textureAtlases[i];
                string path = Path.Combine(bakingFolder, $"BakedTexture_{i}.png");
                byte[] encoded = entry.texture.EncodeToPNG();
                Object.DestroyImmediate(entry.texture);
                File.WriteAllBytes(path, encoded);
                AssetDatabase.ImportAsset(path);
                entry.texture = AssetDatabase.LoadAssetAtPath<Texture2D>(path);
            }
        }

        void ApplyTextureAtlases(List<TextureAtlas> textureAtlases, string bakingFolder)
        {
            List<Material> bakedMaterials = new List<Material>();

            Shader standardShader = Shader.Find("Icebreakers/Standard");
            if (standardShader == null)
                throw new Exception("Cannot find standard shader");

            Shader bakedShader = Shader.Find("Icebreakers/Baked");

            foreach (var textureAtlas in textureAtlases)
            {
                foreach (var entry in textureAtlas.entries)
                {
                    Material[] materials = entry.renderer.sharedMaterials;

                    for (int i = 0; i < materials.Length; i++)
                    {
                        Material material = materials[i];
                        if (material.shader == standardShader)
                        {
                            material = new Material(bakedShader) { name = $"BakedMaterial_{bakedMaterials.Count}" };
                            material.SetTexture("_BakedTexture", textureAtlas.texture);
                            material.SetVector("_BakedRect", new Vector4(entry.atlasRect.x, entry.atlasRect.y, entry.atlasRect.width, entry.atlasRect.height));

                            bakedMaterials.Add(material);
                            AssetDatabase.CreateAsset(material, Path.Combine(bakingFolder, material.name + ".mat"));
                        }

                        materials[i] = material;
                    }

                    entry.renderer.sharedMaterials = materials;
                }
            }
        }

        public static float CalculateArea(MeshRenderer renderer, out Vector3 centerOfMass)
        {
            MeshFilter filter = renderer.GetComponent<MeshFilter>();
            if (filter == null)
            {
                Debug.LogError($"Cannot bake mesh renderer \"{renderer.name}\", it has no mesh filter attached");
                centerOfMass = renderer.transform.position;
                return 0;
            }

            if (filter.sharedMesh == null)
            {
                Debug.LogError($"Cannot bake mesh renderer \"{renderer.name}\", it has no mesh assigned");
                centerOfMass = renderer.transform.position;
                return 0;
            }

            var meshDataArray = MeshUtility.AcquireReadOnlyMeshData(filter.sharedMesh);

            var meshData = meshDataArray[0];
            int uvChannel = meshData.HasVertexAttribute(VertexAttribute.TexCoord1) ? 1 : 0;

            NativeArray<Vector3> vertices = new NativeArray<Vector3>(meshData.vertexCount, Allocator.Temp);
            //NativeArray<Vector2> uvs = new NativeArray<Vector2>(meshData.vertexCount, Allocator.Temp);
            meshData.GetVertices(vertices);
            //meshData.GetUVs(uvChannel, uvs);

            float totalArea = 0;
            float totalWeight = 0;
            Vector3 tmpCenterOfMass = Vector3.zero;

            Matrix4x4 localToWorldMatrix = renderer.transform.localToWorldMatrix;

            for (int i = 0; i < meshData.subMeshCount; i++)
            {
                var submesh = meshData.GetSubMesh(i);
                if (submesh.topology != MeshTopology.Triangles)
                    continue;

                NativeArray<int> indices = new NativeArray<int>(submesh.indexCount, Allocator.Temp);
                meshData.GetIndices(indices, i);

                for (int j = 0; j < indices.Length; j += 3)
                {
                    int idx1 = indices[j + 0];
                    int idx2 = indices[j + 1];
                    int idx3 = indices[j + 2];

                    Vector3 v1 = vertices[idx1];
                    Vector3 v2 = vertices[idx2];
                    Vector3 v3 = vertices[idx3];

                    v1 = localToWorldMatrix.MultiplyPoint(v1);
                    v2 = localToWorldMatrix.MultiplyPoint(v2);
                    v3 = localToWorldMatrix.MultiplyPoint(v3);

                    float area = 0.5f * Vector3.Cross(v2 - v1, v3 - v1).magnitude;
                    totalArea += area;

                    totalWeight += area;
                    tmpCenterOfMass += (v1 + v2 + v3) * (1.0f / 3.0f) * area;
                }

                indices.Dispose();
            }

            centerOfMass = tmpCenterOfMass / totalWeight;

            vertices.Dispose();
            //uvs.Dispose();

            meshDataArray.Dispose();

            return totalArea;
        }

        unsafe List<TextureAtlas> CreateTextureAtlases(List<MeshRenderer> renderers, Vector3 origin)
        {
            List<TextureAtlas> result = new List<TextureAtlas>();

            List<stbrp_rect> rects = new List<stbrp_rect>();
            for (int i = 0; i < renderers.Count; i++)
            {
                MeshRenderer renderer = renderers[i];

                float area = CalculateArea(renderer, out Vector3 centerOfMass);

                float distanceToOrigin = Vector3.Distance(centerOfMass, origin);

                float pixels = pixelsPerMeter * pixelsPerMeter * area / (distanceToOrigin * distanceToOrigin);

                float scale = 1;
                if (renderer.TryGetComponent<MaterialOverride>(out var materialOverride) && materialOverride.enabled)
                {
                    scale = materialOverride.bakingScale;
                }

                int size = Mathf.RoundToInt(Mathf.Sqrt(pixels) * scale);

                int maxSize = atlasSize - atlasPadding * 2;
                size = Mathf.Clamp(size, 0, maxSize);

                stbrp_rect rect = new stbrp_rect
                {
                    id = i,
                    x = 0,
                    y = 0,
                    w = size + atlasPadding * 2,
                    h = size + atlasPadding * 2,
                };

                rects.Add(rect);
            }

            stbrp_context packingContext;
            stbrp_node[] nodes = new stbrp_node[atlasSize];
            fixed (stbrp_node* pNodes = nodes)
            {
                while (rects.Count > 0)
                {
                    TextureAtlas currentAtlas = new TextureAtlas();
                    currentAtlas.texture = new Texture2D(atlasSize, atlasSize, atlasFormat, TextureCreationFlags.None);
                    result.Add(currentAtlas);

                    stbrp_init_target(&packingContext, currentAtlas.texture.width, currentAtlas.texture.height, pNodes, nodes.Length);
                    stbrp_setup_heuristic(&packingContext, STBRP_HEURISTIC_Skyline_BF_sortHeight);

                    stbrp_rect[] rectsArray = rects.ToArray();
                    stbrp_pack_rects(&packingContext, rectsArray);

                    float xScale = 1.0f / currentAtlas.texture.width;
                    float yScale = 1.0f / currentAtlas.texture.height;

                    rects.Clear();
                    for (int i = 0; i < rectsArray.Length; i++)
                    {
                        stbrp_rect rect = rectsArray[i];
                        if (rect.was_packed == 0)
                        {
                            rects.Add(rect);
                            continue;
                        }

                        Rect atlasRect = new Rect(
                            (rect.x + atlasPadding) * xScale,
                            (rect.y + atlasPadding) * yScale,
                            (rect.w - atlasPadding * 2) * xScale,
                            (rect.h - atlasPadding * 2) * yScale
                        );

                        currentAtlas.entries.Add(new TextureAtlasEntry
                        {
                            atlas = currentAtlas,
                            renderer = renderers[rect.id],
                            atlasRect = atlasRect,
                        });
                    }
                }
            }

            return result;
        }

        void FilterBakeableRenderers(List<MeshRenderer> renderers)
        {
            for (int i = 0; i < renderers.Count; i++)
            {
                MeshRenderer renderer = renderers[i];

                MeshFilter filter = renderer.GetComponent<MeshFilter>();
                if (filter == null)
                {
                    Debug.LogError($"Cannot bake mesh renderer \"{renderer.name}\", it has no mesh filter attached");
                    renderers.RemoveAt(i);
                    i--;
                    continue;
                }

                if (filter.sharedMesh == null)
                {
                    Debug.LogError($"Cannot bake mesh renderer \"{renderer.name}\", it has no mesh assigned");
                    renderers.RemoveAt(i);
                    i--;
                    continue;
                }

                Mesh mesh = filter.sharedMesh;

                bool anyBakeableMaterial = false;

                Material[] materials = renderer.sharedMaterials;
                for (int j = 0; j < materials.Length; j++)
                {
                    Material material = materials[j];
                    if (material == null)
                    {
                        Debug.LogError($"Null material in renderer {renderer.name} at index {j}");
                        continue;
                    }

                    int passIndex;
                    if (material.shader == standardShader)
                    {
                        passIndex = standardBakePassIndex;
                    }
                    else
                    {
                        Debug.LogWarning($"Cannot bake unsupported shader {material.shader.name} in renderer {renderer.name} at index {j}");
                        continue;
                    }

                    int subMeshIndex = renderer.subMeshStartIndex + j;
                    if (subMeshIndex < mesh.subMeshCount)
                    {
                        anyBakeableMaterial = true;
                    }
                }

                if (!anyBakeableMaterial)
                {
                    renderers.RemoveAt(i);
                    i--;
                    continue;
                }
            }
        }

        bool NeedsUVFix(Mesh mesh)
        {
            var meshDataArray = MeshUtility.AcquireReadOnlyMeshData(mesh);

            var meshData = meshDataArray[0];

            bool needsUVFix = false;
            if (!meshData.HasVertexAttribute(VertexAttribute.TexCoord1))
            {
                needsUVFix = true;
            }

            meshDataArray.Dispose();

            return needsUVFix;
        }

        void FixSecondaryUVChannels(List<MeshRenderer> renderers, List<Mesh> bakedMeshes, string bakingFolder)
        {
            Dictionary<Mesh, Mesh> meshResults = new Dictionary<Mesh, Mesh>();

            foreach (var renderer in renderers)
            {
                MeshFilter filter = renderer.GetComponent<MeshFilter>();
                Mesh mesh = filter.sharedMesh;
                if (!meshResults.TryGetValue(mesh, out Mesh result))
                {
                    if (!NeedsUVFix(mesh))
                        continue;

                    Debug.LogWarning($"Mesh {mesh.name} in renderer {renderer.name} needs a secondary uv channel fix");
                    if (bakedMeshes.Contains(mesh))
                    {
                        result = mesh;
                    }
                    else
                    {
                        result = GameObject.Instantiate(mesh);
                        result.name = $"BakedMesh_{bakedMeshes.Count + 1} ({mesh.name})";
                        bakedMeshes.Add(result);
                        AssetDatabase.CreateAsset(result, Path.Combine(bakingFolder, result.name + ".asset"));
                    }

                    result.uv2 = result.uv;
                    result.UploadMeshData(false);

                    EditorUtility.SetDirty(result);

                    meshResults.Add(mesh, result);
                }

                filter.sharedMesh = result;
            }
        }

        class BakeSet
        {
            public Panel panel;
            public List<TextureAtlasEntry> entries;
        }

        void BakeTextures(List<TextureAtlas> textureAtlases, Page page)
        {
            List<BakeSet> sets = new List<BakeSet>();
            if (page == null)
            {
                List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
                foreach (TextureAtlas textureAtlas in textureAtlases)
                {
                    entries.AddRange(textureAtlas.entries);
                }
                sets.Add(new BakeSet { panel = null, entries = entries});
            }
            else
            {
                foreach (var panel in page.Panels)
                {
                    List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
                    uint layer = 1u << panel.PanelNumber;
                    foreach (TextureAtlas textureAtlas in textureAtlases)
                    {
                        foreach (TextureAtlasEntry entry in textureAtlas.entries)
                        {
                            if (entry.renderer.renderingLayerMask == layer)
                            {
                                entries.Add(entry);
                            }
                        }
                    }

                    if (entries.Count > 0)
                    {
                        sets.Add(new BakeSet { panel = panel, entries = entries });
                    }
                }

                {
                    List<TextureAtlasEntry> entries = new List<TextureAtlasEntry>();
                    uint layer = 1;
                    foreach (TextureAtlas textureAtlas in textureAtlases)
                    {
                        foreach (TextureAtlasEntry entry in textureAtlas.entries)
                        {
                            if (entry.renderer.renderingLayerMask == layer)
                            {
                                entries.Add(entry);
                            }
                        }
                    }

                    if (entries.Count > 0)
                    {
                        sets.Add(new BakeSet { panel = null, entries = entries });
                    }
                }
            }

            Dictionary<TextureAtlas, RenderTexture> atlasRenderTextures = new Dictionary<TextureAtlas, RenderTexture>();
            foreach (var textureAtlas in textureAtlases)
            {
                var descriptor = new RenderTextureDescriptor(
                    textureAtlas.texture.width,
                    textureAtlas.texture.height,
                    GraphicsFormat.R32G32B32A32_SFloat, 0, 0)
                {
                    enableRandomWrite = true,
                };
                atlasRenderTextures.Add(textureAtlas, new RenderTexture(descriptor));
            }

            CommandBuffer cmd = new CommandBuffer();
            foreach (var renderTexture in atlasRenderTextures.Values)
            {
                cmd.SetRenderTarget(renderTexture);
                cmd.SetViewport(new Rect(0, 0, renderTexture.width, renderTexture.height));
                cmd.ClearRenderTarget(true, true, new Color(0, 0, 0, 0));
            }

            RenderPipeline.SetGlobalVariables(cmd, page, true);

            Vector2[] samples = new Vector2[supersamplingFactor];
            BestCanditateSampler sampler = new BestCanditateSampler();
            int seed = 0;

            foreach (var set in sets)
            {
                BuildIntersectionStructure(set.entries.Select(x => x.renderer).ToArray());

                ComputeBuffer primitiveBuffer = new ComputeBuffer(intersectionStructure.Primitives.Length, UnsafeUtility.SizeOf<BVHPrimitive>());
                ComputeBuffer nodesBuffer = new ComputeBuffer(intersectionStructure.Nodes.Length, UnsafeUtility.SizeOf<BVHNode>());

                primitiveBuffer.SetData(intersectionStructure.Primitives);
                nodesBuffer.SetData(intersectionStructure.Nodes);

                cmd.SetGlobalBuffer("_Primitives", primitiveBuffer);
                cmd.SetGlobalBuffer("_Nodes", nodesBuffer);

                RenderPipeline.SetPerPanelVariables(set.panel, cmd);

                cmd.EnableShaderKeyword("_RAYTRACED_SHADOWS");

                foreach (var entry in set.entries)
                {
                    MeshFilter filter = entry.renderer.GetComponent<MeshFilter>();
                    if (filter == null || filter.sharedMesh == null)
                        continue;

                    Mesh mesh = filter.sharedMesh;

                    seed++;
                    sampler.GenerateSamples2D(seed, new Vector2(-sampleDistance, -sampleDistance), new Vector2(sampleDistance, sampleDistance), samples.Length, samples);

                    RenderTexture renderTexture = atlasRenderTextures[entry.atlas];
                    cmd.SetRenderTarget(renderTexture);
                    cmd.SetViewport(new Rect(0, 0, renderTexture.width, renderTexture.height));
                    cmd.SetGlobalVector("_BakeRect", new Vector4(entry.atlasRect.x, entry.atlasRect.y, entry.atlasRect.width, entry.atlasRect.height));

                    if (entry.renderer.TryGetComponent<MaterialOverride>(out var materialOverride) && materialOverride.enabled)
                    {
                        materialOverride.ApplyOverride();
                    }

                    Material[] materials = entry.renderer.sharedMaterials;
                    for (int i = 0; i < materials.Length; i++)
                    {
                        Material material = materials[i];
                        if (material == null)
                        {
                            Debug.LogError($"Null material in renderer {entry.renderer.name} at index {i}");
                            continue;
                        }

                        int passIndex;
                        if (material.shader == standardShader)
                        {
                            passIndex = standardBakePassIndex;
                        }
                        else if (material.shader == skySphereShader)
                        {
                            passIndex = skySphereBakePassIndex;
                        }
                        else
                        {
                            Debug.LogWarning($"Cannot bake unsupported shader {material.shader.name} in renderer {entry.renderer.name} at index {i}");
                            continue;
                        }

                        int subMeshIndex = entry.renderer.subMeshStartIndex + i;
                        if (subMeshIndex < mesh.subMeshCount)
                        {
                            for (int j = 0; j < samples.Length; j++)
                            {
                                cmd.SetGlobalVector("_SampleOffset", new Vector4(
                                    samples[j].x * 1.0f / renderTexture.width,
                                    samples[j].y * 1.0f / renderTexture.height, 0, 0)
                                );

                                cmd.DrawRenderer(entry.renderer, material, subMeshIndex, passIndex);
                            }
                        }
                    }
                }

                cmd.DisableShaderKeyword("_RAYTRACED_SHADOWS");

                Graphics.ExecuteCommandBuffer(cmd);
                cmd.Clear();

                primitiveBuffer.Dispose();
                nodesBuffer.Dispose();
            }

            Graphics.ExecuteCommandBuffer(cmd);
            cmd.Clear();

            for (int i = 0; i < textureAtlases.Count; i++)
            {
                TextureAtlas textureAtlas = textureAtlases[i];

                RenderTexture renderTexture = atlasRenderTextures[textureAtlas];

                var descriptor = new RenderTextureDescriptor(
                    textureAtlas.texture.width,
                    textureAtlas.texture.height,
                    textureAtlas.texture.graphicsFormat, 0, 0)
                {
                    enableRandomWrite = true,
                };
                RenderTexture downsampleTexture = new RenderTexture(descriptor);

                downsampleMaterial.SetFloat("_DownsampleFactor", supersamplingFactor);
                Graphics.Blit(renderTexture, downsampleTexture, downsampleMaterial);

                Graphics.SetRenderTarget(downsampleTexture);
                textureAtlas.texture.ReadPixels(new Rect(0, 0, renderTexture.width, renderTexture.height), 0, 0, false);
                Graphics.SetRenderTarget(null);
                Object.DestroyImmediate(renderTexture);
                Object.DestroyImmediate(downsampleTexture);
            }

            cmd.Dispose();

            for (int i = 0; i < textureAtlases.Count; i++)
            {
                TextureAtlas textureAtlas = textureAtlases[i];
                if (EditorUtility.DisplayCancelableProgressBar("Baking Scene", $"Dilating Textures", (float)i / textureAtlases.Count))
                    throw new Exception("User canceled");

                NativeArray<Color32> pixels = textureAtlas.texture.GetPixelData<Color32>(0);
                int width = textureAtlas.texture.width;
                int height = textureAtlas.texture.height;

#if false
                NativeList<int> indices = new NativeList<int>(Allocator.TempJob);
                indices.Capacity = width * height;

                NativeList<int> outIndices = new NativeList<int>(Allocator.TempJob);

                new ClassifyPixelsJob
                {
                    pixels = pixels,
                    outIndices = indices.AsParallelWriter(),
                }.Schedule(pixels.Length, width).Complete();

                // Number of indices can only shrink from this point on,
                // so we need to set the capacity only once.
                outIndices.Capacity = indices.Length;

                const int maxIterations = 64;
                for (int j = 0; j < maxIterations; j++)
                {
                    if (indices.Length == 0)
                        break;

                    new DilateJob
                    {
                        pixels = pixels,
                        indices = indices,
                        outIndices = outIndices.AsParallelWriter(),
                        width = width,
                        height = height,
                    }.Schedule(indices.Length, 64).Complete();

                    CoreUtils.Swap(ref indices, ref outIndices);
                    outIndices.Length = 0;
                }

                indices.Dispose();
                outIndices.Dispose();

                textureAtlas.texture.SetPixelData(pixels, 0);
#else
                NativeArray<Color32> tmpPixels = new NativeArray<Color32>(pixels.Length, Allocator.TempJob);

                NativeArray<Color32> src = pixels;
                NativeArray<Color32> dst = tmpPixels;

                const int maxIterations = 64;
                for (int j = 0; j < maxIterations; j++)
                {
                    new DilateSimpleJob
                    {
                        srcPixels = src,
                        dstPixels = dst,
                        width = width,
                        height = height,
                    }.Schedule(src.Length, 1024).Complete();

                    CoreUtils.Swap(ref src, ref dst);
                }

                textureAtlas.texture.SetPixelData(src, 0);

                tmpPixels.Dispose();
#endif
            }
        }

        struct RNG
        {
            public ulong state;

            public float NextFloat()
            {
                uint value = Generate();
                // convert to a float in [0, 1) range
                const float u32ToFloat = 1.0f / (1 << 23);
                return (float)(value >> 9) * u32ToFloat;
            }

            /// Return the next integer in the range [min, max), exclusive, using a uniform distribution
            public int NextIndex(int min, int max)
            {
                // NOTE: this assumes modular arithmetic and twos-complement integers
                uint range = (uint)max - (uint)min;
                uint x = Generate();

                if (range == 0)
                {
                    return min;
                }
                else
                {
                    uint div = uint.MaxValue / (range);
                    uint rmax = div * (range);
                    while (x > rmax)
                    {
                        x = Generate();
                    }
                    return (int)((x / div) + (uint)min);
                }
            }

            /// Return the next integer in the range [min, max], inclusive using a uniform distribution
            public int Next(int min, int max)
            {
                // NOTE: this assumes modular arithmetic and twos-complement integers
                uint range = (uint)max - (uint)min;
                uint x = Generate();

                if (range == uint.MaxValue)
                {
                    return (int)x;
                }
                else
                {
                    uint div = uint.MaxValue / (range + 1);
                    uint rmax = div * (range + 1);
                    while (x > rmax)
                    {
                        x = Generate();
                    }
                    return (int)((x / div) + (uint)min);
                }
            }

            uint Generate()
            {
                // PCG32 algorithm
                // about twice as fast as System.Random.NextDouble
                ulong new_state = state * 6364136223846793005UL + 1442695040888963407UL;
                state = new_state;

                uint value = (uint)((new_state ^ (new_state >> 18)) >> 27);
                int rotate = (int)(new_state >> 59);
                return (value >> rotate) | (value << (32 - rotate));
            }
        }

        public class BestCanditateSampler
        {
            static readonly ProfilerMarker perfMarker2D = new ProfilerMarker("BestCanditateSampler.GenerateSamples2D");
            static readonly ProfilerMarker perfMarker1D = new ProfilerMarker("BestCanditateSampler.GenerateSamples1D");

            /// Quality of generated samples
            /// Time required to generate samples scales linearly with this value
            /// A value of 1 is the same as purely random sampling
            public int numCandidatesPerRound = 5;
            RNG rng;

            public void GenerateSamples2D(int seed, Vector2 min, Vector2 max, int numSamplesToGenerate, Vector2[] samples)
            {
                perfMarker2D.Begin();

                Vector2 scale = max - min;
                Vector2 offset = min;

                rng.state = (ulong)seed;

                for (int i = 0; i < numSamplesToGenerate; i++)
                {
                    Vector2 best = Vector2.zero;
                    float bestDist = 0;
                    for (int j = 0; j < numCandidatesPerRound; j++)
                    {
                        Vector2 sample;
                        sample.x = rng.NextFloat() * scale.x + offset.x;
                        sample.y = rng.NextFloat() * scale.y + offset.y;

                        float minDist = float.PositiveInfinity;
                        for (int k = 0; k < i; k++)
                        {
                            float dx = sample.x - samples[k].x;
                            float dy = sample.y - samples[k].y;
                            float dist = dx * dx + dy * dy;

                            if (dist < minDist)
                            {
                                minDist = dist;
                            }
                        }

                        if (minDist > bestDist)
                        {
                            bestDist = minDist;
                            best = sample;
                        }
                    }

                    samples[i] = best;
                }

                perfMarker2D.End();
            }

            public void GenerateSamples1D(int seed, float min, float max, int numSamplesToGenerate, float[] samples)
            {
                perfMarker1D.Begin();

                float scale = max - min;
                float offset = min;

                rng.state = (ulong)seed;

                for (int i = 0; i < numSamplesToGenerate; i++)
                {
                    float best = 0;
                    float bestDist = 0;
                    for (int j = 0; j < numCandidatesPerRound; j++)
                    {
                        float sample = rng.NextFloat() * scale + offset;

                        float minDist = float.PositiveInfinity;
                        for (int k = 0; k < i; k++)
                        {
                            float dist = sample - samples[k];
                            dist = dist < 0 ? -dist : dist;

                            if (dist < minDist)
                            {
                                minDist = dist;
                            }
                        }

                        if (minDist > bestDist)
                        {
                            bestDist = minDist;
                            best = sample;
                        }
                    }

                    samples[i] = best;
                }

                perfMarker1D.End();
            }
        }

        static readonly ProfilerMarker BuildIntersectionStructurePerfMarker = new ProfilerMarker("BakingTool.BuildIntersectionStructure");
        void BuildIntersectionStructure(MeshRenderer[] toBuild)
        {
            using var _ = BuildIntersectionStructurePerfMarker.Auto();

            ReleaseIntersectionStructure();

            NativeList<BVHPrimitive> primitives = new NativeList<BVHPrimitive>(200_000, Allocator.TempJob);
            foreach (var renderer in toBuild)
            {
                if (!(renderer.gameObject.activeInHierarchy && renderer.enabled))
                    continue;

                if (renderer.shadowCastingMode == ShadowCastingMode.Off)
                    continue;

                MeshFilter meshFilter = renderer.GetComponent<MeshFilter>();
                if (meshFilter != null && meshFilter.sharedMesh != null)
                {
                    var meshDataArray = MeshUtility.AcquireReadOnlyMeshData(meshFilter.sharedMesh);
                    var meshData = meshDataArray[0];

                    NativeArray<Vector3> vertices = new NativeArray<Vector3>(meshData.vertexCount, Allocator.TempJob);
                    meshData.GetVertices(vertices);

                    int numUsedSubMeshes = Mathf.Min(meshData.subMeshCount, renderer.sharedMaterials.Length);
                    for (int i = 0; i < numUsedSubMeshes; i++)
                    {
                        SubMeshDescriptor descriptor = meshData.GetSubMesh(i);
                        if (descriptor.topology != MeshTopology.Triangles)
                            continue;

                        NativeArray<int> indices = new NativeArray<int>(descriptor.indexCount, Allocator.TempJob);
                        meshData.GetIndices(indices, i);

                        int primitiveCount = descriptor.indexCount / 3;
                        int offset = primitives.Length;
                        primitives.ResizeUninitialized(primitives.Length + primitiveCount);

                        new AppendPrimitivesJob
                        {
                            primitives = primitives,
                            primOffset = offset,
                            vertices = vertices,
                            indices = indices,
                            localToWorldMatrix = renderer.localToWorldMatrix,
                        }.Schedule(primitiveCount, 256).Complete();

                        indices.Dispose();
                    }

                    vertices.Dispose();

                    meshDataArray.Dispose();
                }
            }

            intersectionStructure = new NativeBVH<BVHPrimitive>(primitives.AsArray(), Allocator.Persistent);
            primitives.Dispose();

            intersectionStructure.BuildJob(4).Complete();
        }

        [BurstCompile(CompileSynchronously = true)]
        struct AppendPrimitivesJob : IJobParallelFor
        {
            [NativeDisableParallelForRestriction]
            public NativeList<BVHPrimitive> primitives;
            public int primOffset;

            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Vector3> vertices;
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<int> indices;

            public float4x4 localToWorldMatrix;

            public void Execute(int index)
            {
                int index1 = indices[index * 3 + 0];
                int index2 = indices[index * 3 + 1];
                int index3 = indices[index * 3 + 2];

                float3 vertex1 = math.transform(localToWorldMatrix, vertices[index1]);
                float3 vertex2 = math.transform(localToWorldMatrix, vertices[index2]);
                float3 vertex3 = math.transform(localToWorldMatrix, vertices[index3]);

                primitives[primOffset + index] = new BVHPrimitive
                {
                    v1 = vertex1,
                    v2 = vertex2,
                    v3 = vertex3,
                };
            }
        }

        void ReleaseIntersectionStructure()
        {
            if (intersectionStructure.IsCreated)
            {
                intersectionStructure.Dispose();
                intersectionStructure = default;
            }
        }

        struct BVHPrimitive : IPrimitive
        {
            public float3 v1;
            public float3 v2;
            public float3 v3;

            public Bounds GetBounds()
            {
                float3 min = Vector3.Min(Vector3.Min(v1, v2), v3);
                float3 max = Vector3.Max(Vector3.Max(v1, v2), v3);
                return new Bounds((min + max) * 0.5f, max - min);
            }

            public Vector3 GetCentroid()
            {
                return (v1 + v2 + v3) * (1 / 3.0f);
            }
        }

        struct Intersector : IIntersectionTest<BVHPrimitive>
        {
            //IntersectionPrimitives.RayTrianglePrecomputed precomputed;

            public float IntersectWith(in BVHPrimitive primitive, in NativeRay ray)
            {
                bool intersect = IntersectionPrimitives.IntersectRayTriangle2(
                    primitive.v1, primitive.v2, primitive.v3, ray,
                    out float t, out float b0, out float b1, out float b2);

                if (!intersect)
                {
                    return float.PositiveInfinity;
                }

                return t;
            }

            public void Precompute(in NativeRay ray)
            {
                //precomputed = IntersectionPrimitives.PrecomputeRayTriangle(ray);
            }
        }

        [BurstCompile]
        struct ClassifyPixelsJob : IJobParallelFor
        {
            [ReadOnly]
            [NativeMatchesParallelForLength]
            public NativeArray<Color32> pixels;

            [WriteOnly]
            public NativeList<int>.ParallelWriter outIndices;

            public void Execute(int index)
            {
                Color32 pixel = pixels[index];
                if (pixel.a == 0)
                {
                    outIndices.AddNoResize(index);
                }
            }
        }

        [BurstCompile]
        struct DilateJob : IJobParallelFor
        {
            [NativeDisableParallelForRestriction]
            public NativeArray<Color32> pixels;

            [ReadOnly]
            public NativeList<int> indices;

            [WriteOnly]
            public NativeList<int>.ParallelWriter outIndices;

            public int width;
            public int height;

            public void Execute(int index)
            {
                int pixelIndex = indices[index];

                int x = pixelIndex % width;
                int y = pixelIndex / width;

                float weight = 0;
                Color color = new Color(0, 0, 0, 0);
                for (int dy = -1; dy <= 1; dy++)
                {
                    for (int dx = -1; dx <= 1; dx++)
                    {
                        int sx = x + dx;
                        int sy = y + dy;

                        if (sx < 0 || sx >= width || sy < 0 || sy >= height)
                            continue;

                        Color32 s = pixels[sx + sy * width];
                        if (s.a != 0)
                        {
                            Color sample = ((Color)s).linear;
                            color += sample;
                            weight += 1.0f;
                        }
                    }
                }

                if (weight > 0)
                {
                    color /= weight;
                    color.a = 1;
                    pixels[x + y * width] = color.gamma;
                }
                else
                {
                    outIndices.AddNoResize(pixelIndex);
                }
            }
        }

        [BurstCompile]
        struct DilateSimpleJob : IJobParallelFor
        {
            [ReadOnly, NativeDisableParallelForRestriction]
            public NativeArray<Color32> srcPixels;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<Color32> dstPixels;

            public int width;
            public int height;

            public void Execute(int index)
            {
                Color32 srcPixel = srcPixels[index];
                if (srcPixel.a != 0)
                {
                    dstPixels[index] = srcPixel;
                    return;
                }

                int x = index % width;
                int y = index / width;

                float weight = 0;
                Color color = new Color(0, 0, 0, 0);
                for (int dy = -1; dy <= 1; dy++)
                {
                    for (int dx = -1; dx <= 1; dx++)
                    {
                        int sx = x + dx;
                        int sy = y + dy;

                        if (sx < 0 || sx >= width || sy < 0 || sy >= height)
                            continue;

                        Color32 s = srcPixels[sx + sy * width];
                        if (s.a != 0)
                        {
                            Color sample = SRGBToLinear(s);
                            color += sample;
                            weight += 1.0f;
                        }
                    }
                }

                if (weight > 0)
                {
                    color /= weight;
                    color.a = 1;
                }

                dstPixels[x + y * width] = LinearToSRGB(color);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            static float SRGBToLinear(float srgb)
            {
                if (srgb <= 0.04045f)
                    return srgb / 12.92f;
                else
                    return Mathf.Pow((srgb + 0.055f) / 1.055f, 2.4f);
            }

            [MethodImpl(MethodImplOptions.AggressiveInlining)]
            static float LinearToSRGB(float linear)
            {
                if (linear <= 0.0031308f)
                    return linear * 12.92f;
                else
                    return Mathf.Pow(linear, 1.0f / 2.4f) * 1.055f - 0.055f;
            }

            public static Color SRGBToLinear(Color cSRGB)
            {
                Color cLinear;
                cLinear.r = SRGBToLinear(cSRGB.r);
                cLinear.g = SRGBToLinear(cSRGB.g);
                cLinear.b = SRGBToLinear(cSRGB.b);
                cLinear.a = cSRGB.a;
                return cLinear;
            }

            public static Color LinearToSRGB(Color cLinear)
            {
                Color cSRGB;
                cSRGB.r = LinearToSRGB(cLinear.r);
                cSRGB.g = LinearToSRGB(cLinear.g);
                cSRGB.b = LinearToSRGB(cLinear.b);
                cSRGB.a = cLinear.a;
                return cSRGB;
            }
        }

        void BakePosableCharacters(List<PosableCharacter> posableCharacters, List<Mesh> bakedMeshes, string bakingFolder)
        {
            for (int i = 0; i < posableCharacters.Count; i++)
            {
                PosableCharacter posableCharacter = posableCharacters[i];

                foreach (var skinnedMeshRenderer in posableCharacter.GetComponentsInChildren<SkinnedMeshRenderer>())
                {
                    if (skinnedMeshRenderer.sharedMesh == null)
                    {
                        continue;
                    }

                    Mesh bakedMesh = new Mesh();
                    bakedMesh.name = $"BakedMesh_{bakedMeshes.Count + 1} ({skinnedMeshRenderer.sharedMesh.name})";

                    skinnedMeshRenderer.BakeMesh(bakedMesh, true);

                    AssetDatabase.CreateAsset(bakedMesh, Path.Combine(bakingFolder, bakedMesh.name + ".asset"));

                    GameObject gameObject = skinnedMeshRenderer.gameObject;
                    Material[] materials = skinnedMeshRenderer.sharedMaterials;
                    uint renderingLayerMask = skinnedMeshRenderer.renderingLayerMask;

                    Object.DestroyImmediate(skinnedMeshRenderer);

                    var meshFilter = gameObject.AddComponent<MeshFilter>();
                    meshFilter.sharedMesh = bakedMesh;

                    var meshRenderer = gameObject.AddComponent<MeshRenderer>();
                    meshRenderer.sharedMaterials = materials;
                    meshRenderer.renderingLayerMask = renderingLayerMask;

                    if (gameObject.TryGetComponent<OutlineRenderer>(out var outlineRenderer))
                    {
                        outlineRenderer.Reinitialize();
                    }

                    bakedMeshes.Add(bakedMesh);
                }

                // We cannot destroy the rig, because there might be objects
                // parented to bones
                // if (posableCharacter.RigRoot != null)
                // {
                //     Object.DestroyImmediate(posableCharacter.RigRoot.gameObject);
                // }

                Object.DestroyImmediate(posableCharacter);
            }
        }

        void BakeLineData(List<OutlineRenderer> outlineRenderers, List<DrawnLineRenderer> drawnLineRenderers, string bakingFolder)
        {
            Dictionary<Mesh, BakedLineData> bakedData = new Dictionary<Mesh, BakedLineData>();
            List<Mesh> bakedLineMeshes = new List<Mesh>();

            foreach (var outlineRenderer in outlineRenderers)
            {
                if (outlineRenderer.IsSkinned)
                {
                    Debug.LogError($"Cannot bake outline renderer \"{outlineRenderer.name}\", it is attached to a skinned mesh", outlineRenderer);
                    continue;
                }

                if (outlineRenderer.SourceRenderer == null)
                {
                    Debug.LogError($"Cannot bake outline renderer \"{outlineRenderer.name}\", source renderer is null", outlineRenderer);
                    continue;
                }

                var meshFilter = outlineRenderer.SourceRenderer.GetComponent<MeshFilter>();
                if (meshFilter == null || meshFilter.sharedMesh == null)
                {
                    Debug.LogError($"Cannot bake outline renderer \"{outlineRenderer.name}\", no mesh filter with a valid mesh attached", outlineRenderer);
                    continue;
                }

                Mesh mesh = meshFilter.sharedMesh;
                if (!bakedData.TryGetValue(mesh, out BakedLineData bakedLineData))
                {
                    bakedLineData = OutlineMesh.GenerateBakedData(mesh);
                    bakedLineData.name = $"BakedLineData_{bakedData.Count} ({mesh.name})";
                    AssetDatabase.CreateAsset(bakedLineData, Path.Combine(bakingFolder, bakedLineData.name + ".asset"));
                    bakedData.Add(mesh, bakedLineData);
                }

                outlineRenderer.SetBakedLineData(bakedLineData);
            }

            Material drawnLineMaterial = new Material(Shader.Find("Hidden/Icebreakers/DrawnLine"));
            AssetDatabase.CreateAsset(drawnLineMaterial, Path.Combine(bakingFolder, "DrawnLineMaterial.material"));
            foreach (var drawnLineRenderer in drawnLineRenderers)
            {
                if (drawnLineRenderer.enabled)
                {
                    Mesh mesh = drawnLineRenderer.Bake();
                    if (mesh != null)
                    {
                        mesh.name = $"BakedDrawnLines_{bakedLineMeshes.Count} ({drawnLineRenderer.name})";
                        AssetDatabase.CreateAsset(mesh, Path.Combine(bakingFolder, mesh.name + ".asset"));
                        bakedLineMeshes.Add(mesh);

                        GameObject gameObject = new GameObject(drawnLineRenderer.name + " DrawnLines", typeof(MeshFilter), typeof(MeshRenderer));
                        MeshFilter filter = gameObject.GetComponent<MeshFilter>();
                        MeshRenderer renderer = gameObject.GetComponent<MeshRenderer>();
                        filter.sharedMesh = mesh;
                        renderer.sharedMaterial = drawnLineMaterial;

                        gameObject.transform.SetParent(drawnLineRenderer.transform, false);
                    }

                    GameObject.DestroyImmediate(drawnLineRenderer);
                }
            }
        }

        void CheckPageTransforms(Page page)
        {
            if (page.transform.position != Vector3.zero
                || page.transform.rotation != Quaternion.identity
                || page.transform.lossyScale != Vector3.one)
            {
                ReportError("Page transform is not zeroed", page);
            }

            foreach (var panel in page.Panels)
            {
                if (panel.transform.position != Vector3.zero
                    || panel.transform.rotation != Quaternion.identity
                    || panel.transform.lossyScale != Vector3.one)
                {
                    ReportError($"{panel.name} transform is not zeroed", panel);
                }
            }
        }

        void ReportError(string message, Object obj = null)
        {
            errors.Add((message, obj));
            Debug.LogError(message, obj);
        }
    }
}