using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace Icebreakers.Editor
{
    [CustomEditor(typeof(PanelView))]
    public class PanelViewEditor : UnityEditor.Editor
    {
        SerializedProperty size;
        SerializedProperty isPolygon;
        SerializedProperty polygonPoints;
        SerializedProperty sizePoints;
        SerializedProperty mask;

        void OnEnable()
        {
            size = serializedObject.FindProperty("size");
            isPolygon = serializedObject.FindProperty("isPolygon");
            polygonPoints = serializedObject.FindProperty("polygonPoints");
            mask = serializedObject.FindProperty("mask");
            sizePoints = serializedObject.FindProperty("sizePoints");
        }

        public override void OnInspectorGUI()
        {
            PanelView panelView = (PanelView)target;

            serializedObject.UpdateIfRequiredOrScript();

            EditorGUI.BeginChangeCheck();
            EditorGUILayout.PropertyField(isPolygon);
            if (EditorGUI.EndChangeCheck())
            {
                if (isPolygon.boolValue)
                {
                    panelView.IsPolygon = true;
                    polygonPoints.arraySize = panelView.PolygonPoints.Length;
                    for (int i = 0; i < panelView.PolygonPoints.Length; i++)
                    {
                        SerializedProperty elem = polygonPoints.GetArrayElementAtIndex(i);
                        elem.vector2Value = panelView.PolygonPoints[i];
                    }
                }
            }

            if (isPolygon.boolValue)
            {
                EditorGUILayout.PropertyField(polygonPoints);
            }
            else
            {
                EditorGUILayout.PropertyField(size);
            }

            if(GUILayout.Button("Reset Polygon To Size"))
            {
                panelView.IsPolygon = true;
                panelView.PolygonPoints = new Vector2[4]
                {
                    new Vector2(-size.vector2Value.x, -size.vector2Value.y) * 0.5f,
                    new Vector2(-size.vector2Value.x, size.vector2Value.y) * 0.5f,
                    new Vector2(size.vector2Value.x, size.vector2Value.y) * 0.5f,
                    new Vector2(size.vector2Value.x, -size.vector2Value.y) * 0.5f,
                };
            }

            EditorGUILayout.PropertyField(mask);

            serializedObject.ApplyModifiedProperties();
        }

        public void OnSceneGUI()
        {
            PanelView panelView = (PanelView)target;

            serializedObject.UpdateIfRequiredOrScript();
            
            if (isPolygon.boolValue)
            {
                for (int i = 0; i < polygonPoints.arraySize; i++)
                {
                    SerializedProperty elem = polygonPoints.GetArrayElementAtIndex(i);
                    Vector2 pos = elem.vector2Value;

                    Vector3 worldPos = panelView.transform.TransformPoint(pos);
                    EditorGUI.BeginChangeCheck();

                    worldPos = PolygonPointHandle(worldPos, panelView.transform.rotation, HandleUtility.GetHandleSize(worldPos) * 0.1f);

                    if (EditorGUI.EndChangeCheck())
                    {
                        elem.vector2Value = panelView.transform.InverseTransformPoint(worldPos);
                    }
                }
            }
            else
            {
                for (int i = 0; i < sizePoints.arraySize; i++)
                {
                    SerializedProperty elem = sizePoints.GetArrayElementAtIndex(i);
                    Vector2 pos = elem.vector2Value;

                    Vector3 worldPos = panelView.transform.TransformPoint(pos);
                    EditorGUI.BeginChangeCheck();

                    worldPos = PolygonPointHandle(worldPos, panelView.transform.rotation, HandleUtility.GetHandleSize(worldPos) * 0.1f);

                    if (EditorGUI.EndChangeCheck())
                    {
                        Vector2 nPos = panelView.transform.InverseTransformPoint(worldPos);
                        if (i < 2) nPos.x = 0;
                        else nPos.y = 0;
                        elem.vector2Value = nPos;
                        SerializedProperty p;
                        switch (i)
                        {
                            case 0: p = sizePoints.GetArrayElementAtIndex(1); p.vector2Value = -nPos; break;
                            case 1: p = sizePoints.GetArrayElementAtIndex(0); p.vector2Value = -nPos; break;
                            case 2: p = sizePoints.GetArrayElementAtIndex(3); p.vector2Value = -nPos; break;
                            case 3: p = sizePoints.GetArrayElementAtIndex(2); p.vector2Value = -nPos; break;
                        }
                    }
                }
                Vector2 p0 = sizePoints.GetArrayElementAtIndex(0).vector2Value;
                Vector2 p1 = sizePoints.GetArrayElementAtIndex(1).vector2Value;
                Vector2 p2 = sizePoints.GetArrayElementAtIndex(2).vector2Value;
                Vector2 p3 = sizePoints.GetArrayElementAtIndex(3).vector2Value;
                size.vector2Value = new Vector2(Mathf.Abs(p2.x - p3.x), Mathf.Abs(p0.y - p1.y));
            }

            serializedObject.ApplyModifiedProperties();
        }

        static Plane plane;
        static Vector3 PolygonPointHandle(Vector3 position, Quaternion rotation, float size)
        {
            Event evt = Event.current;

            int id = GUIUtility.GetControlID(FocusType.Passive);
            Handles.CubeHandleCap(id, position, rotation, size, evt.type);

            switch (evt.GetTypeForControl(id))
            {
                case EventType.MouseDown:
                    if (evt.button == 0)
                    {
                        if (HandleUtility.nearestControl == id)
                        {
                            plane = new Plane(rotation * Vector3.forward, position);
                            GUIUtility.hotControl = id;
                            evt.Use();
                            SceneView.RepaintAll();
                        }
                    }
                    break;

                case EventType.MouseUp:
                    if (evt.button == 0 && GUIUtility.hotControl == id)
                    {
                        GUIUtility.hotControl = 0;
                        evt.Use();
                        SceneView.RepaintAll();
                    }

                    break;

                case EventType.MouseDrag:
                    if (GUIUtility.hotControl == id)
                    {
                        Ray ray = HandleUtility.GUIPointToWorldRay(evt.mousePosition);
                        if (plane.Raycast(ray, out float t))
                        {
                            position = ray.origin + ray.direction * t;
                            evt.Use();
                            SceneView.RepaintAll();
                            GUI.changed = true;
                        }
                    }
                    break;
            }

            return position;
        }
    }
}