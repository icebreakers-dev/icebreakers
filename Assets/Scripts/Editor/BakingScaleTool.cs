using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

namespace Icebreakers.Editor
{
    public class BakingScaleTool : EditorWindow
    {
        [MenuItem("Window/Icebreakers/Baking Scale")]
        static void Initialize()
        {
            var window = EditorWindow.GetWindow<BakingScaleTool>();
            window.titleContent = new GUIContent("Baking Scale");
        }

        public bool isInBakingScaleMode
        {
            get => PageRenderFeature.bakingPreview;
            set => PageRenderFeature.bakingPreview = value;
        }

        void OnGUI()
        {
            string text = isInBakingScaleMode ? "Leave Baking Scale Mode" : "Enter Baking Scale Mode";
            bool newIsInBakingScaleMode = GUILayout.Toggle(isInBakingScaleMode, text, GUI.skin.button);
            if (newIsInBakingScaleMode != isInBakingScaleMode)
            {
                if (newIsInBakingScaleMode)
                {
                    EnterBakingScaleMode();
                }
                else
                {
                    LeaveBakingScaleMode();
                }
            }

            GameObject gameObject = Selection.activeGameObject;
            if (gameObject != null)
            {
                EditorGUILayout.ObjectField("Target", gameObject, typeof(GameObject), true);
                if (!gameObject.TryGetComponent<MaterialOverride>(out var materialOverride))
                {
                    if (GUILayout.Button("Add Override"))
                    {
                        Undo.AddComponent<MaterialOverride>(gameObject);
                    }
                }
                else
                {
                    EditorGUI.BeginChangeCheck();
                    float newBakingScale = EditorGUILayout.FloatField("Baking Scale", materialOverride.bakingScale);
                    if (EditorGUI.EndChangeCheck())
                    {
                        Undo.RecordObject(materialOverride, "Change baking scale");
                        materialOverride.bakingScale = newBakingScale;
                    }
                }
            }
            else
            {
                GUILayout.Label("Select a Game Object to edit the scale");
            }
        }

        void OnSelectionChanged()
        {
            Repaint();
        }

        void OnEnable()
        {
            Selection.selectionChanged += OnSelectionChanged;
        }

        void OnDisable()
        {
            Selection.selectionChanged -= OnSelectionChanged;
            if (isInBakingScaleMode)
            {
                LeaveBakingScaleMode();
            }
        }

        void EnterBakingScaleMode()
        {
            isInBakingScaleMode = true;
            SceneView.RepaintAll();
        }

        void LeaveBakingScaleMode()
        {
            isInBakingScaleMode = false;
            SceneView.RepaintAll();
        }
    }
}