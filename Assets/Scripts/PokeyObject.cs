using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    [ExecuteAlways]
    public class PokeyObject : MonoBehaviour
    {
        public static readonly HashSet<PokeyObject> pokeyObjects = new HashSet<PokeyObject>();

        [System.NonSerialized]
        public Renderer renderer;

        void OnEnable()
        {
            if (!pokeyObjects.Add(this))
            {
                Debug.LogError("Failed to add pokey object, state has been corrupted");
            }

            renderer = GetComponent<Renderer>();
        }

        void OnDisable()
        {
            if (!pokeyObjects.Remove(this))
            {
                Debug.LogError("Failed to remove pokey object, state has been corrupted");
            }
        }
    }
}