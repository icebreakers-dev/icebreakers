using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

namespace Icebreakers.Input
{
    public class InputCtrler : MonoBehaviour
    {
        [SerializeField] private Main main;

        private IcebreakersInput input;

        public void Awake() =>  input = new IcebreakersInput();

        public void OnEnable() => input.Testing.Enable();

        public void OnDisable() => input.Testing.Disable();

        bool isGrabbingPage;
        float lastRotatePageValue;

        PlayerInput playerInput;

        void Start()
        {
            playerInput = GetComponent<PlayerInput>();
#if !UNITY_EDITOR
            playerInput.SwitchCurrentControlScheme("XR");
#endif

            input.Testing.GrabPage.started += ctx =>
            {
                //Debug.Log("Begin Grabbing Page");
                isGrabbingPage = true;
                lastRotatePageValue = input.Testing.RotatePage.ReadValue<float>();
            };

            input.Testing.GrabPage.canceled += ctx =>
            {
                //Debug.Log("Relase Grabbing Page");
                isGrabbingPage = false;
            };
        }

        private void Update()
        {
            if (input.Testing.nextPage.triggered) main.NextPage();
            if (input.Testing.previousPage.triggered) main.PreviousPage();
            if (input.Testing.reloadPage.triggered) main.ReloadPage();

            if (isGrabbingPage)
            {
                float rotatePageValue = input.Testing.RotatePage.ReadValue<float>();
                //Debug.Log("Rotate Page: " + rotatePageValue);

                float delta = rotatePageValue - lastRotatePageValue;
                lastRotatePageValue = rotatePageValue;

                //Debug.Log(playerInput.currentControlScheme);
                if (playerInput.currentControlScheme == "XR")
                {
                    delta *= 360;
                }

                main.RotatePage(delta);
            }

            if (input.Testing.ToggleMenu.triggered)
            {
                if (main.IsShowingMenu)
                {
                    Debug.Log("Hide Menu");
                    main.HideMenu();
                }
                else
                {
                    Debug.Log("Show Menu");
                    main.ShowMenu();
                }
            }
        }

        public void ContinueClicked()
        {
            Debug.Log("Continue Clicked");
            main.HideMenu();
        }

        public void ExitClicked()
        {
            Debug.Log("Exit Clicked");
            main.HideMenu();
            Application.Quit();
#if UNITY_EDITOR
            UnityEditor.EditorApplication.ExitPlaymode();
#endif
        }
    }
}
