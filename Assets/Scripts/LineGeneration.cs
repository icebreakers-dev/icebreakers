using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Burst;
using Unity.Collections;
using Unity.Jobs;
using Unity.Mathematics;
using UnityEngine;

namespace Icebreakers
{
    public static class LineGeneration
    {
        [StructLayout(LayoutKind.Sequential)]
        [Serializable]
        public struct Line
        {
            public float3 p1;
            public float3 p2;
        }

        [StructLayout(LayoutKind.Sequential)]
        public struct VertexData
        {
            public float4 p1;
            public float4 p2;
        }

        [BurstCompile]
        public struct GenerateLineDataJob : IJob
        {
            [ReadOnly]
            public NativeArray<Line> lines;

            [WriteOnly]
            public NativeList<int> indices;
            public NativeList<VertexData> vertexData;

            public int indexOffset;

            public void Execute()
            {
                for (int i = 0; i < lines.Length; i++)
                {
                    Line line = lines[i];

                    int indexOffset = vertexData.Length;

                    vertexData.AddNoResize(new VertexData
                    {
                        p1 = new float4(line.p1, 0),
                        p2 = new float4(line.p2, -1),
                    });

                    vertexData.AddNoResize(new VertexData
                    {
                        p1 = new float4(line.p1, 0),
                        p2 = new float4(line.p2, 1),
                    });

                    vertexData.AddNoResize(new VertexData
                    {
                        p1 = new float4(line.p1, 1),
                        p2 = new float4(line.p2, -1),
                    });

                    vertexData.AddNoResize(new VertexData
                    {
                        p1 = new float4(line.p1, 1),
                        p2 = new float4(line.p2, 1),
                    });

                    indices.AddNoResize(indexOffset + 0);
                    indices.AddNoResize(indexOffset + 1);
                    indices.AddNoResize(indexOffset + 3);
                    indices.AddNoResize(indexOffset + 0);
                    indices.AddNoResize(indexOffset + 3);
                    indices.AddNoResize(indexOffset + 2);
                }
            }
        }
    }
}