using System.Collections;
using System.Collections.Generic;
using UnityEngine;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Icebreakers
{
    public struct PanelIntersection
    {
        public bool front;
        public float t;
    }

    [ExecuteInEditMode]
    public class Panel : MonoBehaviour
    {
        public string id;

        [SerializeField]
        Transform sceneRoot;
        public Transform SceneRoot => sceneRoot;

        [SerializeField]
        PanelView panelView;
        public PanelView PanelView => panelView;

        [SerializeField]
        int panelNumber = 1;
        public int PanelNumber => panelNumber;

        [SerializeField]
        bool isInside;
        public bool IsInside
        {
            get => isInside;
            set => isInside = value;
        }

        [SerializeField]
        [Range(0, 1)]
        public float fade = 1;

        [SerializeField]
        [HideInInspector]
        public Vector3 origin, viewDir;
        [SerializeField]
        [HideInInspector]
        public float viewPlaneDist;

        [SerializeField]
        public float fovDistortion = 1;

        [SerializeField]
        Light sceneLight;
        public Light SceneLight => sceneLight;

        [SerializeField]
        List<SpeechBubble> speechBubbles = new List<SpeechBubble>();
        public IReadOnlyList<SpeechBubble> SpeechBubbles => speechBubbles;

        public bool focusOnInPreview = true;

        public void RemoveSpeechBubble(SpeechBubble speechBubble)
        {
            speechBubbles.Remove(speechBubble);
        }

        public void AddSpeechBubble(SpeechBubble speechBubble)
        {
            if (!speechBubbles.Contains(speechBubble))
                speechBubbles.Add(speechBubble);
        }

        public bool IntersectWithPanel(Ray ray, float maxDistance, out PanelIntersection intersection)
        {
            intersection = default;

            if (panelView == null)
                return false;

            Vector3 planeNormal = panelView.transform.forward;
            Vector3 planeOrigin = panelView.transform.position;
            Vector3 panelRight = panelView.transform.right / panelView.Size.x;
            Vector3 panelUp = panelView.transform.up / panelView.Size.y;

            float distance = -Vector3.Dot(planeNormal, planeOrigin);

            float vdot = Vector3.Dot(ray.direction, planeNormal);
            float ndot = -Vector3.Dot(ray.origin, planeNormal) - distance;

            intersection.front = vdot < 0;

            if (Mathf.Abs(vdot) < 0.0001f)
            {
                return false;
            }

            float t = ndot / vdot;
            intersection.t = t;

            if (t < 0 || t > maxDistance)
            {
                return false;
            }

            Vector3 pointOnPlane = (ray.origin + ray.direction * t) - planeOrigin;
            float x = Vector3.Dot(pointOnPlane, panelRight);
            float y = Vector3.Dot(pointOnPlane, panelUp);

            Vector2 uv = new Vector2(x, y) + new Vector2(0.5f, 0.5f);

            if (uv.x < 0 || uv.x > 1 || uv.y < 0 || uv.y > 1)
            {
                return false;
            }

            Texture2D mask = panelView.Mask;
            if (mask != null)
            {
                if (!mask.isReadable)
                {
                    Debug.LogError("Cannot read panel mask", mask);
                    return true;
                }
                else
                {
                    float maskValue = mask.GetPixelBilinear(uv.x, uv.y).a;
                    return maskValue > 0.5;
                }

            }

            return true;
        }

        [HideInInspector]
        public bool isInEditMode = false;
        [HideInInspector]
        public GameObject panelPreviewCamera;
        [HideInInspector]
        public GameObject localViewOrigion;
        [HideInInspector]
        public GameObject panelCutoutMesh;

        public void FadeIn()
        {
            StartCoroutine(DoFade(0, 1, 0.5f));
        }

        IEnumerator DoFade(float from, float to, float duration)
        {
            float t = 0;
            while (t < duration)
            {
                fade = (t / duration) * (to - from) + from;
                t += Time.deltaTime;
                yield return null;
            }

            fade = to;
        }

        Mesh panelBacksideMesh;
        GameObject panelObject;

        void OnEnable()
        {
            panelObject = new GameObject("Panel Mesh")
            {
                hideFlags = HideFlags.HideAndDontSave
            };

            panelObject.transform.SetParent(panelView.transform, false);
            var meshFilter = panelObject.AddComponent<MeshFilter>();
            var meshRenderer = panelObject.AddComponent<MeshRenderer>();

            panelBacksideMesh = new Mesh()
            {
                hideFlags = HideFlags.HideAndDontSave
            };

            meshFilter.sharedMesh = panelBacksideMesh;
            meshRenderer.sharedMaterial = Resources.Load<Material>("PanelBacksideMaterial");

            UpdateMesh();

            panelObject.AddComponent<OutlineRenderer>();
        }

        void OnDisable()
        {
            GameObject.DestroyImmediate(panelObject);
            GameObject.DestroyImmediate(panelBacksideMesh);
        }

        void UpdateMesh()
        {
            float depth = 0.02f;

            Vector2 v1 = new Vector2(0, 0);
            Vector2 v2 = new Vector2(0, 0);
            Vector2 v3 = new Vector2(0, 0);
            Vector2 v4 = new Vector2(0, 0);

            if (panelView.IsPolygon)
            {
                var polygonPoints = panelView.PolygonPoints;
                if (polygonPoints.Length != 4 && polygonPoints.Length != 3)
                {
                    Debug.LogError("Panel view only supports polygons with three or four points");
                }

                if (polygonPoints.Length > 0)
                    v1 = polygonPoints[0];

                if (polygonPoints.Length > 1)
                    v2 = polygonPoints[1];

                if (polygonPoints.Length > 2)
                    v3 = polygonPoints[2];

                if (polygonPoints.Length > 3)
                    v4 = polygonPoints[3];
            }
            else
            {
                var size = panelView.Size;
                v1 = new Vector2(-size.x, -size.y) * 0.5f;
                v2 = new Vector2(-size.x, size.y) * 0.5f;
                v3 = new Vector2(size.x, size.y) * 0.5f;
                v4 = new Vector2(size.x, -size.y) * 0.5f;
            }

            Vector3[] vertices = new Vector3[]
            {
                new Vector3(v1.x, v1.y, 0),
                new Vector3(v2.x, v2.y, 0),
                new Vector3(v4.x, v4.y, 0),
                new Vector3(v3.x, v3.y, 0),
            };

            Vector3[] normals = new Vector3[]
            {
                new Vector3(0, 0, 1),
                new Vector3(0, 0, 1),
                new Vector3(0, 0, 1),
                new Vector3(0, 0, 1),
            };

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),
            };

            int[] indices = new int[6]
            {
                0, 3, 1,
                0, 2, 3,
            };

            panelBacksideMesh.Clear();
            panelBacksideMesh.subMeshCount = 1;
            panelBacksideMesh.vertices = vertices;
            panelBacksideMesh.normals = normals;
            panelBacksideMesh.uv = uvs;
            panelBacksideMesh.SetIndices(indices, MeshTopology.Triangles, 0);
            panelBacksideMesh.UploadMeshData(false);
        }

#if UNITY_EDITOR

        private void Update()
        {
            origin = localViewOrigion == null ? GameObject.Find("ViewOrigin").transform.position : localViewOrigion.transform.position;
            viewDir = panelView.transform.position - origin;
            float tempF = fovDistortion;
            viewPlaneDist = viewDir.magnitude;
            viewDir.Normalize();
            UpdateMesh();
        }

        /*private void OnDrawGizmos()
        {
            Vector3 origin = localViewOrigion == null ? GameObject.Find("ViewOrigin").transform.position : localViewOrigion.transform.position;
            Vector3 viewDir = panelView.transform.position - origin;
            Gizmos.DrawLine(origin, origin + viewDir.normalized * 3);
            Gizmos.DrawLine(origin, origin + Vector3.forward * 3);
            // distortionMatrix = Matrix4x4.Rotate(Quaternion.FromToRotation(Vector3.forward, viewDir)) * Matrix4x4.Translate(origin);
        }*/

        void printMatrix(Matrix4x4 mat)
        {
            Debug.LogFormat(
                $"{mat.m00:f4},  {mat.m01:f4},  {mat.m02:f4},  {mat.m03:f4}\n" +
                $"{mat.m10:f4},  {mat.m11:f4},  {mat.m12:f4},  {mat.m13:f4}\n" +
                $"{mat.m20:f4},  {mat.m21:f4},  {mat.m22:f4},  {mat.m23:f4}\n" +
                $"{mat.m30:f4},  {mat.m31:f4},  {mat.m32:f4},  {mat.m33:f4}");
        }

        public void LookAtPanel()
        {
            if (focusOnInPreview)
            {
                foreach (SceneView sceneView in SceneView.sceneViews)
                {
                    Quaternion lookDirection = Quaternion.LookRotation(panelView.transform.position - new Vector3(0, 1.75f, 0), Vector3.up);
                    Vector3 origin = new Vector3(0, 1.75f, 0) + lookDirection * Vector3.forward * sceneView.cameraDistance;
                    sceneView.LookAt(origin, lookDirection);
                }
            }
        }

        public IList SpeechBubbleList => speechBubbles;

        public void Initialize(PanelView panelView, Transform sceneRoot)
        {
            this.panelView = panelView;
            this.sceneRoot = sceneRoot;
            this.id = System.Guid.NewGuid().ToString("D");
        }

        public void UpdateRenderingLayers(int panelNumber)
        {
            this.panelNumber = panelNumber;

            if (sceneRoot == null)
                return;

            if (sceneLight != null)
            {
                sceneLight.renderingLayerMask = (int)(1u << panelNumber);
                if (PrefabUtility.IsPartOfAnyPrefab(sceneLight))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(sceneLight);
                }
            }

            uint layer = 1u << panelNumber;
            foreach (var renderer in sceneRoot.GetComponentsInChildren<Renderer>())
            {
                renderer.renderingLayerMask = layer;
                if (PrefabUtility.IsPartOfAnyPrefab(renderer))
                {
                    PrefabUtility.RecordPrefabInstancePropertyModifications(renderer);
                }
            }
        }
#endif
    }
}