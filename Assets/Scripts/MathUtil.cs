using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    public static class MathUtil
    {
        /// Real modulo, returns always a positive value
        public static int Mod(int value, int modulo)
        {
            int remainder = value % modulo;
            return remainder < 0 ? remainder + modulo : remainder;
        }

        /// Real modulo, returns always a positive value
        public static float Mod(float value, float modulo)
        {
            return value - Mathf.Floor(value / modulo) * modulo;
        }
    }
}