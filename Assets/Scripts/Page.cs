using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Audio;

namespace Icebreakers
{
    [ExecuteInEditMode]
    public class Page : MonoBehaviour
    {
        [SerializeField]
        List<Panel> panels = new List<Panel>();

        public IReadOnlyList<Panel> Panels => panels;

        [SerializeField]
        Transform viewOriginTransform;

        public AudioClip music;
        private AudioSource musicSource;

        public Vector3 ViewOrigin => viewOriginTransform != null ? viewOriginTransform.position : new Vector3(0,1.75f,0);
        public Quaternion ViewOriginRotation => viewOriginTransform != null ? viewOriginTransform.rotation : Quaternion.identity;

        [SerializeField]
        Transform worldRoot;
        public Transform WorldRoot => worldRoot;

        public void AddPanel(Panel panel)
        {
            panels.Add(panel);
        }

        public void RemovePanel(Panel panel)
        {
            panels.Remove(panel);
        }

        public void playMusic()
        {
            if (!Application.isEditor || Application.isPlaying)
            {
                if (musicSource == null)
                {
                    musicSource = gameObject.AddComponent<AudioSource>();
                    musicSource.playOnAwake = false;
                    musicSource.loop = false;
                    AudioMixer mixer = Resources.Load("MainAudioMixer") as AudioMixer;
                    musicSource.outputAudioMixerGroup = mixer.FindMatchingGroups("Music")[0];
                }
                musicSource.clip = music;
                musicSource.Play();
            }
        }

        private void Awake()
        {
            playMusic();
        }

#if UNITY_EDITOR
        public bool autoUpdateLayers = true;
        public IList PanelList => panels;

        public void PerformInitialSetup()
        {
            viewOriginTransform = new GameObject("ViewOrigin").transform;
            worldRoot = new GameObject("World").transform;
            viewOriginTransform.SetParent(transform, false);
            worldRoot.SetParent(transform, false);
        }

        void Update()
        {
            if (Application.isPlaying)
                return;

            if (autoUpdateLayers)
            {
                RefreshRenderingLayers();
            }
        }

        public void RefreshRenderingLayers()
        {
            for (int i = 0; i < panels.Count; i++)
            {
                Panel panel = panels[i];
                panel.UpdateRenderingLayers(i + 1);

                panel.transform.localPosition = Vector3.zero;
                panel.transform.localRotation = Quaternion.identity;
                panel.transform.localScale = Vector3.one;
            }

            transform.localPosition = Vector3.zero;
            transform.localRotation = Quaternion.identity;
            transform.localScale = Vector3.one;
        }

        private void OnDrawGizmos()
        {
            bool editMode = false;

            for (int i = 0; i < panels.Count; i++)
            {
                editMode = editMode || panels[i].isInEditMode;
            }

            if (!editMode)
            {
                Gizmos.DrawWireSphere(ViewOrigin, 1);
            }
        }
#endif
    }
}