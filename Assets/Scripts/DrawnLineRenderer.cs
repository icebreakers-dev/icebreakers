using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.InteropServices;
using Unity.Collections;
using UnityEngine;
using Unity.Profiling;
using Unity.Burst;
using Unity.Jobs;
using Unity.Mathematics;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine.Rendering;

namespace Icebreakers
{
    [ExecuteAlways]
    public class DrawnLineRenderer : MonoBehaviour
    {
        //[SerializeField]
        //[HideInInspector]
        //public List<DrawnLine> lines = new List<DrawnLine>();
        [SerializeField]
        public List<Stroke> strokes = new List<Stroke>();

        //[HideInInspector]
        public List<DrawnLine> currentLine;

        [NonSerialized]
        DrawnLineMesh drawnLineMesh;

        Renderer sourceRenderer;
        public Renderer SourceRenderer => sourceRenderer;

        public static readonly HashSet<DrawnLineRenderer> drawnLineRenderers = new HashSet<DrawnLineRenderer>();

        void OnEnable()
        {
            if (!drawnLineRenderers.Add(this))
            {
                Debug.LogError("Failed to add drawn line renderer, state has been corrupted");
            }

            sourceRenderer = GetComponent<Renderer>();
        }

        void OnDisable()
        {
            if (!drawnLineRenderers.Remove(this))
            {
                Debug.LogError("Failed to remove drawn line renderer, state has been corrupted");
            }

            if (drawnLineMesh != null)
            {
                drawnLineMesh.Dispose();
                drawnLineMesh = null;
            }
        }

        void OnValidate()
        {
            RefreshMesh();
        }

        public void RefreshMesh()
        {
            if (drawnLineMesh == null)
                return;
            List<DrawnLine> fullLine = new List<DrawnLine>();
            foreach(Stroke s in strokes)
            {
                if (!s.hide) fullLine.AddRange(s.lines);
            }
            if (currentLine != null) fullLine.AddRange(currentLine);
            drawnLineMesh.Refresh(fullLine);
        }

        public Mesh Bake()
        {
            EnsureDrawnLineMesh();

            if (drawnLineMesh != null)
            {
                return drawnLineMesh.Bake();
            }

            return null;
        }

        public void GenerateForCamera(Camera camera, in LineGenerationContext context)
        {
            if (!enabled)
                throw new Exception("Drawn Lines Renderer is disabled");

            if (camera == null)
                throw new ArgumentNullException(nameof(camera));

            EnsureDrawnLineMesh();

            if (drawnLineMesh != null)
            {
                drawnLineMesh.GenerateLines(
                    transform.localToWorldMatrix,
                    context);
            }
        }

        public bool GetDrawingParams(out DrawnLineMeshDrawingArgs args)
        {
            if (!enabled)
                throw new Exception("Drawn Lines Renderer is disabled");

            EnsureDrawnLineMesh();

            if (drawnLineMesh != null)
            {
                drawnLineMesh.GetDrawingParams(
                    transform.localToWorldMatrix,
                    out args);

                return true;
            }

            args = default;
            return false;
        }

        void EnsureDrawnLineMesh()
        {
            if (drawnLineMesh == null)
            {
                List<DrawnLine> fullLine = new List<DrawnLine>();
                foreach (Stroke s in strokes)
                {
                    if(!s.hide) fullLine.AddRange(s.lines);
                }
                if (currentLine != null) fullLine.AddRange(currentLine);
                drawnLineMesh = new DrawnLineMesh(fullLine, transform);
            }
        }
    }

    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct DrawnLine
    {
        public Vector3 p1;
        public Vector3 p2;
        public float width1;
        public float width2;
        public Vector3 tangent;
        public Vector3 p1Normal;
        public Vector3 p2Normal;
        public Vector3 p1Tangent;
        public Vector3 p2Tangent;
        public bool end;
    }

    [StructLayout(LayoutKind.Sequential)]
    [Serializable]
    public struct Stroke
    {
        public Bounds bounds;
        public bool hide;
        public List<DrawnLine> lines;
    }

    public struct DrawnLineMeshDrawingArgs
    {
        public Mesh lineMesh;
        public Matrix4x4 localToWorldMatrix;
    }

    public class DrawnLineMesh : IDisposable
    {
        NativeList<DrawnLine> drawnLines;

        Mesh lineMesh;

        Transform transform;

        static readonly ProfilerMarker ConstructDrawnLineMeshPerfMarker = new ProfilerMarker("DrawnLineMesh.Construct");

        public DrawnLineMesh(List<DrawnLine> lines, Transform transform)
        {
            if (lines == null)
                throw new ArgumentNullException();

            ConstructDrawnLineMeshPerfMarker.Begin();

            drawnLines = new NativeList<DrawnLine>(lines.Count, Allocator.Persistent);
            foreach (var line in lines)
            {
                drawnLines.AddNoResize(line);
            }

            lineMesh = new Mesh() { name = "Drawn Line Mesh" };
            lineMesh.hideFlags = HideFlags.HideAndDontSave;

            this.transform = transform;

            GenerateMesh(transform.localToWorldMatrix, lineMesh);

            ConstructDrawnLineMeshPerfMarker.End();
        }

        public void Dispose()
        {
            drawnLines.Dispose();
            GameObject.DestroyImmediate(lineMesh);
        }

        public void Refresh(List<DrawnLine> lines)
        {
            drawnLines.ResizeUninitialized(lines.Count);
            for (int i = 0; i < lines.Count; i++)
            {
                drawnLines[i] = lines[i];
            }

            GenerateMesh(transform.localToWorldMatrix, lineMesh);
        }

        public void GetDrawingParams(Matrix4x4 localToWorldMatrix, out DrawnLineMeshDrawingArgs args)
        {
            args.localToWorldMatrix = localToWorldMatrix;
            args.lineMesh = lineMesh;
        }

        static readonly ProfilerMarker GenerateMeshPerfMarker = new ProfilerMarker("DrawnLinesMesh.GenerateMesh");
        void GenerateMesh(Matrix4x4 localToWorldMatrix, Mesh mesh)
        {
            using var _ = GenerateMeshPerfMarker.Auto();

            NativeArray<float3> vertices = new NativeArray<float3>(drawnLines.Length * 4, Allocator.TempJob);
            NativeArray<float4> sides = new NativeArray<float4>(drawnLines.Length * 4, Allocator.TempJob);
            NativeArray<int> indices = new NativeArray<int>(drawnLines.Length * 6, Allocator.TempJob);

            new GenerateMeshJob
            {
                drawnLines = drawnLines,
                vertices = vertices,
                sides = sides,
                indices = indices,
                localToWorldMatrix = localToWorldMatrix,
                worldToLocalMatrix = localToWorldMatrix.inverse
            }.Schedule(drawnLines.Length, 64).Complete();

            mesh.Clear();
            mesh.subMeshCount = 1;
            mesh.indexFormat = vertices.Length < ushort.MaxValue ? IndexFormat.UInt16 : IndexFormat.UInt32;

            mesh.SetVertices(vertices);
            mesh.SetUVs(0, sides);
            mesh.SetIndices(indices, MeshTopology.Triangles, 0);

            mesh.UploadMeshData(false);

            vertices.Dispose();
            sides.Dispose();
            indices.Dispose();
        }

        public Mesh Bake()
        {
            Mesh mesh = new Mesh();

            GenerateMesh(transform.localToWorldMatrix, mesh);

            return mesh;
        }

        [BurstCompile]
        public struct GenerateMeshJob : IJobParallelFor
        {
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<DrawnLine> drawnLines;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<float3> vertices;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<float4> sides;

            [WriteOnly, NativeDisableParallelForRestriction]
            public NativeArray<int> indices;

            public Matrix4x4 localToWorldMatrix;
            public Matrix4x4 worldToLocalMatrix;

            public void Execute(int index)
            {
                DrawnLine line = drawnLines[index];
                int vertexOffset = index * 4;
                int indexOffset = index * 6;

                Vector3 scaledP1Normal = worldToLocalMatrix * ((localToWorldMatrix * line.p1Normal).normalized * 0.0005f);
                Vector3 scaledP2Normal = worldToLocalMatrix * ((localToWorldMatrix * line.p2Normal).normalized * 0.0005f);

                vertices[vertexOffset + 0] = line.p1 + scaledP1Normal;
                vertices[vertexOffset + 1] = line.p1 + scaledP1Normal;
                vertices[vertexOffset + 2] = line.p2 + scaledP2Normal;
                vertices[vertexOffset + 3] = line.p2 + scaledP2Normal;

                //float3 side = math.normalize(math.cross(line.p2 - line.p1, line.normal));
                sides[vertexOffset + 0] = new float4(- line.p1Tangent, line.width1);
                sides[vertexOffset + 1] = new float4(line.p1Tangent, line.width1);
                sides[vertexOffset + 2] = new float4(-line.p2Tangent, line.width2);
                sides[vertexOffset + 3] = new float4(line.p2Tangent, line.width2);

                indices[indexOffset + 0] = vertexOffset + 0;
                indices[indexOffset + 1] = vertexOffset + 1;
                indices[indexOffset + 2] = vertexOffset + 3;
                indices[indexOffset + 3] = vertexOffset + 0;
                indices[indexOffset + 4] = vertexOffset + 3;
                indices[indexOffset + 5] = vertexOffset + 2;
            }
        }

        public void GenerateLines(Matrix4x4 localToWorldMatrix, LineGenerationContext context)
        {
            GenerateMeshPerfMarker.Begin();

            NativeArray<LineGeneration.Line> lines = new NativeArray<LineGeneration.Line>(drawnLines.Length, Allocator.TempJob);

            int numVertices = lines.Length * 4;
            int numIndices = lines.Length * 6;

            if (context.vertexData.Length + numVertices > context.vertexData.Capacity)
            {
                context.vertexData.Capacity = Mathf.Max(2 * context.vertexData.Length, context.vertexData.Length + numVertices);
            }

            if (context.indices.Length + numIndices > context.indices.Capacity)
            {
                context.indices.Capacity = Mathf.Max(2 * context.indices.Length, context.indices.Length + numIndices);
            }

            JobHandle jobHandle = default;

            jobHandle = new TransformLinesJob
            {
                drawnLines = drawnLines,
                lines = lines,
                localToWorldMatrix = localToWorldMatrix,
            }.Schedule(drawnLines.Length, 256, jobHandle);

            jobHandle = new LineGeneration.GenerateLineDataJob
            {
                lines = lines,
                indices = context.indices,
                vertexData = context.vertexData,
                indexOffset = context.vertexData.Length,
            }.Schedule(jobHandle);

            jobHandle.Complete();

            lines.Dispose();

            GenerateMeshPerfMarker.End();
        }

        [BurstCompile]
        struct TransformLinesJob : IJobParallelFor
        {
            [ReadOnly, NativeMatchesParallelForLength]
            public NativeArray<DrawnLine> drawnLines;

            [WriteOnly, NativeMatchesParallelForLength]
            public NativeArray<LineGeneration.Line> lines;

            public float4x4 localToWorldMatrix;

            public void Execute(int index)
            {
                var line = drawnLines[index];
                line.p1 = math.transform(localToWorldMatrix, line.p1);
                line.p2 = math.transform(localToWorldMatrix, line.p2);
                lines[index] = new LineGeneration.Line { p1 = line.p1, p2 = line.p2 };
            }
        }
    }
}