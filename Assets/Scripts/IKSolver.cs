using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Icebreakers
{
    public class IKSolver
    {
        PoseBone[] boneChain;
        PoseBone target;
        PoseBone poleTarget;
        Vector3[] vertexPositions;
        Vector3[] startVertexPositions;
        Quaternion[] initialRotations;
        float[] lengths;

        public Vector3[] VertexPositions => vertexPositions;
        public PoseBone[] BoneChain => boneChain;

        public IKSolver(PoseBone[] boneChain, PoseBone target, PoseBone poleTarget)
        {
            if (boneChain == null)
                throw new ArgumentNullException(nameof(boneChain));

            if (target == null)
                throw new ArgumentNullException(nameof(target));

            if (boneChain.Length == 0)
                throw new ArgumentException("Bone chain must have at least one");

            for (int i = 1; i < boneChain.Length; i++)
            {
                if (boneChain[i].parent != boneChain[i - 1])
                {
                    throw new ArgumentException($"Bone chain bone {i} is not a child of bone {i - 1}");
                }
            }

            this.boneChain = (PoseBone[])boneChain.Clone();
            this.target = target;
            this.poleTarget = poleTarget;

            vertexPositions = new Vector3[boneChain.Length];
            startVertexPositions = new Vector3[boneChain.Length];
            initialRotations = new Quaternion[boneChain.Length];
            lengths = new float[boneChain.Length - 1];
        }

        public void Solve(int maxIterations, float allowedDelta)
        {
            float totalLength = 0;
            for (int i = 0; i < boneChain.Length; i++)
            {
                PoseBone poseBone = boneChain[i];
                poseBone.transform.localRotation = poseBone.restRotation;
                initialRotations[i] = poseBone.LocalSpaceToRigSpace(poseBone.transform.localRotation);
                startVertexPositions[i] = poseBone.LocalSpaceToRigSpace(poseBone.transform.localPosition);

                if (i != 0)
                {
                    float length = Vector3.Magnitude(startVertexPositions[i] - startVertexPositions[i - 1]);
                    lengths[i - 1] = length;
                    totalLength += length;
                }
            }

            for (int i = 0; i < vertexPositions.Length; i++)
            {
                vertexPositions[i] = startVertexPositions[i];
            }

            Vector3 targetPosition = target.LocalSpaceToRigSpace(target.transform.localPosition);

            if (Vector3.SqrMagnitude(targetPosition - vertexPositions[0]) > totalLength * totalLength)
            {
                Vector3 directionToTarget = Vector3.Normalize(targetPosition - vertexPositions[0]);
                for (int i = 1; i < vertexPositions.Length; i++)
                {
                    vertexPositions[i] = vertexPositions[i - 1] + directionToTarget * lengths[i - 1];
                }
            }
            else
            {
                for (int i = 0; i < maxIterations; i++)
                {
                    vertexPositions[vertexPositions.Length - 1] = targetPosition;
                    for (int j = vertexPositions.Length - 2; j > 0; j--)
                    {
                        vertexPositions[j] = vertexPositions[j + 1] + Vector3.Normalize(vertexPositions[j] - vertexPositions[j + 1]) * lengths[j];
                    }

                    for (int j = 1; j < vertexPositions.Length; j++)
                    {
                        vertexPositions[j] = vertexPositions[j - 1] + Vector3.Normalize(vertexPositions[j] - vertexPositions[j - 1]) * lengths[j - 1];
                    }

                    if (Vector3.SqrMagnitude(vertexPositions[vertexPositions.Length - 1] - targetPosition) < allowedDelta * allowedDelta)
                    {
                        break;
                    }
                }
            }

            if (poleTarget != null)
            {
                Vector3 poleTargetPosition = poleTarget.LocalSpaceToRigSpace(poleTarget.transform.localPosition);

                for (int i = 1; i < vertexPositions.Length - 1; i++)
                {
                    var plane = new Plane(vertexPositions[i + 1] - vertexPositions[i - 1], vertexPositions[i - 1]);
                    var projectedPoleTarget = plane.ClosestPointOnPlane(poleTargetPosition);
                    var projectedVertex = plane.ClosestPointOnPlane(vertexPositions[i]);
                    var angle = Vector3.SignedAngle(projectedVertex - vertexPositions[i - 1], projectedPoleTarget - vertexPositions[i - 1], plane.normal);
                    vertexPositions[i] = Quaternion.AngleAxis(angle, plane.normal) * (vertexPositions[i] - vertexPositions[i - 1]) + vertexPositions[i - 1];
                }
            }

            for (int i = 0; i < vertexPositions.Length - 1; i++)
            {
                PoseBone poseBone = boneChain[i];

                Vector3 startDirection = startVertexPositions[i + 1] - startVertexPositions[i];
                Vector3 direction = vertexPositions[i + 1] - vertexPositions[i];

                Quaternion rotation = Quaternion.FromToRotation(startDirection, direction);
                poseBone.transform.localRotation = poseBone.RigSpaceToLocalSpace(rotation * initialRotations[i]);
            }

            // copy rotation delta from target bone to final bone in the chain
            {
                PoseBone poseBone = boneChain[boneChain.Length - 1];

                Quaternion targetRestRotation = target.LocalSpaceToRigSpaceRestPose(target.restRotation);
                Quaternion boneRestRotation = poseBone.LocalSpaceToRigSpaceRestPose(poseBone.restRotation);

                Quaternion targetRotation = target.LocalSpaceToRigSpace(target.transform.localRotation);

                poseBone.transform.localRotation = poseBone.RigSpaceToLocalSpace(targetRotation * Quaternion.Inverse(targetRestRotation) * boneRestRotation);
            }
        }
    }
}