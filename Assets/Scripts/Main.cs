using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.XR.LegacyInputHelpers;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;
using UnityEngine.SceneManagement;

namespace Icebreakers
{
    public class Main : MonoBehaviour
    {
        public static Main instance;

        [SerializeField]
        GameObject cameraRig;

        [SerializeField]
        Camera mainCamera;

        Panel currentlyInsidePanel;

        Vector3 lastCameraPosition;

        Storyboard storyboard;

        StoryboardPage currentStorybardPage;
        Scene pageScene;
        Page activePage;

        public Page ActivePage => activePage;

        bool isLoading;

        List<GameObject> rootGameObjects = new List<GameObject>();

        float t;

        [SerializeField]
        float fadeToBlackDuration = 0.5f;

        float fadeToBlackT;

        public float FadeToBlack => fadeToBlackT;

        float viewRotationValue = 0;
        float targetViewRotationValue = 0;

        public float rotateDamp = 100;

        public Canvas menu;
        public Button continueButton;
        public Button exitButton;

        void Start()
        {
            instance = this;

            HideMenu();

            lastCameraPosition = mainCamera.transform.position;

            storyboard = Resources.Load<Storyboard>("Storyboard");
            if (storyboard == null)
            {
                Debug.LogError("[Main] Failed to load storyboard: no resource with name \"Storyboard\"");
            }

#if UNITY_EDITOR
            for (int i = 0; i < SceneManager.sceneCount; i++)
            {
                Scene scene = SceneManager.GetSceneAt(i);
                foreach (var page in storyboard.Pages)
                {
                    if (page.runtimeSceneIndex == scene.buildIndex)
                    {
                        pageScene = scene;
                        currentStorybardPage = page;
                        break;
                    }
                }
            }

            if (currentStorybardPage != null)
            {
                SceneManager.SetActiveScene(pageScene);

                pageScene.GetRootGameObjects(rootGameObjects);
                foreach (var gameObject in rootGameObjects)
                {
                    if (gameObject.TryGetComponent<Page>(out Page page))
                    {
                        activePage = page;
                        break;
                    }
                }
            }
#endif

            if (currentStorybardPage == null)
            {
                if (storyboard.Pages.Count > 0)
                {
                    StartCoroutine(LoadPage(storyboard.Pages[0], instantFadeToBlack: true));
                }
            }
            else
            {
                t = 0;
                InitializePanelAndBubbleVisibility();
                MoveCameraToViewOrigin();
            }
        }

        public bool IsShowingMenu => menu.gameObject.activeSelf;

        public void ShowMenu()
        {
            menu.gameObject.SetActive(true);
        }

        public void HideMenu()
        {
            menu.gameObject.SetActive(false);
        }

        IEnumerator LoadPage(StoryboardPage storyboardPage, bool skipPanelFadeIn = false, bool instantFadeToBlack = false)
        {
            if (isLoading)
            {
                throw new Exception("Already loading");
            }

            isLoading = true;

            if (instantFadeToBlack)
            {
                fadeToBlackT = 1;
            }
            else
            {
                fadeToBlackT = 0;
                while (fadeToBlackT < 1)
                {
                    fadeToBlackT = Mathf.Clamp01(fadeToBlackT + Time.deltaTime / fadeToBlackDuration);
                    yield return null;
                }
            }

            if (currentStorybardPage != null)
            {
                currentStorybardPage = null;
                activePage = null;
                Scene toUnload = pageScene;
                pageScene = default;
                yield return SceneManager.UnloadSceneAsync(toUnload);
            }

            yield return SceneManager.LoadSceneAsync(storyboardPage.runtimeSceneIndex, LoadSceneMode.Additive);
            pageScene = SceneManager.GetSceneByBuildIndex(storyboardPage.runtimeSceneIndex);
            currentStorybardPage = storyboardPage;
            activePage = null;
            pageScene.GetRootGameObjects(rootGameObjects);
            foreach (var gameObject in rootGameObjects)
            {
                if (gameObject.TryGetComponent<Page>(out Page page))
                {
                    activePage = page;
                    break;
                }
            }

            if (activePage == null)
            {
                Debug.LogError($"[Main] Could not find Page GameObject for scene {storyboardPage.title}\nNote that it must be at the root of the scene!");
            }

            t = 0;
            if (!skipPanelFadeIn)
            {
                InitializePanelAndBubbleVisibility();
            }

            MoveCameraToViewOrigin();

            // wait one frame to avoid large time skips caused by scene loading
            yield return null;

            while (fadeToBlackT > 0)
            {
                fadeToBlackT = Mathf.Clamp01(fadeToBlackT - Time.deltaTime / fadeToBlackDuration);
                yield return null;
            }

            isLoading = false;
        }

        void MoveCameraToViewOrigin()
        {
            if (activePage == null)
            {
                cameraRig.transform.localPosition = Vector3.zero;
                return;
            }

            XRRig offset = cameraRig.GetComponent<XRRig>();
            cameraRig.transform.localPosition = activePage.ViewOrigin - new Vector3(0, offset.cameraYOffset, 0);
            viewRotationValue = Quaternion.Angle(Quaternion.identity, Quaternion.LookRotation(activePage.ViewOriginRotation * Vector3.forward, Vector3.up));
            targetViewRotationValue = viewRotationValue;
            cameraRig.transform.localRotation = Quaternion.AngleAxis(viewRotationValue, Vector3.up);
        }

        int GetActivePageIndex()
        {
            for (int i = 0; i < storyboard.Pages.Count; i++)
            {
                StoryboardPage page = storyboard.Pages[i];
                if (page == currentStorybardPage)
                {
                    return i;
                }
            }

            return -1;
        }

        public void RotatePage(float delta)
        {
            if (activePage == null)
                return;

            targetViewRotationValue += delta;
        }

        public void NextPage()
        {
            if (isLoading)
            {
                Debug.LogError("[Main] Cannot go to next page, currently loading");
                return;
            }

            int index = GetActivePageIndex();

            if (index == -1)
            {
                Debug.LogError("[Main] Cannot go to next page, we have no active page");
                return;
            }

            if (index == storyboard.Pages.Count - 1)
            {
                Debug.LogError("[Main] Cannot go to next page, we are at the last page");
                return;
            }

            StartCoroutine(LoadPage(storyboard.Pages[index + 1]));
        }

        public void PreviousPage()
        {
            if (isLoading)
            {
                Debug.LogError("[Main] Cannot go to previous page, currently loading");
                return;
            }

            int index = GetActivePageIndex();

            if (index == -1)
            {
                Debug.LogError("[Main] Cannot go to previous page, we have no active page");
                return;
            }

            if (index == 0)
            {
                Debug.LogError("[Main] Cannot go to previous page, we are at the last page");
                return;
            }

            StartCoroutine(LoadPage(storyboard.Pages[index - 1], skipPanelFadeIn: true));
        }

        public void ReloadPage()
        {
            activePage.playMusic();
            t = 0;
            InitializePanelAndBubbleVisibility();
            UpdatePanelAndBubbleVisibility();
        }

        void Update()
        {
            if (activePage != null)
            {
                viewRotationValue = Mathf.Lerp(viewRotationValue, targetViewRotationValue, 1 - Mathf.Exp(-Time.deltaTime * rotateDamp));
                cameraRig.transform.localRotation = Quaternion.AngleAxis(viewRotationValue, Vector3.up);
            }

            t += Time.deltaTime;
            UpdatePanelAndBubbleVisibility();
        }

        void OnNextPage()
        {
            NextPage();
        }

        void InitializePanelAndBubbleVisibility()
        {
            if (activePage == null)
                return;

            foreach (var panel in activePage.Panels)
            {
                if (currentStorybardPage.GetTimelineTime(panel.id) != 0)
                {
                    panel.gameObject.SetActive(false);
                }
                else
                {
                    panel.gameObject.SetActive(true);
                }

                foreach (var speechBubble in panel.SpeechBubbles)
                {
                    foreach (var bubble in speechBubble.bubbles)
                    {
                        if (currentStorybardPage.GetTimelineTime(bubble.id) != 0)
                        {
                            speechBubble.HideBubble(bubble);
                        }
                    }
                }
            }
        }

        void UpdatePanelAndBubbleVisibility()
        {
            if (activePage == null)
                return;

            foreach (var panel in activePage.Panels)
            {
                if (t >= currentStorybardPage.GetTimelineTime(panel.id))
                {
                    if (!panel.gameObject.activeSelf)
                    {
                        panel.gameObject.SetActive(true);
                        panel.FadeIn();
#if UNITY_EDITOR
                        panel.LookAtPanel();
#endif
                    }
                }

                foreach (var speechBubble in panel.SpeechBubbles)
                {
                    foreach (var bubble in speechBubble.bubbles)
                    {
                        if (t >= currentStorybardPage.GetTimelineTime(bubble.id))
                        {
                            if (!bubble.isVisible) speechBubble.ShowBubble(bubble);
                        }
                    }
                }
            }
        }

        void LateUpdate()
        {
            // XXX: When moving closer to the panel than the near clipping plane
            // the panel will not be rendered correctly. We might want to switch
            // to the inside earlier so that the transition is not visible.

            Vector3 currentCameraPosition = mainCamera.transform.position;

            Ray ray = new Ray(lastCameraPosition, currentCameraPosition - lastCameraPosition);
            float maxDistance = Vector3.Distance(currentCameraPosition, lastCameraPosition);

            if (currentlyInsidePanel != null)
            {
                if (currentlyInsidePanel.IntersectWithPanel(ray, maxDistance, out PanelIntersection intersection))
                {
                    if (!intersection.front)
                    {
                        currentlyInsidePanel.IsInside = false;
                        currentlyInsidePanel = null;
                    }
                }
            }

            if (activePage != null && currentlyInsidePanel == null)
            {
                foreach (var panel in activePage.Panels)
                {
                    if (panel.IntersectWithPanel(ray, maxDistance, out PanelIntersection intersection))
                    {
                        if (intersection.front)
                        {
                            currentlyInsidePanel = panel;
                            panel.IsInside = true;
                            break;
                        }
                    }
                }
            }

            lastCameraPosition = currentCameraPosition;
        }
    }
}