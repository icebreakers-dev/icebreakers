using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace Icebreakers
{
    public static class NativeUtil
    {
        public static unsafe ref T ElementAt<T>(this NativeArray<T> array, int index) where T : struct
        {
            CheckIndexInRange(index, array.Length);
            return ref UnsafeUtility.ArrayElementAsRef<T>(array.GetUnsafePtr<T>(), index);
        }

        [Conditional("ENABLE_UNITY_COLLECTIONS_CHECKS")]
        static void CheckIndexInRange(int value, int length)
        {
            if (value < 0)
                throw new IndexOutOfRangeException($"Value {value} must be positive.");

            if ((uint)value >= (uint)length)
                throw new IndexOutOfRangeException($"Value {value} is out of range in NativeList of '{length}' Length.");
        }
    }
}