using System.Collections;
using System.Collections.Generic;
using UnityEngine;
//using UnityEngine.UI;
using TMPro;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Icebreakers
{
    [ExecuteInEditMode]
    [RequireComponent(typeof(MeshFilter), typeof(MeshRenderer))]
    public class SpeechBubbleMesh : MonoBehaviour
    {
        Mesh mesh;

        public MeshRenderer meshRenderer;
        public MeshFilter meshFilter;
        public TMP_Text textMesh;
        private Vector2 size;
        public Vector2 Size => size;

        public void Init()
        {
            mesh = new Mesh() { name = "SpeechBubbleMesh" };
            mesh.hideFlags = HideFlags.HideAndDontSave;
            meshFilter.sharedMesh = mesh;
        }

        public void Deinit()
        {
            GameObject.DestroyImmediate(mesh);
        }

        public void UpdateSpeechBubbleMesh(SpeechBubble speechBubble, SpeechBubble.SubBubble bubble, bool hasArrow)
        {
            transform.localPosition = bubble.position;

            textMesh.text = bubble.text;
            // XXX HACK: force an immediate update of the text info before accessing the text bounds
            textMesh.GetTextInfo(bubble.text);

            float w = Mathf.Max(textMesh.textBounds.size.x / 2, 0.001f);
            float h = Mathf.Max(textMesh.textBounds.size.y / 2, 0.001f);
            float n = 1.3f; //1.4142135f if sqrt(2) the distances to the sides are the same so A = x+n and B = y+n, visually 1.3 is a bit better? lower is more elongated.
            float width =  Mathf.Sqrt(w * w + Mathf.Pow(w, 2 / n) * Mathf.Pow(h, 2 - 2 / n)) + bubble.padding;
            float height = Mathf.Sqrt(h * h + Mathf.Pow(h, 2 / n) * Mathf.Pow(w, 2 - 2 / n)) + bubble.padding;
            size.x = width; size.y = height;

            if (hasArrow)
            {
                GenerateSpeechBubbleMesh(width, height, bubble.resolution, speechBubble.arrowPosition - bubble.position,
                    speechBubble.attachmentAngle, speechBubble.arrowWidth, bubble.square, speechBubble.outlineThickness, speechBubble.outlineMiterLimit);
            }
            else
            {
                GenerateSpeechBubbleMeshNoArrow(width, height, bubble.resolution, bubble.square, speechBubble.outlineThickness);
            }
        }

        ///////speech bubble creation

        static float VecAngle(Vector2 v)
        {
            return MathUtil.Mod(Mathf.Atan2(v.y, v.x), Mathf.PI * 2);
        }

        static float NormalizeAngle(float a)
        {
            return MathUtil.Mod(a, Mathf.PI * 2);
        }

        static float AngleDistance(float b, float a)
        {
            if (a > b) return b + Mathf.PI * 2 - a;
            else return b - a;
        }

        static Vector2 RotateBy(Vector2 v, float angle)
        {
            float cos = Mathf.Cos(angle);
            float sin = Mathf.Sin(angle);
            return new Vector2(v.x * cos - v.y * sin, v.x * sin + v.y * cos);
        }

        static Vector2 IntersectCircle(Vector2 origin, Vector2 direction)
        {
            Vector2 directionOrth = new Vector2(direction.y, -direction.x).normalized;
            float intersectionDist = Vector2.Dot(origin, directionOrth);
            float intersectionLength = Mathf.Sqrt(1 - intersectionDist * intersectionDist);
            return directionOrth*intersectionDist+direction.normalized*(-intersectionLength);
        }

        static bool Inbetween(float a, float b,float c)
        {
            if (c > b) return a < b || a > c;
            else return a < b && a > c;
        }

        static Vector2 SquareBubble(Vector2 p, float fact)
        {
            fact *= 0.99999f;
            if (fact == 0) return p;
            float u = p.x * fact;
            float v = p.y * fact;
            float u2 = u * u;
            float v2 = v * v;
            float twosqrt2 = 2.0f * Mathf.Sqrt(2.0f);
            float subtermx = 2.0f + u2 - v2;
            float subtermy = 2.0f - u2 + v2;
            float termx1 = subtermx + u * twosqrt2;
            float termx2 = subtermx - u * twosqrt2;
            float termy1 = subtermy + v * twosqrt2;
            float termy2 = subtermy - v * twosqrt2;
            float x = 0.5f * Mathf.Sqrt(termx1) - 0.5f * Mathf.Sqrt(termx2);
            float y = 0.5f * Mathf.Sqrt(termy1) - 0.5f * Mathf.Sqrt(termy2);
            return new Vector2(x / fact, y / fact);
        }

        void GenerateSpeechBubbleMesh(float width, float height, int resolution, Vector2 target, float attachmentAngle, float arrowWidth,
            float square, float outlineThickness, float outlineMiterLimit)
        {
            mesh.Clear();

            if (resolution < 3) return;
            if (textMesh.text == "") return;
            ////////////////////////

            Vector2 ellipseAxes = new Vector2(width, height);
            Vector2 targetCS = target/ellipseAxes;
            if(targetCS.magnitude < 1) targetCS = targetCS.normalized*1.001f;
            float targetVecAngle = VecAngle(targetCS);
            float tangentAngle = NormalizeAngle(targetVecAngle + Mathf.Acos(1 / targetCS.magnitude));
            float tangentAngle2 = NormalizeAngle(targetVecAngle - Mathf.Acos(1 / targetCS.magnitude));
            Vector2 tangentPoint = new Vector2(Mathf.Cos(tangentAngle), Mathf.Sin(tangentAngle));
            Vector2 tangentPoint2 = new Vector2(Mathf.Cos(tangentAngle2), Mathf.Sin(tangentAngle2));
            float attachAngle = tangentAngle2 + (AngleDistance(tangentAngle, tangentAngle2) * attachmentAngle);
            Vector2 attachPoint = new Vector2(Mathf.Cos(attachAngle), Mathf.Sin(attachAngle));
            Vector2 centerLineES = (attachPoint-targetCS)*ellipseAxes;
            Vector2 tangentES = (tangentPoint-targetCS)*ellipseAxes;
            Vector2 tangent2ES = (tangentPoint2-targetCS)*ellipseAxes;
            float arrowAngle = Mathf.Atan(arrowWidth / 2 / centerLineES.magnitude);
            Vector2 pointA, pointB, secondSideCS, firstSideCS;
            if (Vector2.Angle(centerLineES, tangentES)*Mathf.Deg2Rad < arrowAngle / 2)
            {
                pointA = tangentPoint;
                secondSideCS = RotateBy(tangentES, arrowAngle) /ellipseAxes;
                firstSideCS = tangentES/ellipseAxes;
                pointB = IntersectCircle(targetCS, secondSideCS);
            }
            else if (Vector2.Angle(tangent2ES, centerLineES) * Mathf.Deg2Rad < arrowAngle / 2)
            {
                pointB = tangentPoint2;
                firstSideCS = RotateBy(tangent2ES, -arrowAngle) / ellipseAxes;
                secondSideCS = tangent2ES/ellipseAxes;
                pointA = IntersectCircle(targetCS, firstSideCS);
            }
            else
            {
                firstSideCS = RotateBy(centerLineES, -arrowAngle / 2) / ellipseAxes;
                secondSideCS = RotateBy(centerLineES, arrowAngle / 2) / ellipseAxes;
                pointA = IntersectCircle(targetCS, firstSideCS);
                pointB = IntersectCircle(targetCS, secondSideCS);
            }

            if (arrowAngle >= Vector2.Angle(tangent2ES, tangentES) * Mathf.Deg2Rad)
            {
                firstSideCS = tangentES/ellipseAxes;
                secondSideCS = tangent2ES/ellipseAxes;
                pointA = tangentPoint;
                pointB = tangentPoint2;
            }

            ////////////////////////

            float anglePointA = VecAngle(pointA);
            float anglePointB = VecAngle(pointB);

            List<Vector3> vertices = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<float> angles = new List<float>();
            List<float> miterLengthNormals = new List<float>();

            for (int i = 0; i < resolution; i++)
            {
                float vertAngle = (Mathf.PI * 2 * i) / resolution;
                if (!Inbetween(vertAngle, anglePointA, anglePointB))
                {
                    Vector2 vertPos = new Vector2(Mathf.Cos(vertAngle), Mathf.Sin(vertAngle));
                    vertPos = SquareBubble(vertPos, square);
                    vertices.Add(new Vector3(vertPos.x * width, vertPos.y * height, 0));
                    angles.Add(vertAngle);
                }
            }

            int indexPointA = -1, indexPointB = -1;

            float anglePrev = 0;
            for(int i = 0; i < vertices.Count; i++)
            {
                float angle = angles[i];
                if (anglePointA > anglePrev && anglePointA < angle) indexPointA = i;
                if (anglePointB > anglePrev && anglePointB < angle) indexPointB = i;
                anglePrev = angle;
            }
            if (anglePointA > anglePrev) indexPointA = vertices.Count;
            if (anglePointB > anglePrev) indexPointB = vertices.Count;
            if (indexPointA == -1) indexPointA = vertices.Count - 1;
            if (indexPointB == -1) indexPointB = vertices.Count - 1;

            pointA = SquareBubble(pointA, square);
            pointB = SquareBubble(pointB, square);
            pointA *= ellipseAxes;
            pointB *= ellipseAxes;

            //indexPointB += (indexPointA < indexPointB ? 1 : 0);
            vertices.Insert(indexPointA, new Vector3(pointA.x, pointA.y, 0));
            vertices.Insert(indexPointB + (indexPointA < indexPointB ? 1 : 0), new Vector3(pointB.x, pointB.y, 0));
            angles.Insert(indexPointA, anglePointA);
            angles.Insert(indexPointB + (indexPointA < indexPointB ? 1 : 0), anglePointB);

            int verCount = vertices.Count;
            int[] triangles = new int[((verCount - 2) * 3) + 3];
            vertices.Add(new Vector3(target.x, target.y, 0));

            for (int i = 0; i < (verCount - 2); i++)
            {
                if (i % 2 == 0)
                {
                    int startIndex = i / 2;
                    triangles[i * 3 + 0] = startIndex + 1;
                    triangles[i * 3 + 1] = verCount - startIndex - 1;
                    triangles[i * 3 + 2] = (verCount - startIndex) % verCount;
                }
                else
                {
                    int startIndex = (i+1) / 2;
                    triangles[i * 3 + 0] = verCount - startIndex;
                    triangles[i * 3 + 1] = startIndex;
                    triangles[i * 3 + 2] = startIndex + 1;
                }
            }

            triangles[triangles.Length - (indexPointA < indexPointB ? 3 : 2)] = indexPointA;
            triangles[triangles.Length - (indexPointA < indexPointB ? 2 : 3)] = indexPointB + 1;
            triangles[triangles.Length - 1] = vertices.Count - 1;

            /////////////////////////////////////////

            verCount = vertices.Count;
            float arrowVertAngle = 0;

            for (int i = 0; i < verCount; i++)
            {
                Vector3 v0 = vertices[i == 0 ? verCount - 2 : i - 1]; //prev, current and next vert
                Vector3 v1 = vertices[i];
                Vector3 v2 = vertices[(i + 1) % (verCount-1)];

                if (i == indexPointA)
                {
                    if(indexPointA < indexPointB) v0 = vertices[verCount - 1];
                    else v2 = vertices[verCount - 1];
                }
                if (i == indexPointB + 1)
                {
                    if (indexPointA < indexPointB) v2 = vertices[verCount - 1];
                    else v0 = vertices[verCount - 1];
                }
                if(i == verCount-1)
                {
                    v0 = vertices[indexPointA];
                    v2 = vertices[indexPointB + 1];
                }

                Vector3 e0 = v1 - v0; //edge
                Vector3 e1 = v2 - v1;
                Vector2 eo0 = new Vector2(e0.y, -e0.x); //orthagonal vec to edge, normal of edge
                Vector2 eo1 = new Vector2(e1.y, -e1.x);
                float percE0 = e1.magnitude / (e0.magnitude + e1.magnitude); // influence percentages of edge normals
                float percE1 = e0.magnitude / (e0.magnitude + e1.magnitude);
                Vector2 norm = (eo0 * percE0 + eo1 * percE1).normalized; // normal direction
                //if(i == verCount - 1) norm *= indexPointA < indexPointB ? -1 : 1; 
                normals.Add(new Vector3(norm.x, norm.y, 0));
                float angle = AngleDistance(VecAngle(eo0), VecAngle(eo1)); //calc miter length for even extrusion
                if (i == verCount - 1) arrowVertAngle = angle;
                float miterLength = (angle > Mathf.PI ? -1 : 1) / Mathf.Cos(angle / 2);
                miterLengthNormals.Add(miterLength);
            }

            int[] trianglesOutline = new int[((verCount - 2) * 3) + 6];
            for (int i = 0; i < triangles.Length; i++)
            {
                trianglesOutline[i] = triangles[i] + verCount;
            }

            for (int i = 0; i < verCount; i++)
            {
                vertices.Add(vertices[i] + normals[i] * miterLengthNormals[i] * outlineThickness);
                //vertices[i] -= normals[i] * miterLengthNormals[i] * outlineThickness;
                normals.Add(normals[i]);
            }

            //miter limit
            vertices.Add(new Vector3(target.x, target.y, 0));
            normals.Add(normals[normals.Count-1]);
            int miterA = vertices.Count - 2, miterB = vertices.Count - 1;
            float miterInset = (miterLengthNormals[verCount - 1] * outlineThickness - outlineThickness) * (1-outlineMiterLimit);
            float miterWidth = miterInset/Mathf.Tan(arrowVertAngle / 2);
            Vector3 tangent = new Vector3(normals[miterA].y, -normals[miterA].x, 0).normalized;

            Vector3 extrusion = normals[miterA] * miterLengthNormals[verCount - 1] * outlineThickness;
            if (indexPointA < indexPointB) vertices[miterA] -= extrusion * 2 - normals[miterA] * miterInset + tangent * miterWidth;
            else vertices[miterA] -= normals[miterA] * miterInset + tangent * miterWidth;

            vertices[miterB] = vertices[miterA] + tangent * miterWidth * 2;

            mesh.subMeshCount = 2;

            trianglesOutline[trianglesOutline.Length - 3] = vertices.Count - 2;
            trianglesOutline[trianglesOutline.Length - 2] = vertices.Count - 1;
            trianglesOutline[trianglesOutline.Length - 1] = triangles[triangles.Length - 3] + verCount;

            mesh.vertices = vertices.ToArray();
            mesh.normals = normals.ToArray();

            mesh.SetTriangles(triangles, 0);
            mesh.SetTriangles(trianglesOutline, 1);

            mesh.UploadMeshData(false);
        }

        void GenerateSpeechBubbleMeshNoArrow(float width, float height, int resolution, float square, float outlineThickness)
        {
            mesh.Clear();

            if (resolution < 3) return;
            if (textMesh.text == "") return;

            List<Vector3> vertices = new List<Vector3>();
            List<Vector3> normals = new List<Vector3>();
            List<float> miterLengthNormals = new List<float>();
            List<float> angles = new List<float>();

            for (int i = 0; i < resolution; i++)
            {
                float vertAngle = (Mathf.PI * 2 * i) / resolution;
                Vector2 vertPos = new Vector2(Mathf.Cos(vertAngle), Mathf.Sin(vertAngle));
                vertPos = SquareBubble(vertPos, square);
                vertices.Add(new Vector3(vertPos.x * width, vertPos.y * height, 0));
                angles.Add(vertAngle);
            }

            int verCount = vertices.Count;
            int[] triangles = new int[((verCount - 2) * 3) + 3];

            for (int i = 0; i < (verCount - 2); i++)
            {
                if (i % 2 == 0)
                {
                    int startIndex = i / 2;
                    triangles[i * 3 + 0] = startIndex + 1;
                    triangles[i * 3 + 1] = verCount - startIndex - 1;
                    triangles[i * 3 + 2] = (verCount - startIndex) % verCount;
                }
                else
                {
                    int startIndex = (i + 1) / 2;
                    triangles[i * 3 + 0] = verCount - startIndex;
                    triangles[i * 3 + 1] = startIndex;
                    triangles[i * 3 + 2] = startIndex + 1;
                }
            }

            for(int i = 0; i < verCount; i++)
            {
                Vector3 v0 = vertices[i == 0 ? verCount-1 : i-1]; //prev, current and next vert
                Vector3 v1 = vertices[i];
                Vector3 v2 = vertices[(i + 1)%verCount];
                Vector3 e0 = v1 - v0; //edge
                Vector3 e1 = v2 - v1;
                Vector2 eo0 = new Vector2(e0.y, -e0.x); //orthagonal vec to edge, normal of edge
                Vector2 eo1 = new Vector2(e1.y, -e1.x);
                float percE0 = e1.magnitude / (e0.magnitude + e1.magnitude); // influence percentages of edge normals
                float percE1 = e0.magnitude / (e0.magnitude + e1.magnitude);
                Vector2 norm = (eo0 * percE0 + eo1 * percE1).normalized; // normal direction
                normals.Add(new Vector3(norm.x, norm.y, 0));
                float angle = AngleDistance(VecAngle(eo0), VecAngle(eo1)); //calc miter length for even extrusion
                float miterLength = -1 / Mathf.Cos(angle / 2);
                miterLengthNormals.Add(miterLength);
            }

            int[] trianglesOutline = new int[((verCount - 2) * 3) + 3];
            for (int i = 0; i < triangles.Length; i++)
            {
                trianglesOutline[i] = triangles[i] + verCount;
            }

            for (int i = 0; i < verCount; i++) {
                vertices.Add(vertices[i] + normals[i] * miterLengthNormals[i] * outlineThickness);
                normals.Add(normals[i]);
            }

            mesh.subMeshCount = 2;

            mesh.vertices = vertices.ToArray();
            mesh.normals = normals.ToArray();

            mesh.SetTriangles(triangles, 0);
            mesh.SetTriangles(trianglesOutline, 1);

            mesh.UploadMeshData(false);
        }
    }
}