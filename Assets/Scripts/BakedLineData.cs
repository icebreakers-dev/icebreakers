using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Collections.LowLevel.Unsafe;
using UnityEngine;

namespace Icebreakers
{
    public class BakedLineData : ScriptableObject
    {
        public Vector3[] _weldedVertices;
        public Vector3[] _faceNormals;
        public OutlineMesh.HalfEdge[] _halfEdges;
        public OutlineMesh.Edge[] _edges;

        public NativeArray<Vector3> weldedVertices;
        public NativeArray<Vector3> faceNormals;
        public NativeArray<OutlineMesh.HalfEdge> halfEdges;
        public NativeArray<OutlineMesh.Edge> edges;

        public ComputeBuffer weldedVerticesBuffer;
        public ComputeBuffer faceNormalsBuffer;
        public ComputeBuffer halfEdgesBuffer;
        public ComputeBuffer edgesBuffer;


        [NonSerialized]
        int referenceCount = 0;

        public void Load()
        {
            if (referenceCount == 0)
            {
                weldedVertices = new NativeArray<Vector3>(_weldedVertices, Allocator.Persistent);
                faceNormals = new NativeArray<Vector3>(_faceNormals, Allocator.Persistent);
                halfEdges = new NativeArray<OutlineMesh.HalfEdge>(_halfEdges, Allocator.Persistent);
                edges = new NativeArray<OutlineMesh.Edge>(_edges, Allocator.Persistent);

                if (SystemInfo.supportsComputeShaders)
                {
                    weldedVerticesBuffer = new ComputeBuffer(weldedVertices.Length, UnsafeUtility.SizeOf<Vector3>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                    weldedVerticesBuffer.SetData(weldedVertices);

                    faceNormalsBuffer = new ComputeBuffer(faceNormals.Length, UnsafeUtility.SizeOf<Vector3>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                    faceNormalsBuffer.SetData(faceNormals);

                    halfEdgesBuffer = new ComputeBuffer(halfEdges.Length, UnsafeUtility.SizeOf<OutlineMesh.HalfEdge>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                    halfEdgesBuffer.SetData(halfEdges);

                    edgesBuffer = new ComputeBuffer(edges.Length, UnsafeUtility.SizeOf<OutlineMesh.Edge>(), ComputeBufferType.Structured, ComputeBufferMode.Immutable);
                    edgesBuffer.SetData(edges);
                }
            }

            referenceCount++;
        }

        public void Unload()
        {
            referenceCount--;

            if (referenceCount == 0)
            {
                weldedVertices.Dispose();
                faceNormals.Dispose();
                halfEdges.Dispose();
                edges.Dispose();

                if (weldedVerticesBuffer != null)
                    weldedVerticesBuffer.Dispose();
                if (faceNormalsBuffer != null)
                    faceNormalsBuffer.Dispose();
                if (halfEdgesBuffer != null)
                    halfEdgesBuffer.Dispose();
                if (edgesBuffer != null)
                    edgesBuffer.Dispose();

                weldedVertices = default;
                faceNormals = default;
                halfEdges = default;
                edges = default;
                weldedVerticesBuffer = null;
                faceNormalsBuffer = null;
                halfEdgesBuffer = null;
                edgesBuffer = null;
            }
        }
    }
}