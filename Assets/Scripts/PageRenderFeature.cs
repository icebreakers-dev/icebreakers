using System;
using System.Collections;
using System.Collections.Generic;
using Unity.Collections;
using Unity.Mathematics;
using UnityEngine;
using UnityEngine.Rendering;
using UnityEngine.Rendering.Universal;

namespace Icebreakers
{
    public class RenderShadowsPass : ScriptableRenderPass
    {
        ProfilingSampler m_ProfilingSampler = new ProfilingSampler("Icebreakers.RenderShadowsPass");
        PageRenderFeature pageRenderer;
        RenderPipelineResources resources;

        public RenderTexture shadowTexture;
        public int lightIndex = -1;
        public Matrix4x4 viewMatrix;
        public Matrix4x4 projMatrix;
        public Matrix4x4 shadowTransform;
        public Panel panel;

        public bool isValid => lightIndex != -1;

        public RenderShadowsPass(PageRenderFeature pageRenderer, RenderPipelineResources resources)
        {
            this.renderPassEvent = RenderPassEvent.BeforeRenderingShadows; //after rendering shadows did not work for some reason?
            this.pageRenderer = pageRenderer;
            this.resources = resources;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
            shadowTexture = ShadowUtils.GetTemporaryShadowTexture(4096, 4096, 24);
            ConfigureTarget(new RenderTargetIdentifier(shadowTexture));
            ConfigureClear(ClearFlag.All, Color.black);
        }

        public override void OnCameraCleanup(CommandBuffer cmd)
        {
            if (cmd == null)
                throw new ArgumentNullException("cmd");

            if (shadowTexture)
            {
                RenderTexture.ReleaseTemporary(shadowTexture);
                shadowTexture = null;
            }
        }

        public bool Setup(Panel panel, ref RenderingData renderingData)
        {
            lightIndex = -1;
            if (panel.SceneLight == null)
                return false;

            this.panel = panel;
            NativeArray<VisibleLight> visibleLights = renderingData.cullResults.visibleLights;
            for (int i = 0; i < visibleLights.Length; i++)
            {
                VisibleLight light = visibleLights[i];
                if (light.lightType != LightType.Directional)
                    continue;

                if (light.light == panel.SceneLight)
                {
                    lightIndex = i;
                    break;
                }
            }

            if (lightIndex == -1)
            {
                //Debug.LogWarning("Light not found", panel);
                return false;
            }

            VisibleLight visibleLight = visibleLights[lightIndex];
            if (!renderingData.cullResults.GetShadowCasterBounds(lightIndex, out Bounds bounds))
            {
                //Debug.LogWarning("Bounds failed");
                return false;
            }

            ShadowSplitData splitData;
            bool success = renderingData.cullResults.ComputeDirectionalShadowMatricesAndCullingPrimitives(lightIndex,
                0, 1, Vector3.one, 4096, visibleLight.light.shadowNearPlane, out viewMatrix, out projMatrix, out splitData);

            if (!success)
            {
                //Debug.LogWarning("Matrix failed", panel);
                return false;
            }

            shadowTransform = GetShadowTransform(projMatrix, viewMatrix);

            return true;
        }

        static Matrix4x4 GetShadowTransform(Matrix4x4 proj, Matrix4x4 view)
        {
            // Currently CullResults ComputeDirectionalShadowMatricesAndCullingPrimitives doesn't
            // apply z reversal to projection matrix. We need to do it manually here.
            if (SystemInfo.usesReversedZBuffer)
            {
                proj.m20 = -proj.m20;
                proj.m21 = -proj.m21;
                proj.m22 = -proj.m22;
                proj.m23 = -proj.m23;
            }

            Matrix4x4 worldToShadow = proj * view;

            var textureScaleAndBias = Matrix4x4.identity;
            textureScaleAndBias.m00 = 0.5f;
            textureScaleAndBias.m11 = 0.5f;
            textureScaleAndBias.m22 = 0.5f;
            textureScaleAndBias.m03 = 0.5f;
            textureScaleAndBias.m23 = 0.5f;
            textureScaleAndBias.m13 = 0.5f;

            // Apply texture scale and offset to save a MAD in shader.
            return textureScaleAndBias * worldToShadow;
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            CommandBuffer cmd = CommandBufferPool.Get();
            using (new ProfilingScope(cmd, m_ProfilingSampler))
            {
                VisibleLight visibleLight = renderingData.cullResults.visibleLights[lightIndex];

                var settings = new ShadowDrawingSettings(renderingData.cullResults, lightIndex);
                settings.useRenderingLayerMaskTest = true;

                Vector4 shadowBias = ShadowUtils.GetShadowBias(ref visibleLight, lightIndex, ref renderingData.shadowData, projMatrix, 4096);
                ShadowUtils.SetupShadowCasterConstantBuffer(cmd, ref visibleLight, shadowBias);

                cmd.SetViewport(new Rect(0, 0, 4096, 4096));
                cmd.SetViewProjectionMatrices(viewMatrix, projMatrix);
                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();

                cmd.SetGlobalVector("_PanelCameraOrigin", panel.origin);
                cmd.SetGlobalVector("_PanelCameraDirection", panel.viewDir);
                cmd.SetGlobalFloat("_PanelCameraFOV", panel.fovDistortion);
                cmd.SetGlobalFloat("_PanelCameraImageDist", panel.viewPlaneDist);
                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();

                context.DrawShadows(ref settings);
                cmd.DisableScissorRect();
                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }
    }

    public class UpdateOutlinesPass : ScriptableRenderPass
    {
        ProfilingSampler m_ProfilingSampler = new ProfilingSampler("Icebreakers.UpdateOutlinesPass");
        Page page;
        Main main;
        PageRenderFeature pageRenderer;
        RenderPipelineResources resources;

        public UpdateOutlinesPass(PageRenderFeature pageRenderer, RenderPipelineResources resources)
        {
            this.renderPassEvent = RenderPassEvent.BeforeRenderingPrepasses;
            this.pageRenderer = pageRenderer;
            this.resources = resources;
        }

        public void Setup(Main main, Page page)
        {
            this.main = main;
            this.page = page;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            bool isValidPageScene = (main != null || page != null) && (renderingData.cameraData.cameraType == CameraType.Game || renderingData.cameraData.cameraType == CameraType.SceneView);

            CommandBuffer cmd = CommandBufferPool.Get();
            using (new ProfilingScope(cmd, m_ProfilingSampler))
            {
                UpdateSkinnedOutlines(context, cmd);

                pageRenderer.ResetOutlineMeshes();

                if (isValidPageScene)
                {
                    if (page != null)
                    {
                        RenderPipeline.SetGlobalVariables(cmd, page, true);

                        Panel panelInEditMode = null;
                        foreach (var panel in page.Panels)
                        {
                            if (panel.isInEditMode)
                            {
                                panelInEditMode = panel;
                                break;
                            }
                        }

                        if (panelInEditMode != null)
                        {
                            OutlineMeshState outlineMeshState = pageRenderer.AllocateOutlineMesh(panelInEditMode);

                            uint renderingLayerMask = 1u;
                            GenerateOutlines(context, cmd, renderingData.cameraData.camera, outlineMeshState, renderingLayerMask);
                        }
                        else
                        {
                            OutlineMeshState outlineMeshState = pageRenderer.AllocateOutlineMesh(null);
                            uint renderingLayerMask = 1u;
                            GenerateOutlines(context, cmd, renderingData.cameraData.camera, outlineMeshState, renderingLayerMask);

                            foreach (var panel in page.Panels)
                            {
                                if (!panel.isActiveAndEnabled)
                                    continue;

                                PanelView panelView = panel.PanelView;
                                if (panelView == null)
                                    continue;

                                outlineMeshState = pageRenderer.AllocateOutlineMesh(panel);

                                renderingLayerMask = 1u << panel.PanelNumber;
                                GenerateOutlines(context, cmd, renderingData.cameraData.camera, outlineMeshState, renderingLayerMask);
                            }
                        }
                    }
                }
                else
                {
                    RenderPipeline.SetGlobalVariables(cmd, null, false);
                    OutlineMeshState outlineMeshState = pageRenderer.AllocateOutlineMesh(null);
                    uint renderingLayerMask = 1u;
                    GenerateOutlines(context, cmd, renderingData.cameraData.camera, outlineMeshState, renderingLayerMask);
                }
            }

            context.ExecuteCommandBuffer(cmd);
            CommandBufferPool.Release(cmd);
        }

        void UpdateSkinnedOutlines(ScriptableRenderContext context, CommandBuffer cmd)
        {
            foreach (var outlineRenderer in OutlineRenderer.outlineRenderers)
            {
                if (!outlineRenderer.IsValid)
                    continue;

                if (!outlineRenderer.enabled)
                    continue;

                if (outlineRenderer.IsSkinned)
                {
                    if (pageRenderer.UseComputeLineGeneration)
                    {
                        outlineRenderer.UpdateDynamicVerticesFromSkinnedMesh(cmd);

                        if (!outlineRenderer.GetComputeBufferParams(out OutlineMeshComputeArgs args))
                        {
                            continue;
                        }

                        cmd.SetComputeBufferParam(resources.outlineUpdateVerticesComputeShader, 0, "WeldedVertices", args.weldedVerticesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateVerticesComputeShader, 0, "SourceVertices", args.dynamicSourceVerticesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateVerticesComputeShader, 0, "WeldedVertexToSourceVertex", args.weldedVertexToSourceVertexBuffer);
                        cmd.DispatchCompute(resources.outlineUpdateVerticesComputeShader, 0, (args.numWeldedVertices + 31) / 32, 1, 1);

                        cmd.SetComputeBufferParam(resources.outlineUpdateFaceNormalsComputeShader, 0, "WeldedVertices", args.weldedVerticesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateFaceNormalsComputeShader, 0, "Faces", args.facesBuffer);
                        cmd.SetComputeBufferParam(resources.outlineUpdateFaceNormalsComputeShader, 0, "FaceNormals", args.faceNormalsBuffer);
                        cmd.DispatchCompute(resources.outlineUpdateFaceNormalsComputeShader, 0, (args.numFaces + 31) / 32, 1, 1);
                    }
                    else
                    {
                        outlineRenderer.UpdateFromSkinningResult();
                    }
                }
            }
        }

        void GenerateOutlines(ScriptableRenderContext context, CommandBuffer cmd, Camera camera, OutlineMeshState outlineMeshState, uint renderingLayerMask)
        {
            Vector3 cameraPosition = camera.transform.position;

            if (pageRenderer.UseComputeLineGeneration)
            {
                cmd.SetComputeBufferCounterValue(outlineMeshState.lineBuffer, 0);

                int cameraMask = camera.cullingMask;
                foreach (var outlineRenderer in OutlineRenderer.outlineRenderers)
                {
                    if (!outlineRenderer.IsValid)
                        continue;

                    if (!outlineRenderer.enabled)
                        continue;

                    if ((cameraMask & (1 << outlineRenderer.gameObject.layer)) == 0)
                        continue;

                    if ((renderingLayerMask & outlineRenderer.SourceRenderer.renderingLayerMask) == 0)
                        continue;

                    if (!outlineRenderer.GetComputeBufferParams(out OutlineMeshComputeArgs args))
                    {
                        continue;
                    }

                    float4x4 worldToLocalMatrix = math.inverse(args.localToWorldMatrix);
                    float3 localCameraPosition = math.transform(worldToLocalMatrix, cameraPosition);

                    cmd.SetComputeMatrixParam(resources.outlineComputeShader, "LocalToWorldMatrix", args.localToWorldMatrix);
                    cmd.SetComputeVectorParam(resources.outlineComputeShader, "LocalCameraPosition", new float4(localCameraPosition, 0));
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "WeldedVertices", args.weldedVerticesBuffer);
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "FaceNormals", args.faceNormalsBuffer);
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "HalfEdges", args.halfEdgesBuffer);
                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "Edges", args.edgesBuffer);

                    cmd.SetComputeBufferParam(resources.outlineComputeShader, 0, "Lines", outlineMeshState.lineBuffer);
                    cmd.DispatchCompute(resources.outlineComputeShader, 0, (args.numEdges + 31) / 32, 1, 1);
                }

                cmd.CopyCounterValue(outlineMeshState.lineBuffer, outlineMeshState.drawArgsBuffer, 0);
                cmd.SetComputeBufferParam(resources.outlineArgsComputeShader, 0, "IndirectArgumentsBuffer", outlineMeshState.drawArgsBuffer);
                cmd.DispatchCompute(resources.outlineArgsComputeShader, 0, 1, 1, 1);
            }
            else
            {
                LineGenerationContext generationContext;
                generationContext.indices = new NativeList<int>(2024, Allocator.TempJob);
                generationContext.vertexData = new NativeList<LineGeneration.VertexData>(1024, Allocator.TempJob);

                int cameraMask = camera.cullingMask;
                foreach (var outlineRenderer in OutlineRenderer.outlineRenderers)
                {
                    if (!outlineRenderer.IsValid)
                        continue;

                    if (!outlineRenderer.enabled)
                        continue;

                    if ((cameraMask & (1 << outlineRenderer.gameObject.layer)) == 0)
                        continue;

                    if ((renderingLayerMask & outlineRenderer.SourceRenderer.renderingLayerMask) == 0)
                        continue;

                    outlineRenderer.GenerateForCamera(camera, generationContext);
                }

                outlineMeshState.mesh.Clear();
                outlineMeshState.mesh.indexFormat = IndexFormat.UInt32;
                outlineMeshState.mesh.SetVertexBufferParams(generationContext.vertexData.Length, new VertexAttributeDescriptor[]
                {
                    new VertexAttributeDescriptor(VertexAttribute.TexCoord0, VertexAttributeFormat.Float32, dimension: 4),
                    new VertexAttributeDescriptor(VertexAttribute.TexCoord1, VertexAttributeFormat.Float32, dimension: 4),
                });

                outlineMeshState.mesh.SetVertexBufferData(generationContext.vertexData.AsArray(), 0, 0, generationContext.vertexData.Length);
                outlineMeshState.mesh.SetIndices(generationContext.indices.AsArray(), MeshTopology.Triangles, 0, false);
                outlineMeshState.mesh.UploadMeshData(false);

                generationContext.indices.Dispose();
                generationContext.vertexData.Dispose();
            }
        }
    }

    public class PageRenderPass : ScriptableRenderPass, IDisposable
    {
        Main main;
        Page page;
        bool bakingPreview;
        RenderPipelineResources resources;
        PageRenderFeature pageRenderer;
        List<ShaderTagId> m_ShaderTagIdList = new List<ShaderTagId>()
        {
            new ShaderTagId("SRPDefaultUnlit"),
            new ShaderTagId("UniversalForward"),
            new ShaderTagId("UniversalForwardOnly"),
            new ShaderTagId("LightweightForward")
        };
        ProfilingSampler m_ProfilingSampler = new ProfilingSampler("Icebreakers.PageRenderPass");

        Material[] outlineMaterials;
        Material[] drawnLineMaterials;
        Material[] panelStencilMaterials;
        Material invertedPanelStencilMaterial;
        Texture2D blankPanelMask;
        Mesh planeMesh;

        MaterialPropertyBlock outlineProperties = new MaterialPropertyBlock();
        MaterialPropertyBlock drawnLineProperties = new MaterialPropertyBlock();
        MaterialPropertyBlock panelStencilProperties = new MaterialPropertyBlock();

        const int STENCIL_PASS_FORWARD = 0;
        const int STENCIL_PASS_INVERTED = 1;
        const int STENCIL_PASS_CLEAR_DEPTH = 2;
        const int STENCIL_PASS_CLEAR_INITIAL = 3;
        const int STENCIL_PASS_DEBUG = 4;
        const int STENCIL_PASS_FADE_TO_BLACK = 5;

        public PageRenderPass(PageRenderFeature pageRenderer, RenderPipelineResources resources)
        {
            this.renderPassEvent = RenderPassEvent.AfterRenderingOpaques;
            this.pageRenderer = pageRenderer;
            this.resources = resources;

            // XXX: Because we cannot change rendering state with MaterialPropertyBlock or SetGlobalProperty
            // we need to create all materials with the correct rendering state beforehand...
            outlineMaterials = new Material[32];
            for (int i = 0; i < 32; i++)
            {
                var material = new Material(resources.outlineShader) { hideFlags = HideFlags.HideAndDontSave };
                material.SetFloat("_PanelNumber", i);
                outlineMaterials[i] = material;
            }
            drawnLineMaterials = new Material[32];
            for (int i = 0; i < 32; i++)
            {
                var material = new Material(resources.drawnLineShader) { hideFlags = HideFlags.HideAndDontSave };
                material.SetFloat("_PanelNumber", i);
                drawnLineMaterials[i] = material;
            }
            panelStencilMaterials = new Material[32];
            for (int i = 0; i < panelStencilMaterials.Length; i++)
            {
                var material = new Material(resources.panelStencilShader) { hideFlags = HideFlags.HideAndDontSave };
                material.SetFloat("_PanelNumber", i);
                panelStencilMaterials[i] = material;
            }
            invertedPanelStencilMaterial = new Material(resources.panelStencilShader) { hideFlags = HideFlags.HideAndDontSave };
            invertedPanelStencilMaterial.SetFloat("_PanelNumber", 100);

            blankPanelMask = new Texture2D(1, 1, TextureFormat.Alpha8, false) { hideFlags = HideFlags.HideAndDontSave };
            blankPanelMask.SetPixels32(new Color32[] { new Color32(255, 255, 255, 255) });
            blankPanelMask.Apply(true, true);

            planeMesh = new Mesh() { hideFlags = HideFlags.HideAndDontSave };
            Vector3[] vertices = new Vector3[]
            {
                new Vector3(-0.5f, -0.5f, 0),
                new Vector3(-0.5f,  0.5f, 0),
                new Vector3( 0.5f, -0.5f, 0),
                new Vector3( 0.5f,  0.5f, 0),
            };

            Vector2[] uvs = new Vector2[]
            {
                new Vector2(0, 0),
                new Vector2(0, 1),
                new Vector2(1, 0),
                new Vector2(1, 1),
            };

            int[] indices = new int[6]
            {
                0, 3, 1,
                0, 2, 3,
            };

            planeMesh.subMeshCount = 1;
            planeMesh.vertices = vertices;
            planeMesh.uv = uvs;
            planeMesh.SetIndices(indices, MeshTopology.Triangles, 0);
            planeMesh.UploadMeshData(true);
        }

        public void Dispose()
        {
            foreach (var material in outlineMaterials)
            {
                GameObject.DestroyImmediate(material);
            }
            foreach (var material in drawnLineMaterials)
            {
                GameObject.DestroyImmediate(material);
            }
            foreach (var material in panelStencilMaterials)
            {
                GameObject.DestroyImmediate(material);
            }
            GameObject.DestroyImmediate(invertedPanelStencilMaterial);
            CoreUtils.Destroy(blankPanelMask);
            CoreUtils.Destroy(planeMesh);
        }

        public void Setup(Main main, Page page, bool bakingPreview)
        {
            this.main = main;
            this.page = page;
            this.bakingPreview = bakingPreview;
        }

        public override void Configure(CommandBuffer cmd, RenderTextureDescriptor cameraTextureDescriptor)
        {
        }

        public override void Execute(ScriptableRenderContext context, ref RenderingData renderingData)
        {
            bool isValidPageScene = (main != null || page != null) && (renderingData.cameraData.cameraType == CameraType.Game || renderingData.cameraData.cameraType == CameraType.SceneView);
            if (!isValidPageScene)
            {
                //Debug.Log("drawning non-valid Page");
                CommandBuffer cmd = CommandBufferPool.Get();
                using (new ProfilingScope(cmd, m_ProfilingSampler))
                {
                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                    RenderStandardScene(context, cmd, ref renderingData);
                }
                context.ExecuteCommandBuffer(cmd);
            }
            else
            {
                //Debug.Log("drawning Page");
                CommandBuffer cmd = CommandBufferPool.Get();
                using (new ProfilingScope(cmd, m_ProfilingSampler))
                {
                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();

                    if (page != null)
                    {
                        Panel panelInEditMode = null;
                        foreach (var panel in page.Panels)
                        {
                            if (panel.isInEditMode)
                            {
                                panelInEditMode = panel;
                                break;
                            }
                        }

                        if (panelInEditMode != null)
                        {
                            RenderPanelInEditMode(context, cmd, ref renderingData, panelInEditMode);
                        }
                        else
                        {
                            RenderPanels(context, cmd, ref renderingData);
                        }
                    }

                    if (main != null && renderingData.cameraData.cameraType == CameraType.Game)
                    {
                        RenderFadeToBlack(context, cmd, ref renderingData);
                    }
                }

                context.ExecuteCommandBuffer(cmd);
                CommandBufferPool.Release(cmd);
            }
        }

        void RenderFadeToBlack(ScriptableRenderContext context, CommandBuffer cmd, ref RenderingData renderingData)
        {
            panelStencilProperties.SetFloat("_Fade", main.FadeToBlack);
            cmd.DrawMesh(planeMesh, Matrix4x4.identity, panelStencilMaterials[0], 0, STENCIL_PASS_FADE_TO_BLACK, panelStencilProperties);
        }

        void RenderStandardScene(ScriptableRenderContext context, CommandBuffer cmd, ref RenderingData renderingData)
        {
            uint renderingLayer = 1u;

            OutlineMeshState outlineMeshState = pageRenderer.GetOutlineMeshForPanel(null);

            DrawingSettings drawingSettings = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, renderingData.cameraData.defaultOpaqueSortFlags);
            DrawingSettings drawingSetingsTransparent = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, SortingCriteria.CommonTransparent);
            FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque);

            filteringSettings.renderingLayerMask = renderingLayer;

            RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
            stateBlock.stencilState = new StencilState(enabled: false);
            context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

            {
                RenderDrawnLines(context, cmd, renderingData.cameraData.camera, renderingLayer, 0, false);
                RenderOutlines(context, cmd, outlineMeshState, 0, false, false);
                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            filteringSettings.renderQueueRange = RenderQueueRange.transparent;
            context.DrawRenderers(renderingData.cullResults, ref drawingSetingsTransparent, ref filteringSettings, ref stateBlock);
        }

        void RenderPanels(ScriptableRenderContext context, CommandBuffer cmd, ref RenderingData renderingData)
        {
            Panel invertedPanel = null;
            foreach (var panel in page.Panels)
            {
                if (!panel.isActiveAndEnabled)
                    continue;

                if (panel.IsInside)
                {
                    invertedPanel = panel;
                    break;
                }
            }

            DrawingSettings drawingSettings = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, renderingData.cameraData.defaultOpaqueSortFlags);
            DrawingSettings drawingSetingsTransparent = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, SortingCriteria.CommonTransparent);
            FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque);

            // For clipping objects in panels to their panel rectangle we fill the stencil buffer
            // with an index for each panel and test for this index during drawing. The background
            // is always set to 0; panel indices start at 1.
            // Using the stencil buffer allows for sorting panels based on depth, so panels in front
            // of others do occlude what is behind them. Using the shader test for the panel would mix
            // the contents of the panels because the depth test does not include the panel geometry.
            // When we are inside of a panel the process becomes more complicated. The inverted panel
            // cannot use the stencil based test, because we need to draw geometry that occludes the
            // panel itself, so we fall back to the shader test for the inverted panel.
            // When looking to the outside from inside the inverted panel we need to draw the other panels
            // only in the places where we are looking outside. We do this by drawing the inverted panel
            // with a 0 stencil to a background cleared with a large stencil value. This way the greater stencil
            // test will only succeed on pixels where the inverted panel was drawn and fail on the background.
            // The panels will also still be sorted correctly based on depth. This method does not cover the case
            // where a panel is placed on the "inside" of the inverted panel and oriented towards the inside.

            {
                // clear depth and write background stencil
                Material clearMaterial = invertedPanel != null ? invertedPanelStencilMaterial : panelStencilMaterials[0];
                cmd.DrawMesh(planeMesh, Matrix4x4.identity, clearMaterial, 0, STENCIL_PASS_CLEAR_INITIAL);

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            {
                cmd.EnableShaderKeyword("_NO_FAKE_FOV");

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            { // draw stuff outsite panels
                uint renderingLayer = 1u;
                OutlineMeshState outlineMeshState = pageRenderer.GetOutlineMeshForPanel(null);
                filteringSettings.renderingLayerMask = renderingLayer;
                RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
                stateBlock.stencilState = new StencilState(enabled: false);
                context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

                {
                    RenderDrawnLines(context, cmd, renderingData.cameraData.camera, renderingLayer, 0, false);
                    RenderOutlines(context, cmd, outlineMeshState, 0, false, false);
                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                }

                filteringSettings.renderQueueRange = RenderQueueRange.transparent;
                context.DrawRenderers(renderingData.cullResults, ref drawingSetingsTransparent, ref filteringSettings, ref stateBlock);
            }

            {
                cmd.DisableShaderKeyword("_NO_FAKE_FOV");

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            {
                if (invertedPanel != null)
                {
                    PanelView panelView = invertedPanel.PanelView;
                    if (panelView != null)
                    {
                        Matrix4x4 matrix = Matrix4x4.TRS(
                            panelView.transform.position,
                            panelView.transform.rotation,
                            panelView.Size
                        );

                        panelStencilProperties.SetTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                        panelStencilProperties.SetTexture("_DitherTex", resources.whiteNoiseTexture);
                        panelStencilProperties.SetFloat("_Fade", invertedPanel.fade);
                        cmd.DrawMesh(panelView.StencilMesh, matrix, panelStencilMaterials[0], 0, STENCIL_PASS_INVERTED, panelStencilProperties);
                    }
                }

                // Draw panels from smaller to larger, so the greater check always succeeds where
                // a previous panel was already drawn, and only fails when
                // testing the outside of the inverted panel.
                foreach (Panel panel in page.Panels)
                {
                    if (!panel.isActiveAndEnabled)
                        continue;

                    // Inverted panel has already been drawn
                    if (panel == invertedPanel)
                        continue;

                    PanelView panelView = panel.PanelView;
                    if (panelView == null)
                        continue;

                    Matrix4x4 matrix = panelView.transform.localToWorldMatrix;

                    panelStencilProperties.SetTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                    panelStencilProperties.SetTexture("_DitherTex", resources.whiteNoiseTexture);
                    panelStencilProperties.SetFloat("_Fade", panel.fade);
                    cmd.DrawMesh(panelView.StencilMesh, matrix, panelStencilMaterials[panel.PanelNumber], 1, STENCIL_PASS_FORWARD, panelStencilProperties); // draw the page stencil objects
                }

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            {
                // reset the depth buffer
                cmd.DrawMesh(planeMesh, Matrix4x4.identity, panelStencilMaterials[0], 0, STENCIL_PASS_CLEAR_DEPTH);

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            foreach (var panel in page.Panels)
            {
                if (!panel.isActiveAndEnabled)
                    continue;

                PanelView panelView = panel.PanelView;
                if (panelView == null)
                    continue;

                filteringSettings.renderingLayerMask = 1u << panel.PanelNumber;

                #if UNITY_EDITOR
                var shadows = pageRenderer.GetRenderShadowsForPanel(panel);
                #endif

                string keyword = panel == invertedPanel ? "_PANEL_CUTOUT" : "_PANEL_CLIP";
                {
                    Vector2 size = panelView.Size;

                    Quaternion rotation = panelView.transform.rotation;
                    Vector3 position = panelView.transform.position;

                    Vector3 normal = rotation * new Vector3(0, 0, 1);
                    if (panel == invertedPanel)
                        normal = -normal;

                    Vector3 up = rotation * new Vector3(0, 1, 0);
                    Vector3 right = rotation * new Vector3(1, 0, 0);

                    cmd.EnableShaderKeyword(keyword);
                    cmd.SetGlobalVector("_PanelForward", normal);
                    cmd.SetGlobalVector("_PanelUp", up / size.y);
                    cmd.SetGlobalVector("_PanelRight", right / size.x);
                    cmd.SetGlobalVector("_PanelPosition", position);
                    cmd.SetGlobalTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                    cmd.SetGlobalTexture("_DitherTex", resources.whiteNoiseTexture);
                    cmd.SetGlobalFloat("_Fade", panel.fade);
                    cmd.SetGlobalInt("_PanelInvert", panel.IsInside ? 1 : 0);

                    cmd.SetGlobalVector("_PanelCameraOrigin", panel.origin);
                    cmd.SetGlobalVector("_PanelCameraDirection", panel.viewDir);
                    cmd.SetGlobalFloat("_PanelCameraFOV", panel.fovDistortion);
                    cmd.SetGlobalFloat("_PanelCameraImageDist", panel.viewPlaneDist);

#if UNITY_EDITOR
                    if (shadows.isValid)
                    {
                        cmd.EnableShaderKeyword("_SHADOWS");
                        cmd.SetGlobalTexture("_DirectionalShadowMap", shadows.shadowTexture);
                        cmd.SetGlobalMatrix("_WorldToShadow", shadows.shadowTransform);
                    }
#endif

                    if (bakingPreview)
                    {
                        cmd.EnableShaderKeyword("_BAKING_PREVIEW");
                    }

                    RenderPipeline.SetPerPanelVariables(panel, cmd);

                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                }

                filteringSettings.renderQueueRange = RenderQueueRange.opaque;
                RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
                if (panel == invertedPanel)
                {
                    stateBlock.stencilState = new StencilState(enabled: false);
                }
                else
                {
                    stateBlock.stencilReference = panel.PanelNumber;
                    stateBlock.stencilState = new StencilState(compareFunction: CompareFunction.Equal);
                }

                context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings, ref stateBlock); // opaques in panel

                {
                    foreach (var obj in PokeyObject.pokeyObjects)
                    {
                        if (!obj.isActiveAndEnabled)
                            continue;

                        if (obj.renderer == null)
                            continue;

                        if (obj.renderer.renderingLayerMask != filteringSettings.renderingLayerMask)
                            continue;

                        Material[] materials = obj.renderer.sharedMaterials;
                        for (int i = 0; i < materials.Length; i++)
                        {
                            cmd.DrawRenderer(obj.renderer, materials[0], i, 0);
                        }
                    }

                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                }

                {
                    RenderDrawnLines(context, cmd, renderingData.cameraData.camera, filteringSettings.renderingLayerMask, panel.PanelNumber, panel == invertedPanel); //drawn lines in panel
                    RenderOutlines(context, cmd, pageRenderer.GetOutlineMeshForPanel(panel), panel.PanelNumber, panel == invertedPanel, true); //outlines in panel

                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                }

                filteringSettings.renderQueueRange = RenderQueueRange.transparent;
                context.DrawRenderers(renderingData.cullResults, ref drawingSetingsTransparent, ref filteringSettings, ref stateBlock); //transparents (eg. speechbubble text)

                {
                    cmd.DisableShaderKeyword(keyword);

                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                }

                {
                    cmd.DisableShaderKeyword("_SHADOWS");
                    cmd.DisableShaderKeyword("_BAKING_PREVIEW");

                    context.ExecuteCommandBuffer(cmd);
                    cmd.Clear();
                }
            }

        }

        void RenderPanelInEditMode(ScriptableRenderContext context, CommandBuffer cmd, ref RenderingData renderingData, Panel panel)
        {
            uint renderingLayer = 1u << panel.PanelNumber;
            OutlineMeshState outlineMeshState = pageRenderer.GetOutlineMeshForPanel(panel);

            {
                var shadows = pageRenderer.GetRenderShadowsForPanel(panel);
                if (shadows.isValid)
                {
                    cmd.EnableShaderKeyword("_SHADOWS");
                    cmd.SetGlobalTexture("_DirectionalShadowMap", shadows.shadowTexture);
                    cmd.SetGlobalMatrix("_WorldToShadow", shadows.shadowTransform);
                }

                if (bakingPreview)
                {
                    cmd.EnableShaderKeyword("_BAKING_PREVIEW");
                }

                cmd.SetGlobalVector("_PanelCameraOrigin", panel.origin);
                cmd.SetGlobalVector("_PanelCameraDirection", panel.viewDir);
                cmd.SetGlobalFloat("_PanelCameraFOV", renderingData.cameraData.cameraType == CameraType.Game ? panel.fovDistortion : 1);
                cmd.SetGlobalFloat("_PanelCameraImageDist", panel.viewPlaneDist);

                RenderPipeline.SetPerPanelVariables(panel, cmd);

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            DrawingSettings drawingSettings = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, renderingData.cameraData.defaultOpaqueSortFlags);
            DrawingSettings drawingSetingsTransparent = CreateDrawingSettings(m_ShaderTagIdList, ref renderingData, SortingCriteria.CommonTransparent);
            FilteringSettings filteringSettings = new FilteringSettings(RenderQueueRange.opaque);

            filteringSettings.renderingLayerMask = renderingLayer;

            RenderStateBlock stateBlock = new RenderStateBlock(RenderStateMask.Stencil);
            stateBlock.stencilState = new StencilState(enabled: false);
            context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

            filteringSettings.renderingLayerMask = 1u;

            context.DrawRenderers(renderingData.cullResults, ref drawingSettings, ref filteringSettings, ref stateBlock);

            {
                RenderDrawnLines(context, cmd, renderingData.cameraData.camera, renderingLayer, 0, false);
                RenderOutlines(context, cmd, outlineMeshState, 0, false, true);

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            {
                PanelView panelView = panel.PanelView;
                if (panelView != null)
                {
                    Matrix4x4 matrix = Matrix4x4.TRS(
                        panelView.transform.position,
                        panelView.transform.rotation,
                        new Vector3(panelView.Size.x, panelView.Size.y, 0.02f)
                    );

                    panelStencilProperties.SetTexture("_PanelMask", panelView.Mask != null ? panelView.Mask : blankPanelMask);
                    panelStencilProperties.SetTexture("_DitherTex", resources.whiteNoiseTexture);
                    panelStencilProperties.SetFloat("_Fade", panel.fade);
                    cmd.DrawMesh(panelView.StencilMesh, matrix, panelStencilMaterials[0], 0, STENCIL_PASS_DEBUG, panelStencilProperties);
                }

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }

            filteringSettings.renderQueueRange = RenderQueueRange.transparent;
            context.DrawRenderers(renderingData.cullResults, ref drawingSetingsTransparent, ref filteringSettings, ref stateBlock);

            {
                cmd.DisableShaderKeyword("_SHADOWS");
                cmd.DisableShaderKeyword("_BAKING_PREVIEW");

                context.ExecuteCommandBuffer(cmd);
                cmd.Clear();
            }
        }

        void RenderOutlines(ScriptableRenderContext context, CommandBuffer cmd, OutlineMeshState outlineMeshState, int panelNumber, bool inverted, bool withFakeFov)
        {
            outlineProperties.SetColor("_BaseColor", new Color(0, 0, 0, 1));
            outlineProperties.SetFloat("_Width", pageRenderer.OutlineThickness);
            outlineProperties.SetFloat("_FakeFOV", withFakeFov ? 1 : 0);

            if (pageRenderer.UseComputeLineGeneration)
            {
                cmd.SetGlobalBuffer("_LinesBuffer", outlineMeshState.lineBuffer);
                int passIndex = inverted ? 3 : 2;
                cmd.DrawProceduralIndirect(Matrix4x4.identity, outlineMaterials[panelNumber], passIndex, MeshTopology.Triangles, outlineMeshState.drawArgsBuffer, 0, outlineProperties);
            }
            else
            {
                int passIndex = inverted ? 1 : 0;
                cmd.DrawMesh(outlineMeshState.mesh, Matrix4x4.identity, outlineMaterials[panelNumber], 0, passIndex, outlineProperties);
            }
        }

        void RenderDrawnLines(ScriptableRenderContext context, CommandBuffer cmd, Camera camera, uint renderingLayerMask, int panelNumber, bool inverted)
        {
            drawnLineProperties.SetColor("_BaseColor", new Color(0, 0, 0, 1));

            int cameraMask = camera.cullingMask;
            foreach (var drawnLineRenderer in DrawnLineRenderer.drawnLineRenderers)
            {
                if (!drawnLineRenderer.enabled)
                    continue;

                if ((cameraMask & (1 << drawnLineRenderer.gameObject.layer)) == 0)
                    continue;

                if ((renderingLayerMask & drawnLineRenderer.SourceRenderer.renderingLayerMask) == 0)
                    continue;

                if (!drawnLineRenderer.GetDrawingParams(out DrawnLineMeshDrawingArgs args))
                    continue;

                int passIndex = inverted ? 1 : 0;
                cmd.DrawMesh(args.lineMesh, args.localToWorldMatrix, drawnLineMaterials[panelNumber], 0, passIndex, drawnLineProperties);
            }
        }
    }

    public class PageRenderFeature : ScriptableRendererFeature
    {
        public static bool bakingPreview = false;

        [SerializeField]
        RenderPipelineResources resources;

        [SerializeField]
        bool useComputeLineGeneration = false;
        public bool UseComputeLineGeneration => useComputeLineGeneration && SystemInfo.supportsComputeShaders;

        [SerializeField]
        [Range(1, 10)]
        float outlineThickness = 2.3f;
        public float OutlineThickness => outlineThickness;

        UpdateOutlinesPass updateOutlinesPass;
        PageRenderPass pageRenderPass;

        readonly Stack<RenderShadowsPass> cachedRenderShadowsPasses = new Stack<RenderShadowsPass>();
        readonly List<RenderShadowsPass> usedRenderShadowsPasses = new List<RenderShadowsPass>();
        readonly Dictionary<Panel, RenderShadowsPass> renderShadowsPassesForPanels = new Dictionary<Panel, RenderShadowsPass>();

        readonly Stack<OutlineMeshState> cachedOutlineMeshes = new Stack<OutlineMeshState>();
        readonly List<OutlineMeshState> usedOutlineMeshes = new List<OutlineMeshState>();
        readonly Dictionary<Panel, OutlineMeshState> outlineMeshesForPanels = new Dictionary<Panel, OutlineMeshState>();
        OutlineMeshState defaultOutlineMeshState; // not entirely sure what you're doing here, so this is probably leaking stuff?

        public OutlineMeshState GetOutlineMeshForPanel(Panel panel)
        {
            if (panel != null)
            {
                return outlineMeshesForPanels[panel];
            }
            else
            {
                return defaultOutlineMeshState;
            }
        }

        public OutlineMeshState AllocateOutlineMesh(Panel panel)
        {

            OutlineMeshState outlineMeshState;
            if (cachedOutlineMeshes.Count > 0)
            {
                outlineMeshState = cachedOutlineMeshes.Pop();
            }
            else
            {
                outlineMeshState = new OutlineMeshState();
            }

            usedOutlineMeshes.Add(outlineMeshState);

            if (panel != null)
            {
                outlineMeshesForPanels.Add(panel, outlineMeshState);
            }
            else
            {
                defaultOutlineMeshState = outlineMeshState;
            }

            return outlineMeshState;
        }

        public void ResetOutlineMeshes()
        {
            foreach (var outlineMeshState in usedOutlineMeshes)
            {
                cachedOutlineMeshes.Push(outlineMeshState);
            }

            usedOutlineMeshes.Clear();
            outlineMeshesForPanels.Clear();
            defaultOutlineMeshState = null;
        }

        public RenderShadowsPass GetRenderShadowsForPanel(Panel panel)
        {
            return renderShadowsPassesForPanels[panel];
        }

        RenderShadowsPass AllocateRenderShadowsPass(Panel panel)
        {
            RenderShadowsPass renderShadowsPass;
            if (cachedRenderShadowsPasses.Count > 0)
            {
                renderShadowsPass = cachedRenderShadowsPasses.Pop();
            }
            else
            {
                renderShadowsPass = new RenderShadowsPass(this, resources);
            }

            usedRenderShadowsPasses.Add(renderShadowsPass);

            if (panel != null)
            {
                renderShadowsPassesForPanels.Add(panel, renderShadowsPass);
            }

            return renderShadowsPass;
        }

        void ResetRenderShadowsPasses()
        {
            foreach (var RenderShadowsPass in usedRenderShadowsPasses)
            {
                cachedRenderShadowsPasses.Push(RenderShadowsPass);
            }

            usedRenderShadowsPasses.Clear();
            renderShadowsPassesForPanels.Clear();
        }

        public override void Create()
        {
            updateOutlinesPass = new UpdateOutlinesPass(this, resources);
            pageRenderPass = new PageRenderPass(this, resources);
        }

        protected override void Dispose(bool disposing)
        {
            base.Dispose(disposing);

            if (disposing)
            {
                pageRenderPass.Dispose();

                foreach (var state in cachedOutlineMeshes)
                {
                    state.Dispose();
                }
                cachedOutlineMeshes.Clear();

                foreach (var state in usedOutlineMeshes)
                {
                    state.Dispose();
                }
                usedOutlineMeshes.Clear();

                outlineMeshesForPanels.Clear();
            }
        }

        public override void AddRenderPasses(ScriptableRenderer renderer, ref RenderingData renderingData)
        {
            // TODO: remove FindObjectOfType
            Main main = Main.instance;
            Page page;
            if (main != null)
            {
                page = main.ActivePage;
            }
            else
            {
                page = GameObject.FindObjectOfType<Page>();
            }

            bool isValidPageScene = (main != null || page != null) && (renderingData.cameraData.cameraType == CameraType.Game || renderingData.cameraData.cameraType == CameraType.SceneView);

            #if UNITY_EDITOR
            ResetRenderShadowsPasses();

            if (isValidPageScene)
            {
                if (page != null)
                {
                    foreach (var panel in page.Panels)
                    {
                        RenderShadowsPass pass = AllocateRenderShadowsPass(panel);
                        if (pass.Setup(panel, ref renderingData))
                        {
                            renderer.EnqueuePass(pass);
                        }
                    }
                }
            }
#endif

            updateOutlinesPass.Setup(main, page);
            pageRenderPass.Setup(main, page, bakingPreview);
            renderer.EnqueuePass(updateOutlinesPass);
            renderer.EnqueuePass(pageRenderPass);
        }
    }

    public class OutlineMeshState : IDisposable
    {
        public Mesh mesh;
        public ComputeBuffer lineBuffer;
        public ComputeBuffer drawArgsBuffer;

        public OutlineMeshState()
        {
            mesh = new Mesh();
            mesh.hideFlags = HideFlags.HideAndDontSave;
            mesh.MarkDynamic();

            if (SystemInfo.supportsComputeShaders)
            {
                // TODO: can we make the maximum size of the line buffer dynamic?
                drawArgsBuffer = new ComputeBuffer(1, 16, ComputeBufferType.IndirectArguments, ComputeBufferMode.Immutable);
                lineBuffer = new ComputeBuffer(200_000, 24, ComputeBufferType.Append, ComputeBufferMode.Immutable);
            }
        }

        public void Dispose()
        {
            CoreUtils.Destroy(mesh);

            if (drawArgsBuffer != null)
            {
                drawArgsBuffer.Dispose();
            }

            if (lineBuffer != null)
            {
                lineBuffer.Dispose();
            }
        }
    }
}