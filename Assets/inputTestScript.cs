using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[ExecuteInEditMode]
public class inputTestScript : MonoBehaviour
{
    public Transform targetSSVec;
    public Transform camera;

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawLine(transform.position, transform.position + transform.forward);
        Gizmos.color = Color.green;
        Gizmos.DrawLine(transform.position, transform.position + transform.right);
        Gizmos.color = Color.black;
        Gizmos.DrawLine(transform.position, targetSSVec.position);
        Gizmos.color = Color.yellow;
        Gizmos.DrawLine(transform.position, camera.position);

        Vector3 p1CamFwd = transform.position - camera.position;
        Vector3 p1CamRight = new Vector3(p1CamFwd.y, -p1CamFwd.x, 0).normalized;
        Vector3 p1CamUp = Vector3.Cross(p1CamFwd, p1CamRight).normalized;
        
        Vector3 p1TangentSS = (targetSSVec.position - transform.position).normalized;
        Vector3 p1WSProjTangent = 
            (transform.forward / Vector3.Dot(transform.forward, p1TangentSS)) + 
            (transform.right / Vector3.Dot(transform.right, p1TangentSS));

        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + p1WSProjTangent);

        /*Gizmos.color = new Color(0,0.3f,0);
        Gizmos.DrawLine(transform.position, transform.position + (Vector3.Dot(transform.right, p1TangentSS) * p1TangentSS));
        Gizmos.color = new Color(0.3f,0, 0);
        Gizmos.DrawLine(transform.position, transform.position + (Vector3.Dot(transform.forward, p1TangentSS) * p1TangentSS));*/
    }
}
